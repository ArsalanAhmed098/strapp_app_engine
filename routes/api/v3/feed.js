const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { FeedController } = require('../../../controllers/v3');
const { userAccessV3 } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);

router.route('/')
  .get(FeedController.getAll);

module.exports = router;
