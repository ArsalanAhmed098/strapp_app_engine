const { Notification, User } = require('../../services/v2');

const getAll = async (req, res) => {
  const notifications = await Notification.getAll(req.user);
  res.success(notifications);
};

const registerfirebaseToken = async (req, res) => {
  await User.registerfirebaseToken(req.user, req.body.token);
  res.success();
};

const getfirebaseTokens = async (req, res) => {
  const tokens = await User.getfirebaseTokens(req.user);
  res.success(tokens);
};

const updateSeen = async (req, res) => {
  await User.updateNotificationSeen(req.user);
  res.success();
};

const updateState = async (req, res) => {
  await User.updateNotificationState(req.user, req.body.pushState);
  res.success();
};

module.exports = {
  getAll,
  registerfirebaseToken,
  getfirebaseTokens,
  updateSeen,
  updateState
};
