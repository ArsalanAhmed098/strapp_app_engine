const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { CourseController, SectionController } = require('../../../controllers/v2');
const { userAccess } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccess);

router.route('/')
  .post(CourseController.add)
  .get(CourseController.getAll);

router.route('/:courseName/sections')
  .post(SectionController.add)
  .get(SectionController.getAllByCourse);

module.exports = router;
