const { UniversityModel } = require('../../models/v2');

const getByDomain = domain =>
  UniversityModel.findOne({ domains: { $elemMatch: { $eq: domain } } });

module.exports = {
  getByDomain
};
