const uuid4 = require('uuid/v4');
const { StudentModel } = require('../../models/v2');
const University = require('./University');
const User = require('./User');
const {
  sendMail,
  genExpire,
  Password,
} = require('../../util');

const register = async (userData, hostURL) => {
  userData.email = userData.email.toLowerCase();
  userData.username = userData.username.toLowerCase();

  // Check the duplication of username/email
  const duplicationError = await isDuplicated(userData);
  if (duplicationError)
    throwError(duplicationError);

  // Check the email based on university data
  const domain = userData.email.split('@')[1];
  let university = await University.getByDomain(domain);
  if (!university)
    (university = {}, university.name = 'University of Michigan - Ann Arbor');
  // throwError('EMAIL_INVALID');

  userData.university = {
    name: university.name,
    domain
  };

  userData.password = await Password.hash(userData.password);
  const currAuthToken = uuid4();
  userData.tokens = [{
    authToken: currAuthToken
  }];

  const confirmCode = uuid4();
  userData.confirmation = {
    code: confirmCode,
    expire: genExpire(5)
  };

  userData.block = {
    deniedRoutes: [
      {
        path: 'sections/*/posts',
        method: 'POST'
      },
      {
        path: 'posts/*/comments',
        method: 'POST'
      }
    ],
  };

  const link = `${hostURL}/api/v2/user/verify/${confirmCode}`;
  const verifyMail = await sendMail({
    firstName: userData.firstName,
    email: userData.email,
    link
  });
  console.log('verifyMail state:', verifyMail);

  const user = await StudentModel.create(userData);
  return User.get(user, currAuthToken);
};

const login = async user => {
  const currAuthToken = uuid4();
  user.tokens.push({
    authToken: currAuthToken
  });
  const loggedInUser = await user.save();
  return User.get(loggedInUser, currAuthToken);
};

const logout = async user => {
  for(const i in user.tokens) {
    if(user.tokens[i].authToken === user.currAuthToken) {
      user.tokens.splice(i, 1);
      break;
    }
  }
  await user.save();
};

const tokenValidation = async token => {
  const user = await StudentModel.findOne({ 'tokens.authToken': token });
  if (!user)
    throwError('TOKEN_INVALID');
};

const emailValidation = async email => {
  email = email.toLowerCase();
  // const emailReg = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/gi;
  // if (email.split('@')[1] !== 'umich.edu' || !emailReg.test(email))
  //   throwError('EMAIL_INVALID');
  const checkEmail = await isUnique({ email });
  if (!checkEmail)
    throwError('EMAIL_EXISTS');
};

const usernameValidation = async username => {
  username = username.toLowerCase();
  const checkUsername = await isUnique({ username });
  if (!checkUsername)
    throwError('USERNAME_EXISTS');
};

// ========== Utilities =============
const isDuplicated = async ({ email, username }) => {
  const user = await StudentModel.findOne({
    $or: [{ email }, { username }]
  });
  return user ?
    (user.email === email ? 'EMAIL_EXISTS' : 'USERNAME_EXISTS') :
    false;
};

const isUnique = async field => !(await StudentModel.findOne(field));

module.exports = {
  register,
  login,
  logout,
  tokenValidation,
  emailValidation,
  usernameValidation,
}
