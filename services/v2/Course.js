const { SectionModel, UserSeenGroupModel } = require('../../models/v2');
const { save, addMembership, getMembership } = require('./Section');

const add = async (user, name) => {
  let course = await SectionModel.findOneAndUpdate({
    'course.name': name,
    'section.name': 'all sections',
    'university.name': user.university.name,
  }, {
    $addToSet: { 'university.domains': user.university.domain }
  }, { new: true });

  if (!course) {
    course = await save({
      course: {
        name,
      },
      university: {
        name: user.university.name,
        domains: [user.university.domain]
      }
    });
  }

  let membership = await getMembership(user._id, course._id);

  if (!membership) {
    membership = await addMembership(user._id, course._id);
    course.memberCount++;
    await course.save();
  }

  course._doc.groupId = course._id;
  course._doc.title = course.course.name;
  delete course._doc._id;

  return course;
};

const getAll = async user => {
  const courses = await UserSeenGroupModel.aggregate([
    {
      $match: {
        userId: user._id
      }
    },
    {
      $lookup: {
        from: 'sections',
        localField: 'groupId',
        foreignField: '_id',
        as: 'section'
      }
    },
    { $unwind: '$section' },
    {
      $group: {
        _id: '$userId',
        courseNames: { $addToSet: '$section.course.name' }
      }
    },
    {
      $project: {
        _id: false,
        courseNames: true
      }
    }
  ]);
  return courses.length ? courses[0].courseNames : [];
};

// ========== Utilities =============

module.exports = {
  add,
  getAll
};
