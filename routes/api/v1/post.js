const router = require('express').Router();
const passport = require('../../../conf/v1/auth');
const { PostController } = require('../../../controllers/v1');
const { upload, userAccess } = require('../../../util');

router.use(passport.authenticate('user-bearer', { session: false }), userAccess);

router.route('/')
  .get(PostController.getPosts)
  .post(upload().single('photo'), PostController.createPost);

router.route('/:postId')
  .get(PostController.getPost)
  .put(PostController.updatePost)
  .delete(PostController.deletePost);

router.route('/:postId/report')
  .patch(PostController.reportPost);

router.route('/:postId/like')
  .patch(PostController.likePost)
  .delete(PostController.removeLikePost);
router.route('/:postId/dislike')
  .patch(PostController.dislikePost)
  .delete(PostController.removeDislikePost);

module.exports = router;
