const mongoose = require('mongoose');

const MajorSchema = new mongoose.Schema({
  major:{
    title: {
    type: String,
    required: true,
  },
},
  isBlocked: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
},{
    timestamps: true
  });
module.exports = mongoose.model('Major', MajorSchema);