const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
// const { SectionController, PostController } = require('../../../controllers/v3');
const { SectionController, PostController } = require('../../../controllers/v4');
const { userAccessV3, upload } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);

router.route('/')
  .get(SectionController.getAll)

  
router.route('/campus')
  .get(SectionController.getAllCampusSections)

  router.route('/joinedsectionslist') 
  .get(SectionController.getalljoinedgroups)

  // router.route('/campusexplorelist')
  // .get(SectionController.getAllCampusExplorelist)
  

  router.route('/:campusGroupId/followAllCampusGroups')
  .post(SectionController.joinCampusGroupOnBoarding)

  router.route('/:campusGroupId/unfollowAllCampusGroups')
  .post(SectionController.leaveCampusGroupOnBoarding)

  router.route('/:userId/followDefaultCampusGroup')
  .post(SectionController.joinDefaultCampusGroupOnBoarding)

  router.route('/:sectionId/:campusgroupId/campusgroup')
  .post(SectionController.joinCampusGroup)
  // .get(SectionController.get)
  .delete(SectionController.leaveCampusGroup);

router.route('/:sectionId')
  .post(SectionController.join)
  .get(SectionController.get)
  .delete(SectionController.leave);

router.route('/:sectionId/seen')
  .patch(SectionController.updateSeen);

router.route('/:sectionId/users')
  .get(SectionController.getAllMembers);

router.route('/:sectionId/posts')
  .post(
    upload().array('photos'),
    PostController.add
  )
  .get(PostController.getAll);

router.route('/:campusCategoryId/allpost')
  .get(PostController.getAllCatrgoryPost);


  router.route('/:sectionId/campusposts')
  .post(
    upload().array('photos'),
    PostController.addCampusPost
  )
  .get(PostController.getAllCampusPost);

router.route('/:sectionId/posts/topic')
  .get(PostController.getAllByTopic);

module.exports = router;
