const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { AuthController } = require('../../../controllers/v3');
const { userAccessV3 } = require('../../../middleware');

router.route('/register')
  .post(AuthController.register);

router.route('/login')
  .post(
    (req, res, next) => {
      req.body.username = req.body.username || '\0';
      req.body.email = req.body.email || '\0';
      next();
    },
    passport.authenticate('student-local', { session: false }),
    userAccessV3,
    AuthController.login
  );

router.route('/logout')
  .post(
    passport.authenticate('student-bearer', { session: false }),
    AuthController.logout
  );

router.route('/validation/token')
  .post(AuthController.validation.token);

router.route('/validation/email')
  .post(AuthController.validation.email);

router.route('/validation/username')
  .post(AuthController.validation.username);

module.exports = router;
