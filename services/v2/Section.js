const mongoose = require('mongoose');
const { SectionModel, UserSeenGroupModel } = require('../../models/v2');
const User = require('./User');

const add = async (user, courseName, sectionName, sectionNumber, sectionCategory) => {
  courseName = courseName.toUpperCase();
  sectionName = sectionName.toLowerCase();
  const course = await SectionModel.find({
    'course.name': courseName,
    'university.name': user.university.name,
  });
  if (!course.length)
    throwError('COURSE_NOT_FOUND');

  let section = course.find(({ section }) => section.name === sectionName);
  if (section) {
    // throwError('SECTION_EXISTS');
    return join(user, section._id);
  }

  section = await save({
    course: {
      name: courseName
    },
    section: {
      name: sectionName,
      number: sectionNumber,
      category: sectionCategory
    },
    memberCount: 1,
    university: {
      name: user.university.name,
      domains: [user.university.domain]
    }
  });

  let membership = await getMembership(user._id, section._id);

  if (!membership)
    membership = await addMembership(user._id, section._id);

  section._doc.groupId = section._id;
  section._doc.title = section.course.name;
  delete section._doc._id;

  return section;
};

const getAllByCourse = async (user, courseName) => {
  courseName = courseName.toUpperCase();
  const sections = await SectionModel.aggregate([
    {
      $match: {
        'course.name': courseName,
        'university.name': user.university.name,
        memberCount: { $gt: 0 },
      }
    },
    {
      $lookup: {
        from: 'userseengroups',
        let: { gId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$groupId', '$$gId'] },
                  { $eq: ['$userId', user._id] }
                ]
              }
            }
          }
        ],
        as: 'membership'
      }
    },
    {
      $unwind: {
        path: '$membership',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'posts',
        let: { gId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$groupId', '$$gId'] },
                  { $ne: ['$isDeleted', true] },
                ]
              }
            }
          }
        ],
        as: 'posts'
      }
    },
    {
      $project: project()
    }
  ]);
  // ,{allowDiskUse : true}
  return sections;
};

const getAll = async user => {
  const sections = await UserSeenGroupModel.aggregate([
    {
      $match: {
        userId: user._id
      }
    },
    {
      $lookup: {
        from: 'sections',
        let: { gId: '$groupId' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $ne: ['$isDeleted', true] },
                  { $eq: ['$_id', '$$gId'] },
                  { $eq: ['$university.name', user.university.name] }
                ]
              }
            }
          }
        ],
        as: 'section'
      }
    },
    { $unwind: '$section' },
    {
      $lookup: {
        from: 'posts',
        let: { gId: '$groupId' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$groupId', '$$gId'] },
                  { $ne: ['$isDeleted', true] }
                ]
              }
            }
          }
        ],
        as: 'posts'
      }
    },
    {
      $project: {
        section: {
          groupId: '$groupId',
          section: true,
          course: true,
          title: '$section.course.name',
          university: true,
          memberCount: true,
          topics: true,
          createdAt: true,
          newPostCount: {
            $size: {
              $filter: {
                input: '$posts',
                as: 'post',
                cond: {
                  $and: [
                    { $gt: ['$$post.createdAt', '$lastSeen'] }
                  ]
                }
              }
            }
          },
        }
      }
    },
    {
      $replaceRoot: { newRoot: '$section' }
    }
  ]);

  // ,{allowDiskUse : true}
  return sections;
};

const get = async (user, sectionId) => {
  const section = await SectionModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(sectionId),
      }
    },
    {
      $lookup: {
        from: 'userseengroups',
        let: { gId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$groupId', '$$gId'] },
                  { $eq: ['$userId', user._id] }
                ]
              }
            }
          }
        ],
        as: 'membership'
      }
    },
    {
      $unwind: {
        path: '$membership',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'posts',
        let: { gId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$groupId', '$$gId'] },
                  { $ne: ['$isDeleted', true] }
                ]
              }
            }
          }
        ],
        as: 'posts'
      }
    },
    {
      $project: project()
    }
  ]);

  // ,{allowDiskUse : true}
  if (!section.length)
    throwError('SECTION_NOT_FOUND');
  return section[0];
};

const join = async (user, sectionId) => {
  let section = await SectionModel.findOne({
    _id: mongoose.Types.ObjectId(sectionId),
    'university.name': user.university.name
  });
  if (!section)
    throwError('SECTION_NOT_FOUND');

  let membership = await getMembership(user._id, sectionId);
  if (!membership) {
    await addMembership(user._id, sectionId);
    section.memberCount++;
    section = await section.save();
  }

  section._doc.groupId = section._id;
  section._doc.title = section.course.name;
  delete section._doc._id;

  return section;
};

const leave = async (user, sectionId) => {
  const section = await UserSeenGroupModel.deleteOne({
    userId: user._id,
    groupId: sectionId
  });
  if (!section.deletedCount)
    throwError('SECTION_NOT_FOUND');

  await SectionModel.findByIdAndUpdate(sectionId, {
    $inc: { memberCount: -1 }
  });
};

const updateSeen = async (user, sectionId) => {
  const section = await UserSeenGroupModel.findOneAndUpdate({
    userId: user._id,
    groupId: sectionId
  }, {
    lastSeen: Date.now()
  });
  if (!section)
    throwError('SECTION_NOT_FOUND');
};

const getAllMembers = async sectionId => {
  const users = await UserSeenGroupModel.aggregate([
    {
      $match: {
        groupId: mongoose.Types.ObjectId(sectionId),
      }
    },
    {
      $lookup: {
        from: 'students',
        let: { uId: '$userId' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$_id', '$$uId'] },
                  { $ne: ['$isDeleted', true] }
                ]
              }
            }
          },
          {
            $project: User.project()
          }
        ],
        as: 'user'
      }
    },
    { $unwind: '$user' },
    {
      $group: {
        _id: '$groupId',
        users: { $addToSet: '$user' }
      }
    }
  ]);

  // ,{allowDiskUse : true}

  return users.length ? users[0].users : [];
};

// ========== Utilities =============
const save = SectionModel.create.bind(SectionModel);

const saveMembership = UserSeenGroupModel.create.bind(UserSeenGroupModel);

const addMembership = (userId, groupId) => saveMembership({ userId, groupId });

const getMembership = (userId, groupId) =>
  UserSeenGroupModel.findOne({ userId, groupId });

const project = () => ({
  _id: false,
  groupId: '$_id',
  title: '$course.name',
  section: true,
  course: true,
  university: true,
  memberCount: true,
  topics: true,
  createdAt: true,
  postCount: {
    $cond: ['$membership',
      {
        $size: {
          $filter: {
            input: '$posts',
            as: 'post',
            cond: {
              $and: [
                { $gt: ['$$post.createdAt', '$membership.lastSeen'] }
              ]
            }
          }
        }
      },
      { $size: '$posts' }]
  },
  newPostCount: {
    $cond: ['$membership',
      {
        $size: {
          $filter: {
            input: '$posts',
            as: 'post',
            cond: {
              $and: [
                { $gt: ['$$post.createdAt', '$membership.lastSeen'] }
              ]
            }
          }
        }
      },
      { $size: '$posts' }]
  },
  currentUserJoined: {
    $cond: ['$membership', true, false]
  }
});

module.exports = {
  add,
  getAllByCourse,
  getAll,
  get,
  join,
  leave,
  updateSeen,
  getAllMembers,
  save,
  saveMembership,
  addMembership,
  getMembership
};
