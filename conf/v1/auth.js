const passport = require('passport');
const BearerStrategy = require('passport-http-bearer').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const CookieStrategy = require('passport-cookie').Strategy;
const bcrypt = require('bcrypt');
const { UserModel, AdminModel } = require('../../models/v1');

// passport.serializeUser(function (user, done) {
//   done(null, user.id)
// })

// passport.deserializeUser(function (id, done) {
//   CustomerModel.findById(id, function (err, user) {
//     done(err, user)
//   })
// })


passport.use('user-bearer', new BearerStrategy(
  async (token, done) => {
    try {
      const user = await UserModel.findOne({ token });
      console.log('current user:', user);
      if (user) {
        return done(null, user, { scope: 'all' });
      }
      throw new Error('User not found');
    } catch (err) {
      done({ status: 401 });
    }
  }
));

passport.use('user-local', new LocalStrategy(
  {
    passReqToCallback: true,
    usernameField: 'username',
    passwordField: 'password',
    session: false
  },
  async (req, username, password, done) => {
    try {
      const email = req.body.email === '\0' ? '' : req.body.email;
      username = username === '\0' ? '' : username;
      const user = await UserModel.findOne({ $or: [{ email }, { username }] });
      if (!user) {
        return done({ status: 401 }, false);
      }
      const result = await bcrypt.compare(password, user.password);
      if (result) {
        return done(null, user);
      }
      throw new Error('Password does not match!');
    } catch (err) {
      done({ status: 401 }, false);
    }
  }
));

passport.use('admin-local', new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password',
    session: false
  },
  async (email, password, done) => {
    try {
      console.log('email:', email, 'password:', password);
      const admin = await AdminModel.findOne({ email });
      console.log('admin:', admin);
      if (!admin) {
        return done({ status: 401 }, false);
      }
      const result = await bcrypt.compare(password, admin.password);
      if (result) {
        return done(null, admin);
      }
      throw new Error('Password does not match!');
    } catch (err) {
      done({ status: 401 }, false);
    }
  }
));

passport.use('admin-cookie', new CookieStrategy(
  async (token, done) => {
    try {
      const admin = await AdminModel.findOne({ token });
      console.log('current admin:', admin);
      if (admin) {
        return done(null, admin);
      }
      throw new Error('Admin not found');
    } catch (err) {
      done({ status: 401 }, false);
    }
  }
));


module.exports = passport
