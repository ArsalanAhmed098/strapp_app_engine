const mongoose = require('mongoose');
const { PostModel, UserSeenGroupModel } = require('../../models/v1');
const { save } = require('../generic');

const getPosts = async (req, res) => {
  let posts = await PostModel.aggregate([
    {
      $match: {
        $and: [
          { userId: req.user._id },
          { isDeleted: { $ne: true } },
        ]
      }
    },
    {
      $project: {
        postId: '$_id',
        _id: false,
        likes: true,
        dislikes: true,
        isRestricted: true,
        isAnonymous: true,
        userId: true,
        groupId: true,
        content: true,
        topics: true,
        createdAt: true,
        commentCount: {
          $cond: { if: { $isArray: '$comments' }, then: { $size: '$comments' }, else: 0 }
        }
      }
    }
  ]);
  posts = posts.map(post => {
    post.currentUserLiked =
      (post.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
      (post.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
      0;
    post.likes = post.likes.length - post.dislikes.length;
    delete post.dislikes;
    return post;
  });
  res.status(200).json({
    success: true,
    data: posts,
    message: 'posts fetched successfully',
    errorCode: null,
  });
};

const createPost = async (req, res) => {
  req.body.content = {
    title: req.body.title,
    description: req.body.description
  };
  if (req.file) {
    req.body.content.photo = `/${req.file.destination}/${req.file.filename}`;
  }
  if (req.body.topics) {
    req.body.topics = req.body.topics.split(',').map(topic => topic.trim());
  }
  const group = await UserSeenGroupModel.findOne({
    userId: req.user._id,
    groupId: req.body.groupId
  });
  if (!group) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'group not fount',
      errorCode: 'GROUP_NOT_FOUND',
    });
  }
  const post = await save(PostModel, { userId: req.user._id, ...req.body });
  post._doc.postId = post._doc._id;
  delete post._doc._id;
  post._doc.currentUserLiked =
    (post._doc.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (post._doc.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
  delete post._doc.dislikes;
  res.status(200).json({
    success: true,
    data: post,
    message: 'post created successfully',
    errorCode: null,
  });
};

const getPost = async (req, res) => {
  let post = await PostModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.params.postId)
      }
    },
    {
      $lookup: {
        from: 'groups',
        localField: 'groupId',
        foreignField: '_id',
        as: 'group'
      }
    },
    { $unwind: '$group' },
    {
      $lookup: {
        from: 'users',
        localField: "comments.userId",
        foreignField: "_id",
        // let: { uId: '$comments.userId' },
        // pipeline: [
        //   {
        //     $match: {
        //       $expr: {
        //         $eq: ['$_id', '$$uId']
        //       }
        //     }
        //   }
        // ],
        as: 'users'
      }
    },
    {
      $project: {
        postId: '$_id',
        _id: false,
        likes: true,
        dislikes: true,
        isRestricted: true,
        isAnonymous: true,
        userId: true,
        group: {
          groupId: '$group._id',
          title: '$group.title'
        },
        content: true,
        comments: {
          $filter: {
            input: '$comments',
            as: 'comment',
            cond: {
              $ne: ['$$comment.isDeleted', true]
            }
          }
        },
        topics: true,
        createdAt: true,
        users: true
      }
    }
  ]);

  if (!post.length) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }
  post = post[0];
  if (post.isRestricted && post.userId.toString() !== req.user._id.toString() && !req.user.isVerified) {
    post = {
      isRestricted: post.isRestricted,
      postId: post.postId
    }
  } else {
    post.comments = post.comments ? post.comments.map(comment => {
      comment.commentId = comment._id;
      delete comment._id;

      comment.currentUserLiked =
        (comment.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
        (comment.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
        0;
      comment.likes = comment.likes.length - comment.dislikes.length;
      delete comment.dislikes;
      if (comment.isAnonymous) {
        comment.user = null;
      } else {
        const user = post.users.find(user => user._id.toString() === comment.userId.toString());
        comment.user = {
          userId: user._id,
          isVerified: user.isVerified,
          firstName: user.firstName,
          lastName: user.lastName,
          photo: user.photo,
          major: user.major,
          email: user.email,
          username: user.username,
        };
      }
      delete comment.userId;
      delete comment.reports;
      return comment;
    }) : [];
    // post.comments = (function makeTree(comments, parentId) {
    //   const newComments = comments.forEach(comment => {
    //     comment.children = getChildren(comment.children);
    //     return comment;
    //   });

    //   function getChildren(ids) {
    //     return comments
    //     .filter(({_id}) => ids.includes(_id))
    //     // .map(comment => {
    //     //   comment.children = getChildren(comment.children);
    //     //   return comment;
    //     // });
    //   }
    // })(post.comments || [], null);

    delete post.users;
    post.currentUserLiked =
      (post.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
      (post.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
      0;
    post.likes = post.likes.length - post.dislikes.length;
    delete post.dislikes;
  }
  res.status(200).json({
    success: true,
    data: post,
    message: 'post fetched successfully',
    errorCode: null,
  });
};

const updatePost = async (req, res) => {
  const post = await PostModel.findOneAndUpdate({
    userId: req.user._id,
    _id: req.params.postId,
  }, req.body, { new: true });
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }
  post._doc.postId = post._doc._id;
  delete post._doc._id;
  post._doc.currentUserLiked =
    (post._doc.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (post._doc.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
  delete post._doc.dislikes;
  delete post._doc.comments;
  res.status(200).json({
    success: true,
    data: post,
    message: 'Post updated successfully',
    errorCode: null,
  });
};

const reportPost = async (req, res) => {
  const { reasons } = req.body;
  const post = await PostModel.findById(req.params.postId);
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }
  reasons.forEach(reason => {
    if(!post.reports[reason])
      reason = 'other';
    if(!post.reports[reason].find(userId => userId.toString() === req.user._id.toString()))
      post.reports[reason].push(req.user._id);
  });

  await post.save();

  res.status(200).json({
    success: true,
    data: null,
    message: 'Post reported successfully',
    errorCode: null,
  });
};

const deletePost = async (req, res) => {
  const post = await PostModel.findOne({
    userId: req.user._id,
    _id: req.params.postId,
  });
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }
  await post.remove();
  res.status(200).json({
    success: true,
    data: null,
    message: 'post removed successfully',
    errorCode: null,
  });
};

const likePost = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(req.params.postId, {
    $pull: { dislikes: req.user._id },
    $addToSet: { likes: req.user._id }
  }, { new: true }).select('likes dislikes');
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }
  post._doc.postId = post._doc._id;
  delete post._doc._id;
  post._doc.currentUserLiked =
    (post._doc.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (post._doc.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
  delete post._doc.dislikes;
  res.status(200).json({
    success: true,
    data: post,
    message: 'post liked successfully',
    errorCode: null,
  });
};

const removeLikePost = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(req.params.postId, {
    $pull: { likes: req.user._id },
  }, { new: true }).select('likes dislikes');
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }
  post._doc.postId = post._doc._id;
  delete post._doc._id;
  post._doc.currentUserLiked =
    (post._doc.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (post._doc.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
  delete post._doc.dislikes;
  res.status(200).json({
    success: true,
    data: post,
    message: 'remove post liked successfully',
    errorCode: null,
  });
};

const dislikePost = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(req.params.postId, {
    $pull: { likes: req.user._id },
    $addToSet: { dislikes: req.user._id },
  }, { new: true }).select('likes dislikes');
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }
  post._doc.postId = post._doc._id;
  delete post._doc._id;
  post._doc.currentUserLiked =
    (post._doc.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (post._doc.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
  delete post._doc.dislikes;
  res.status(200).json({
    success: true,
    data: post,
    message: 'post disliked successfully',
    errorCode: null,
  });
};

const removeDislikePost = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(req.params.postId, {
    $pull: { dislikes: req.user._id },
  }, { new: true }).select('likes dislikes');
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }
  post._doc.postId = post._doc._id;
  delete post._doc._id;
  post._doc.currentUserLiked =
    (post._doc.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (post._doc.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
  delete post._doc.dislikes;
  res.status(200).json({
    success: true,
    data: post,
    message: 'remove post disliked successfully',
    errorCode: null,
  });
};

module.exports = {
  getPosts,
  createPost,
  getPost,
  updatePost,
  reportPost,
  deletePost,
  likePost,
  removeLikePost,
  dislikePost,
  removeDislikePost,
};
