const { Post } = require('../../services/v2');

const add = async (req, res) => {
  req.body.groupId = req.params.sectionId;
  req.body.content = {
    title: req.body.title,
    description: req.body.description,
    photos: []
  };
  for(let file of req.files) {
    req.body.content.photos.push(`/${file.destination}/${file.filename}`);
  }
  if (req.body.topics) {
    req.body.topics = req.body.topics.split(',').map(topic => topic.trim());
  }
  const post = await Post.add(req.user, req.body);
  res.success(post);
};

const get = async (req, res) => {
  const post = await Post.get(req.user, req.params.postId);
  res.success(post);
};

const getAll = async (req, res) => {
  const posts = await Post.getAll(req.user, req.params.sectionId, req.query);
  res.success(posts);
};

const getAllByTopic = async (req, res) => {
  const posts = await Post.getAllByTopic(req.user, req.params.sectionId, req.query);
  res.success(posts);
};

const update = async (req, res) => {
  const post = await Post.update(req.user, req.params.postId, req.body);
  res.success(post);
};

const remove = async (req, res) => {
  await Post.remove(req.user, req.params.postId);
  res.success();
};

const report = async (req, res) => {
  await Post.report(req.user, req.params.postId, req.body.reasons);
  res.success();
};

const like = async (req, res) => {
  const post = await Post.like(req.user, req.params.postId);
  res.success(post);
};

const unlike = async (req, res) => {
  const post = await Post.unlike(req.user, req.params.postId);
  res.success(post);
};

const dislike = async (req, res) => {
  const post = await Post.dislike(req.user, req.params.postId);
  res.success(post);
};

const undislike = async (req, res) => {
  const post = await Post.undislike(req.user, req.params.postId);
  res.success(post);
};

module.exports = {
  add,
  get,
  getAll,
  getAllByTopic,
  update,
  remove,
  report,
  like,
  unlike,
  dislike,
  undislike,
};
