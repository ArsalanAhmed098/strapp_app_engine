const User = require('./User');
const Auth = require('./Auth');
const University = require('./University');
const Notification = require('./Notification');
const Course = require('./Course');
const Section = require('./Section');
const Post = require('./Post');
const Comment = require('./Comment');
const Feed = require('./Feed');
const Admin = require('./Admin');

module.exports = {
  User,
  Auth,
  University,
  Notification,
  Course,
  Section,
  Post,
  Comment,
  Feed,
  Admin
};
