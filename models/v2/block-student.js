const mongoose = require('mongoose');

const BlockStudentSchema = new mongoose.Schema({
  blockerUser: mongoose.Schema.Types.ObjectId,
  blockedUser: mongoose.Schema.Types.ObjectId,
  isAnonymousPost: {
    type: Boolean,
    default: false
  },
  blockDate: {
    type: Date,
    default: Date.now
  },
}, {
  timestamps: true
});

module.exports = mongoose.model('BlockStudent', BlockStudentSchema);
