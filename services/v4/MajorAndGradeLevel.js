const mongoose = require('mongoose');
const { GradeLevelsModel,MajorsModel, } = require('../../models/v2');
const User = require('./User');

const getAll = async (user) => {

    let data1 =[];
    let data2 =[];
    let gradeLevel = await GradeLevelsModel.find({
    },{"gradelevel.title": 1, contribs: 1});
  
    if (!gradeLevel)
      throwError('gradelevel_NOT_FOUND');

      gradeLevel.forEach(async(data) => {
        // console.log('ye rha',data.gradelevel.title);
        data1.push(data.gradelevel.title);
        })

    let major = await MajorsModel.find({
    },{ 'major.title': 1, contribs: 1});
  
    if (!major)
      throwError('major_NOT_FOUND');

      major.forEach(async(data) => {
        // console.log('ye rha',data.major.title);
        data2.push(data.major.title);  
      })
  
    // return '['+data1+']','['+data2+']';
   return {"grade_level":data1,"major":data2};
    // return gradeLevel,major;
  };
  


module.exports = {
    getAll
    };