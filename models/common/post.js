const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  photos: [String],
  likes: [mongoose.Schema.Types.ObjectId],
  dislikes: [mongoose.Schema.Types.ObjectId],
  isRestricted: {
    type: Boolean,
    default: true
  },
  isAnonymous: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  parentId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  reports: {
    'abuse': [mongoose.Schema.Types.ObjectId],
    'recism': [mongoose.Schema.Types.ObjectId],
    'cheating': [mongoose.Schema.Types.ObjectId],
    'bad language': [mongoose.Schema.Types.ObjectId],
    'spam': [mongoose.Schema.Types.ObjectId],
    'fruad': [mongoose.Schema.Types.ObjectId],
    'illegal activity': [mongoose.Schema.Types.ObjectId],
    'other': [mongoose.Schema.Types.ObjectId],
  },
  children: [{
    type: mongoose.Schema.Types.ObjectId,
  }],
  createdAt: { type: Date, default: Date.now }
},
 {
  timestamps: true
});

const PostSchema = new mongoose.Schema({
  groupId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  content: {
    title: {
      type: String,
      required: true,
      // validate: {
      //   validator: title => title.length <= 70,
      //   message: props => `${props.value} must be less than or equal to 70 characters`
      // },
    },
    description: {
      type: String,
    },
    photos: [String],
  },
  likes: [mongoose.Schema.Types.ObjectId],
  dislikes: [mongoose.Schema.Types.ObjectId],
  isRestricted: {
    type: Boolean,
    default: true
  },
  isAnonymous: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  isPin: {
    type: Boolean,
    default: false
  },
  isNotified: {
    type: Boolean,
    default: false
  },
  isPinCreatedAt: { 
    type: Date,
    default: 0 
  },
  isPinUpdatedAt: { 
    type: Date,
    default: 0 
  },
  reports: {
    'abuse': [mongoose.Schema.Types.ObjectId],
    'recism': [mongoose.Schema.Types.ObjectId],
    'cheating': [mongoose.Schema.Types.ObjectId],
    'bad language': [mongoose.Schema.Types.ObjectId],
    'spam': [mongoose.Schema.Types.ObjectId],
    'fruad': [mongoose.Schema.Types.ObjectId],
    'illegal activity': [mongoose.Schema.Types.ObjectId],
    'other': [mongoose.Schema.Types.ObjectId],
  },
  topics: [{
    type: String,
  }],
  comments: [CommentSchema]
}, {
  timestamps: true
});

PostSchema.pre('save', { document: true }, async function () {
  await mongoose.model('Section').findByIdAndUpdate(this.groupId, {
    $addToSet: { topics: this.topics }
  });
});

module.exports = mongoose.model('Post', PostSchema);
