const bcrypt = require('bcrypt');
const genRandom = (start, end) => start + Math.floor(Math.random() * (end - start));
const password = bcrypt.hashSync('12345678', 8);
const getPhoto = () => [
  '/uploads/profiles/photo-1.jpg',
  '/uploads/profiles/photo-2.jpg',
  '/uploads/profiles/photo-3.jpg',
  '/uploads/profiles/photo-4.jpg',
  '/uploads/profiles/photo-5.jpg',
  '/uploads/profiles/photo-6.jpg',
  '/uploads/profiles/photo-7.jpg',
  '/uploads/profiles/photo-8.jpg',
  '/uploads/profiles/photo-9.jpg',
  '/uploads/profiles/photo-10.jpg'
][genRandom(0, 10)];

const universities = [
  {
    name: 'Marywood University',
    domain: 'marywood.edu',
  },
  {
    name: 'Cégep de Saint-Jérôme',
    domain: 'cstj.qc',
  },
  {
    name: 'Lindenwood University',
    domain: 'lindenwood.edu',
  },
  {
    name: 'DAV Institute of Engineering & Technology',
    domain: 'davietjal.org',
  },
  {
    name: 'Lovely Professional University',
    domain: 'lpu.in',
  },
  {
    name: 'Sullivan University',
    domain: 'sullivan.edu',
  },
  {
    name: 'Florida State College at Jacksonville',
    domain: 'fscj.edu',
  },
  {
    name: 'Xavier University',
    domain: 'xavier.edu',
  },
  {
    name: 'Tusculum College',
    domain: 'tusculum.edu',
  },
  {
    name: 'Claremont School of Theology',
    domain: 'cst.edu',
  },
  {
    name: 'Somaiya Vidyavihar',
    domain: 'somaiya.edu',
  },
  {
    name: 'Columbia College (SC)',
    domain: 'columbiasc.edu',
  },
  {
    name: 'Chabot-Las Positas Community College District',
    domain: 'clpccd.edu',
  },
  {
    name: 'Keller Graduate School of Management',
    domain: 'keller.edu',
  },
  {
    name: 'Monroe College',
    domain: 'monroecollege.edu',
  },
  {
    name: 'San Mateo County Community College District',
    domain: 'smccd.edu',
  },
  {
    name: 'Los Rios Community College District',
    domain: 'losrios.edu',
  },
  {
    name: 'DigiPen Institute of Technology',
    domain: 'digipen.edu',
  },
  {
    name: 'University of Pittsburgh Medical Center',
    domain: 'upmc.edu',
  },
  {
    name: 'Claremont Graduate University',
    domain: 'cgu.edu',
  },
];
const majors = [
  'Computer Sciences',
  'Literature',
  'Space Sciences',
  'Data Sciences',
  'Economics',
  'Medicine',
  'Philosophy',
  'Linguistics',
  'Divinity',
  'Mathematics'
];
const persons = [{
  "firstName": "Alanah",
  "lastName": "Clowney",
  "username": "aclowney0"
}, {
  "firstName": "Taddeo",
  "lastName": "Bogart",
  "username": "tbogart1"
}, {
  "firstName": "Cathyleen",
  "lastName": "Blench",
  "username": "cblench2"
}, {
  "firstName": "Yurik",
  "lastName": "Skillings",
  "username": "yskillings3"
}, {
  "firstName": "Bev",
  "lastName": "Beceril",
  "username": "bbeceril4"
}, {
  "firstName": "Dukey",
  "lastName": "Redwood",
  "username": "dredwood5"
}, {
  "firstName": "Lira",
  "lastName": "Lenz",
  "username": "llenz6"
}, {
  "firstName": "Sherye",
  "lastName": "Benedit",
  "username": "sbenedit7"
}, {
  "firstName": "Bord",
  "lastName": "Franckton",
  "username": "bfranckton8"
}, {
  "firstName": "Giordano",
  "lastName": "Beentjes",
  "username": "gbeentjes9"
}, {
  "firstName": "Rey",
  "lastName": "Habens",
  "username": "rhabensa"
}, {
  "firstName": "Vi",
  "lastName": "Pietrasik",
  "username": "vpietrasikb"
}, {
  "firstName": "Ranna",
  "lastName": "Gossipin",
  "username": "rgossipinc"
}, {
  "firstName": "Chelsae",
  "lastName": "Lowfill",
  "username": "clowfilld"
}, {
  "firstName": "Alie",
  "lastName": "Loyd",
  "username": "aloyde"
}, {
  "firstName": "Darrelle",
  "lastName": "Ferns",
  "username": "dfernsf"
}, {
  "firstName": "Jackie",
  "lastName": "Hitzmann",
  "username": "jhitzmanng"
}, {
  "firstName": "Dasya",
  "lastName": "Noden",
  "username": "dnodenh"
}, {
  "firstName": "Wait",
  "lastName": "Terrelly",
  "username": "wterrellyi"
}, {
  "firstName": "Christoffer",
  "lastName": "Couper",
  "username": "ccouperj"
}, {
  "firstName": "Reese",
  "lastName": "Kean",
  "username": "rkeank"
}, {
  "firstName": "Josey",
  "lastName": "Clibbery",
  "username": "jclibberyl"
}, {
  "firstName": "Angelique",
  "lastName": "Pechan",
  "username": "apechanm"
}, {
  "firstName": "Trula",
  "lastName": "Flear",
  "username": "tflearn"
}, {
  "firstName": "Raynell",
  "lastName": "Hugonet",
  "username": "rhugoneto"
}, {
  "firstName": "Tobin",
  "lastName": "Lemmers",
  "username": "tlemmersp"
}, {
  "firstName": "Heidi",
  "lastName": "Bowles",
  "username": "hbowlesq"
}, {
  "firstName": "Bren",
  "lastName": "Portchmouth",
  "username": "bportchmouthr"
}, {
  "firstName": "Jessika",
  "lastName": "Graben",
  "username": "jgrabens"
}, {
  "firstName": "Hunt",
  "lastName": "Bennoe",
  "username": "hbennoet"
}, {
  "firstName": "Yoshiko",
  "lastName": "Jeanneau",
  "username": "yjeanneauu"
}, {
  "firstName": "Nannette",
  "lastName": "Greene",
  "username": "ngreenev"
}, {
  "firstName": "Adam",
  "lastName": "Weatherdon",
  "username": "aweatherdonw"
}, {
  "firstName": "Vanessa",
  "lastName": "Boydell",
  "username": "vboydellx"
}, {
  "firstName": "Ede",
  "lastName": "Cappell",
  "username": "ecappelly"
}, {
  "firstName": "Keene",
  "lastName": "Byass",
  "username": "kbyassz"
}, {
  "firstName": "Asher",
  "lastName": "Colbertson",
  "username": "acolbertson10"
}, {
  "firstName": "Findlay",
  "lastName": "Pelz",
  "username": "fpelz11"
}, {
  "firstName": "Caroljean",
  "lastName": "Tembey",
  "username": "ctembey12"
}, {
  "firstName": "Caresse",
  "lastName": "Busek",
  "username": "cbusek13"
}, {
  "firstName": "Mariquilla",
  "lastName": "Lawranson",
  "username": "mlawranson14"
}, {
  "firstName": "Malachi",
  "lastName": "Jerson",
  "username": "mjerson15"
}, {
  "firstName": "Joleen",
  "lastName": "Schulze",
  "username": "jschulze16"
}, {
  "firstName": "Celestine",
  "lastName": "Nattriss",
  "username": "cnattriss17"
}, {
  "firstName": "Allis",
  "lastName": "Broadfield",
  "username": "abroadfield18"
}, {
  "firstName": "Kylie",
  "lastName": "Skells",
  "username": "kskells19"
}, {
  "firstName": "Bastien",
  "lastName": "Mauger",
  "username": "bmauger1a"
}, {
  "firstName": "Huey",
  "lastName": "Berthomieu",
  "username": "hberthomieu1b"
}, {
  "firstName": "Romain",
  "lastName": "Axell",
  "username": "raxell1c"
}, {
  "firstName": "Addison",
  "lastName": "Kilfedder",
  "username": "akilfedder1d"
}, {
  "firstName": "Read",
  "lastName": "Elizabeth",
  "username": "relizabeth1e"
}, {
  "firstName": "Robinett",
  "lastName": "Masedon",
  "username": "rmasedon1f"
}, {
  "firstName": "Faustina",
  "lastName": "Morrice",
  "username": "fmorrice1g"
}, {
  "firstName": "Hersh",
  "lastName": "Isacke",
  "username": "hisacke1h"
}, {
  "firstName": "Cathrin",
  "lastName": "Clear",
  "username": "cclear1i"
}, {
  "firstName": "Ellie",
  "lastName": "Benaine",
  "username": "ebenaine1j"
}, {
  "firstName": "Emmalee",
  "lastName": "Knightsbridge",
  "username": "eknightsbridge1k"
}, {
  "firstName": "Silvain",
  "lastName": "Rizzetti",
  "username": "srizzetti1l"
}, {
  "firstName": "Brandy",
  "lastName": "Ferrarese",
  "username": "bferrarese1m"
}, {
  "firstName": "Ignacius",
  "lastName": "Wharmby",
  "username": "iwharmby1n"
}, {
  "firstName": "Rhianon",
  "lastName": "Kenealy",
  "username": "rkenealy1o"
}, {
  "firstName": "Nesta",
  "lastName": "Laurentino",
  "username": "nlaurentino1p"
}, {
  "firstName": "Steven",
  "lastName": "Agate",
  "username": "sagate1q"
}, {
  "firstName": "Kettie",
  "lastName": "Wisbey",
  "username": "kwisbey1r"
}, {
  "firstName": "Korry",
  "lastName": "Le Leu",
  "username": "kleleu1s"
}, {
  "firstName": "Loise",
  "lastName": "Tart",
  "username": "ltart1t"
}, {
  "firstName": "Carolus",
  "lastName": "Knaggs",
  "username": "cknaggs1u"
}, {
  "firstName": "Iggie",
  "lastName": "Farden",
  "username": "ifarden1v"
}, {
  "firstName": "Giusto",
  "lastName": "Tomenson",
  "username": "gtomenson1w"
}, {
  "firstName": "Jeanne",
  "lastName": "Blaisdell",
  "username": "jblaisdell1x"
}, {
  "firstName": "Benoit",
  "lastName": "Belderfield",
  "username": "bbelderfield1y"
}, {
  "firstName": "Terza",
  "lastName": "Santostefano.",
  "username": "tsantostefano1z"
}, {
  "firstName": "Ulrika",
  "lastName": "Gabotti",
  "username": "ugabotti20"
}, {
  "firstName": "Anne-marie",
  "lastName": "Mansion",
  "username": "amansion21"
}, {
  "firstName": "Phebe",
  "lastName": "Appleton",
  "username": "pappleton22"
}, {
  "firstName": "Jarret",
  "lastName": "Fitzsymon",
  "username": "jfitzsymon23"
}, {
  "firstName": "Xenia",
  "lastName": "Lappin",
  "username": "xlappin24"
}, {
  "firstName": "Emiline",
  "lastName": "Pagin",
  "username": "epagin25"
}, {
  "firstName": "Ginger",
  "lastName": "Wartonby",
  "username": "gwartonby26"
}, {
  "firstName": "Bern",
  "lastName": "Faint",
  "username": "bfaint27"
}, {
  "firstName": "Currie",
  "lastName": "Petrelli",
  "username": "cpetrelli28"
}, {
  "firstName": "Tommie",
  "lastName": "Gonning",
  "username": "tgonning29"
}, {
  "firstName": "Archaimbaud",
  "lastName": "Minihan",
  "username": "aminihan2a"
}, {
  "firstName": "Troy",
  "lastName": "Warkup",
  "username": "twarkup2b"
}, {
  "firstName": "Berta",
  "lastName": "McCrie",
  "username": "bmccrie2c"
}, {
  "firstName": "Janis",
  "lastName": "Errichi",
  "username": "jerrichi2d"
}, {
  "firstName": "Dyan",
  "lastName": "Dawltrey",
  "username": "ddawltrey2e"
}, {
  "firstName": "Tess",
  "lastName": "Heintzsch",
  "username": "theintzsch2f"
}, {
  "firstName": "Beatrice",
  "lastName": "Neeves",
  "username": "bneeves2g"
}, {
  "firstName": "Ferd",
  "lastName": "Doulden",
  "username": "fdoulden2h"
}, {
  "firstName": "Elnar",
  "lastName": "Iston",
  "username": "eiston2i"
}, {
  "firstName": "Kellby",
  "lastName": "Kilgour",
  "username": "kkilgour2j"
}, {
  "firstName": "Philly",
  "lastName": "Kmietsch",
  "username": "pkmietsch2k"
}, {
  "firstName": "Vania",
  "lastName": "Foxcroft",
  "username": "vfoxcroft2l"
}, {
  "firstName": "Sherye",
  "lastName": "Allbrook",
  "username": "sallbrook2m"
}, {
  "firstName": "Yehudi",
  "lastName": "Craythorne",
  "username": "ycraythorne2n"
}, {
  "firstName": "Nicola",
  "lastName": "Haddinton",
  "username": "nhaddinton2o"
}, {
  "firstName": "Rinaldo",
  "lastName": "MacRory",
  "username": "rmacrory2p"
}, {
  "firstName": "Gian",
  "lastName": "Hallan",
  "username": "ghallan2q"
}, {
  "firstName": "Dominick",
  "lastName": "Howell",
  "username": "dhowell2r"
}];

const userBlock = {
  ['WRITE_BLOCKED'](user) {
    user.block.status = 'WRITE_BLOCKED';
    user.block.deniedRoutes = [
      {
        path: 'posts',
        method: 'POST'
      },
      {
        path: 'comments',
        method: 'POST'
      }
    ];
    return user;
  },
  ['TOTAL_BLOCKED'](user) {
    user.block.status = 'TOTAL_BLOCKED';
    user.block.deniedRoutes = [
      {
        path: '*',
        method: '*'
      }
    ];
    return user;
  },
};

module.exports = persons.map((user, index) => {
  user.password = password;
  const uni = universities[index % universities.length];
  user.email = user.username + '@' + uni.domain;
  user.university = uni;
  if (index % 2) {
    user.major = majors[index % majors.length];
  }
  if (!(index % 5)) {
    user.isVerified = true;
  }
  if (!(index % 10)) {
    user.block = {};
    user = userBlock[['WRITE_BLOCKED', 'TOTAL_BLOCKED'][Math.floor(Math.random() * 2)]](user);
  }
  if(!(index % 4)) {
    user.photo = getPhoto();
  }
  return user;
});



