const { genRandom } = require('../../util');

const words = `remind abnormal confront star minimize carry drift murder race please gradient behead husband assault authority hero brown slump poll workshop fault swallow extinct volume present mail storage wording cooperative laser bother meet means other night implicit act pair modest regular center sickness mouse legislation broadcast available customer legislature pony trainer`.split(' ');
const getWords = (start, end) => {
  const text = [];
  const wordCount = genRandom(start, end);
  const newStart = genRandom(1, wordCount);
  const newEnd = newStart + wordCount;
  for (let i = newStart; i < newEnd; i++) {
    text.push(words[i % words.length]);
  }
  return text.join(' ');
};
const getPhotos = () => {
  const photoList = [
    '/uploads/20200714/photo-1.jpg',
    '/uploads/20200714/photo-2.jpg',
    '/uploads/20200714/photo-3.jpg',
    '/uploads/20200714/photo-4.jpg',
    '/uploads/20200714/photo-5.jpg',
    '/uploads/20200714/photo-6.jpg',
    '/uploads/20200714/photo-7.jpg',
    '/uploads/20200714/photo-8.jpg',
    '/uploads/20200714/photo-9.jpg',
    '/uploads/20200714/photo-10.jpg',
    '/uploads/20200714/photo-11.jpg',
    '/uploads/20200714/photo-12.jpg',
    '/uploads/20200714/photo-13.jpg',
    '/uploads/20200714/photo-14.jpg',
  ];
  const photoCount = genRandom(0, photoList.length);
  const photos = new Set();
  for(let i = 0; i < photoCount; i++) {
    photos.add(photoList[genRandom(0, photoList.length)]);
  }
  return [...photos];
};
const randBoolean = () => [true, false][genRandom(0, 2)];
const getTopics = () => {
  const returnedTopics = new Set();
  const topics = ['general', 'notes', 'homework', 'advice', 'tips'];
  const topicCount = genRandom(0, topics.length);
  for(let i = 0; i < topicCount; i++) {
    returnedTopics.add(topics[genRandom(0, topics.length)]);
  }
  return [...returnedTopics];
};
const genComments = (userId, maxCount) => {
  const commentCount = genRandom(1, maxCount);
  const comments = [];
  for(let i =0; i < commentCount; i++)  {
    const comment = {
      userId,
      description: getWords(10, 20),
      isRestricted: randBoolean(),
      isAnonymous: randBoolean(),
    };
    comments.push(comment);
  }
  return comments;
};

const genPosts = (userId, group, maxCount) => {
  const postCount = genRandom(1, maxCount);
  const posts = [];
  for (let i = 0; i < postCount; i++) {
    const post = {
      userId,
      groupId: group._id,
      content: {
        title: getWords(3, 10),
        description: getWords(10, 30),
        photos: getPhotos(),
      },
      isRestricted: randBoolean(),
      isAnonymous: randBoolean(),
      topics: getTopics(),
      likes: [],
      dislikes: [],
      comments: [],
    };

    for (let otherUser of group.users) {
      if(otherUser._id.toString() !== userId.toString()) {
        const isLike = genRandom(0, 2);
        if(isLike) {
          post.likes.push(otherUser._id);
        } else {
          post.dislikes.push(otherUser._id);
        }
      }
      post.comments.push(...genComments(otherUser._id, 3));
    }

    posts.push(post);
  }
  return posts;
};

module.exports = genPosts;

