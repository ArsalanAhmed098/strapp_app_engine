const uuid4 = require('uuid/v4')
const { MongooseQueryParser } = require('mongoose-query-parser')
const qParser = new MongooseQueryParser()
const asyncHandler = require('../util/async-handler');

async function save(Model, data) {
  const obj = new Model(data)
  const result = await obj.save()
  return result
}

async function find(Model, req) {
  const { filter, sort, skip, limit } = qParser.parse(req.query)
  const result = await Model.find(filter).sort(sort).skip(skip).limit(limit)
  return result
}

async function findQueryWithCount(Model, filter, select, req) {
  const { sort, skip, limit } = qParser.parse(req.query)
  const query = Model.find(filter).sort(sort).skip(skip).limit(limit).select(select)
  const count = Model.countDocuments(filter)
  return Promise.all([query, count]).then((response) => {
    return { entries: response[0], count: response[1] }
  }).catch(() => { })
}

async function updateOne(Model, query, mod) {
  const doc = await Model.updateOne(query, mod)
  return doc
}

async function deleteMany(Model, filter) {
  const result = await Model.deleteMany(filter)
  return result
}

async function duplicateUsernameOrEmail(Model, { email, username }) {
  const foundByUsernameOrEmail = await Model.findOne({
    $or: [ { email }, { username }]
  });
  if (foundByUsernameOrEmail) {
    const error = {};
    if (foundByUsernameOrEmail.email === email) {
      error.email = ['This email is registered before.'];
    }
    if (foundByUsernameOrEmail.username === username) {
      error.username = ['This username is registered before.'];
    }
    return error;
  }
  return false;
}

async function login(req, res) {
  if(!req.user.token)
    req.user.token = uuid4()
  await req.user.save()
  req.user.password = undefined;
  req.user.confirmation = undefined;
  req.user.notification.firebaseTokens = undefined;
  req.user.block.deniedRoutes = undefined;
  return res.status(200).json({
    data: {
      user: req.user,
    },
    success: true,
    message: 'user logged in successfully',
    errorCode: null
  });
}

async function logout(req, res) {
  req.user.token = '';
  req.user.notification.firebaseTokens = '';
  await req.user.save();
  res.status(200).json({
    message: 'user logged out successfully'
  });
}

function currentUser(req, res) {
  req.user.password = undefined;
  req.user.confirmation = undefined;
  req.user.notification.firebaseTokens = undefined;
  req.user.block.deniedRoutes = undefined;
  res.status(200).json({
    data: {
      user: req.user,
    },
    success: true,
    message: 'logged in user',
    errorCode: null
  });
}

function getRandomInt(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

async function isUniqueness(Model, req) {
  if(req.body.email && req.body.email.split('@')[1] !== 'umich.edu') {
    return true;
  }
  return !!(await Model.findOne(req.body));
}

module.exports = asyncHandler({
  save,
  find,
  findQueryWithCount,
  updateOne,
  deleteMany,
  duplicateUsernameOrEmail,
  logout,
  login,
  currentUser,
  isUniqueness
});
