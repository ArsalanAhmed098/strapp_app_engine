const mongoose = require('mongoose');

const UserSeenGroupSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  groupId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  campusgroupId: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
    // required: true,
  },

  lastSeen: {
    type: Date,
    default: Date.now
  }
}, {
  timestamps: true
});

// UserSeenGroupSchema.index({ userId: 1, groupIds: 1}, { unique: true });

module.exports = mongoose.model('UserSeenGroup', UserSeenGroupSchema);
