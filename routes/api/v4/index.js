const router        = require('express').Router();
const authRouter    = require('./auth');
const userRouter    = require('./user');
const courseRouter  = require('./course');
const sectionRouter = require('./section');
const postRouter    = require('./post');
const feedRouter    = require('./feed');
const campusgroupRouter = require('./campusgroup');
const MajorAndGradeLevelRouter = require('./MajorAndGradeLevel');

router.use('/auth',     authRouter);
router.use('/user',     userRouter);
router.use('/courses',  courseRouter);
router.use('/sections', sectionRouter);
router.use('/posts',    postRouter);
router.use('/feed',     feedRouter);
router.use('/campusgroups', campusgroupRouter);
router.use('/MajorAndGradeLevels', MajorAndGradeLevelRouter);

module.exports = router;
