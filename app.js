const createError = require('http-errors');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const routes = require('./routes');
const { responseErrorHandler } = require('./middleware');
const app = express();


var flash = require('connect-flash');
var session = require('express-session');
var expressValidator = require('express-validator');
app.use(expressValidator());


app.use(bodyParser.json()); 
app.use(express.static('uploads'));
// app.use('/public/images/', express.static('./public/images'));
app.use(bodyParser.urlencoded({ 
    extended: false
}));

app.use(cors());

app.use(session({ secret: 'zxcv' })); // session secret
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(cookieParser());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false  }));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));



app.use('/', routes);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(responseErrorHandler);
module.exports = app;