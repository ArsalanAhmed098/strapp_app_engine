const genExpire = hour => {
  const expire = new Date();
  expire.setHours(expire.getHours() + hour);
  return expire;
};

module.exports = genExpire;
