const mongoose = require('mongoose');
const Promise = require("bluebird");
const debug = require('debug')('strapp:server');
function mongooseConf() {
  Promise.promisifyAll(mongoose);
  mongoose.set('useFindAndModify', false)
  mongoose.set('useCreateIndex', true)
  mongoose.set('useNewUrlParser', true)
  mongoose.set('useUnifiedTopology', true)
  mongoose.set('debug', true)
  mongoose
    // .connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DB}`, {
    //   useNewUrlParser: true
    // })
    // .connect(`mongodb+srv://root_user:JShXK3NmWOwy5pug@cluster0.9ktgt.mongodb.net/strapp-new`)
    .connect(`mongodb+srv://root_user:JShXK3NmWOwy5pug@cluster0.9ktgt.mongodb.net/strapp-new?retryWrites=true&w=majority`, {
      useNewUrlParser: true
    })
    .then(() => {
      debug('mongo connection was successful')
      // return CounterModel.findOneAndUpdate({ record: 1 }, { record: 1 }, { upsert: true, new: true, setDefaultsOnInsert: true })
    })
    .catch(err => console.error('mongod err', err))
}
mongooseConf()
