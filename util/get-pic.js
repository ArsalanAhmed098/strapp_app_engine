const path = require('path');
const sanitize = require('sanitize-filename');

const getPic = function (req, res, next) {
  const fileName = sanitize(req.params.file);
  const folderName = sanitize(req.params.folder);
  if (!fileName || !folderName) {
    throw new Error('valid file name required');
  }
  res.download(path.join('uploads', folderName, fileName), fileName, function (err) {
    if (err) {
      next(err);
    }
  });
};

module.exports = getPic;
