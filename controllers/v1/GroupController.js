const mongoose = require('mongoose');
const { GroupModel, UserSeenGroupModel, PostModel } = require('../../models/v1');
const { find, save } = require('../generic');

const getGroups = async (req, res) => {
  const groups = await UserSeenGroupModel.aggregate([
    { $match: { userId: req.user._id } },
    {
      $lookup: {
        from: 'groups',
        let: { gId: '$groupId' },
        pipeline: [{
          $match: {
            $expr: {
              $and: [
                { $eq: ['$_id', '$$gId'] },
                { $ne: ['$isDeleted', true] }
              ]
            }
          }
        }],
        as: 'group'
      }
    },
    { $unwind: '$group' },
    {
      $lookup: {
        from: 'posts',
        let: { gId: '$groupId', lastSeen: '$lastSeen' },
        pipeline: [{
          $match: {
            $expr: {
              $and: [
                { $eq: ['$groupId', '$$gId'] },
                { $gte: ['$createdAt', '$$lastSeen'] },
                { $ne: ['$isDeleted', true] }
              ]
            }
          }
        },
        {
          $group: {
            _id: '$groupId',
            newPostCount: { $sum: 1 }
          }
        },
        {
          $project: { _id: false }
        },
        ],
        as: 'posts'
      }
    },
    {
      $unwind: {
        path: '$posts',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project: {
        _id: false,
        'groupId': '$group._id',
        university: '$group.university.name',
        lastSeen: true,
        'title': '$group.title',
        'memberCount': '$group.memberCount',
        newPostCount: { $ifNull: ['$posts.newPostCount', 0] }
      }
    },
  ]);
  res.status(200).json({
    success: true,
    data: groups,
    message: `Fetch user's group list successfully`,
  });
};

const autoJoinGroup = async (req, res) => {
  const response = await GroupModel.autoJoin(req);
  return res.status(200).json({
    success: true,
    data: response.data,
    message: response.message,
  });
};

const parseQuery = query => {
  const normalizeQuery = Object.entries(query).map(([key, value]) => `${key}=${value}`);
  const parsedQuery = normalizeQuery.reduce((acc, curr) => {
    if (curr.startsWith('read')) {
      const { limit = -1, skip = 0 } = Object.fromEntries(curr
        .split('read')[1]
        .replace(/(\]|\[)/g, '')
        .split(',')
        .reduce((acc, curr) => {
          const [key, value] = curr.split('=');
          acc.push([key, +value]);
          return acc;
        }, []));
      acc.read = { limit, skip };
    } else if (curr.startsWith('unread')) {
      const { limit = -1, skip = 0 } = Object.fromEntries(curr
        .split('unread')[1]
        .replace(/(\]|\[)/g, '')
        .split(',')
        .reduce((acc, curr) => {
          const [key, value] = curr.split('=');
          acc.push([key, +value]);
          return acc;
        }, []));
      acc.unread = { limit, skip };
    } else if (curr.startsWith('topic')) {
      acc.topic = curr.split('=')[1];
    }
    return acc;
  }, {
    read: {
      limit: -1,
      skip: 0
    },
    unread: {
      limit: -1,
      skip: 0
    },
    topic: '',
  });
  return parsedQuery;
};

const genPostPipeline = filterQuery => postType => {
  const pipeline = [{
    $sort: { createdAt: postType === 'read' ? -1 : 1 }
  },
  {
    $match: {
      $expr: {
        $and: [
          { $eq: ['$groupId', '$$gId'] },
          { $ne: ['$isDeleted', true] },
          postType === 'read' ? { $lte: ['$createdAt', '$$lastSeen'] } : { $gt: ['$createdAt', '$$lastSeen'] }
        ]
      }
    }
  },
  ];
  filterQuery[postType].skip > 0 && pipeline.push({ $skip: filterQuery[postType].skip });
  filterQuery[postType].limit > 0 && pipeline.push({ $limit: filterQuery[postType].limit });
  pipeline.push({
    $lookup: {
      from: 'users',
      let: {
        uId: '$userId',
      },
      pipeline: [{
        $match: {
          $expr: {
            $eq: ['$_id', '$$uId']
          }
        }
      },
      {
        $project: {
          userId: '$_id',
          _id: false,
          firstName: true,
          lastName: true,
          photo: true,
          major: true,
          email: true,
          username: true,
          isVerified: true,
        }
      }
      ],
      as: 'user'
    }
  },
    { $unwind: '$user' },
    {
      $lookup: {
        from: 'users',
        localField: "comments.userId",
        foreignField: "_id",
        as: 'commentedUsers'
      }
    },
    {
      $project: {
        postId: '$_id',
        _id: false,
        user: true,
        likes: true,
        dislikes: true,
        isRestricted: true,
        isAnonymous: true,
        content: true,
        topics: true,
        comments: {
          $filter: {
            input: '$comments',
            as: 'comment',
            cond: {
              $ne: ['$$comment.isDeleted', true]
            }
          }
        },
        commentedUsers: true,
        commentCount: {
          $cond: {
            if: { $isArray: '$comments' }, then: {
              $size: {
                $filter: {
                  input: '$comments',
                  as: 'comment',
                  cond: {
                    $ne: ['$$comment.isDeleted', true]
                  }
                }
              }
            }, else: 0
          }
        },
        createdAt: true,
      }
    });

  return filterQuery[postType].limit ?
    pipeline : [{
      $match: {
        $expr: false
      }
    }];

};

const getGroup = async (req, res) => {
  const postPipeline = genPostPipeline(parseQuery(req.query));
  let group = await GroupModel.aggregate([{
    $match: {
      _id: mongoose.Types.ObjectId(req.params.groupId),
      isBlocked: false,
      isDeleted: false
    }
  },
  {
    $lookup: {
      from: 'userseengroups',
      let: { gId: '$_id' },
      pipeline: [{
        $match: {
          $expr: {
            $and: [
              { $eq: ['$groupId', '$$gId'] },
              { $eq: ['$userId', req.user._id] }
            ]
          }
        }
      }],
      as: 'userseengroups'
    }
  },
  { $unwind: '$userseengroups' },
  {
    $lookup: {
      from: 'posts',
      let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
      pipeline: postPipeline('read'),
      as: 'reads'
    }
  },
  {
    $lookup: {
      from: 'posts',
      let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
      pipeline: postPipeline('unread'),
      as: 'unreads'
    }
  },
  {
    $lookup: {
      from: 'posts',
      let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                { $eq: ['$groupId', '$$gId'] },
                { $ne: ['$isDeleted', true] }
              ]
            }
          }
        },
        {
          $group: {
            _id: { $cond: { if: { $gt: ['$createdAt', '$$lastSeen'] }, then: 'unread', else: 'read' } },
            postCount: { $sum: 1 }
          }
        }
      ],
      as: 'totalPosts'
    }
  },
  {
    $project: {
      'groupId': '$_id',
      _id: false,
      memberCount: true,
      university: '$university.name',
      topics: true,
      title: true,
      'lastSeen': '$userseengroups.lastSeen',
      readPosts: {
        posts: '$reads',
        count: {
          '$filter': {
            input: '$totalPosts',
            as: 'elem',
            cond: { $eq: ['$$elem._id', 'read'] }
          }
        }
      },
      unreadPosts: {
        posts: '$unreads',
        count: {
          '$filter': {
            input: '$totalPosts',
            as: 'elem',
            cond: { $eq: ['$$elem._id', 'unread'] }
          }
        }
      },
    }
  }
  ]);

  if (!group.length) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'Group not found',
      errorCode: 'GROUP_NOT_FOUND'
    });
  }
  group = group[0];
  const modifePostFields = post => {
    if (post.isRestricted && post.user.userId.toString() !== req.user._id.toString() && !req.user.isVerified) {
      post = {
        isRestricted: true,
        postId: post.postId
      };
    } else if (post.user.userId.toString() !== req.user._id.toString()) {
      post.user = post.isAnonymous ? null : post.user;
    }
    if (post.likes) {
      post.currentUserLiked =
        (post.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
        (post.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
        0;
      post.likes = post.likes.length - post.dislikes.length;
      delete post.dislikes;
    }
    if (post.comments) {
      post.comments = post.comments
        .filter(comment => !comment.parentId)
        .sort((ca, cb) => cb.createdAt - ca.createdAt)
        .slice(0, 2)
        .map(comment => {
          comment.commentId = comment._id;
          delete comment._id;

          comment.currentUserLiked =
            (comment.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
            (comment.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
            0;
          comment.likes = comment.likes.length - comment.dislikes.length;
          delete comment.dislikes;
          if (comment.isAnonymous) {
            comment.user = null;
          } else {
            const user = post.commentedUsers.find(user => user._id.toString() === comment.userId.toString());
            comment.user = {
              userId: user._id,
              isVerified: user.isVerified,
              firstName: user.firstName,
              photo: user.photo,
              major: user.major,
              lastName: user.lastName,
              email: user.email,
              username: user.username,
            };
          }
          delete comment.userId;
          delete comment.reports;
          return comment;
        });
      delete post.commentedUsers;
    }

    return post;
  };
  group.readPosts.count = group.readPosts.count.length && group.readPosts.count[0].postCount;
  group.unreadPosts.count = group.unreadPosts.count.length && group.unreadPosts.count[0].postCount;
  group.readPosts.posts = group.readPosts.count ? group.readPosts.posts.map(modifePostFields) : [];
  group.unreadPosts.posts = group.unreadPosts.count ? group.unreadPosts.posts.map(modifePostFields) : [];
  res.status(200).json({
    success: true,
    data: group,
    message: 'Fetch group info successfully',
  });
};

const getGroupTopic = async (req, res) => {
  const limit = +req.query.limit > 0 ? +req.query.limit : 20;
  const skip = +req.query.skip > 0 ? +req.query.skip : 0;
  const topics = req.query.topics.split(',').map(topic => topic.trim());
  const count = await PostModel.find({
    $and: [
      { isDeleted: { $ne: true } },
      { groupId: mongoose.Types.ObjectId(req.params.groupId) },
      { topics: { $exists: true } },
      { topics: { $all: topics } }
    ]
  }).count();

  let posts = await PostModel.aggregate([{
    $match: {
      $and: [
        { isDeleted: { $ne: true } },
        { groupId: mongoose.Types.ObjectId(req.params.groupId) },
        { topics: { $exists: true } },
        { topics: { $all: topics } }
      ]
    }
  },
  {
    $sort: { _id: -1 }
  },
  {
    $skip: skip
  },
  {
    $limit: limit
  },
  {
    $lookup: {
      from: 'users',
      let: {
        uId: '$userId',
      },
      pipeline: [{
        $match: {
          $expr: {
            $eq: ['$_id', '$$uId']
          }
        }
      },
      {
        $project: {
          userId: '$_id',
          _id: false,
          firstName: true,
          lastName: true,
          photo: true,
          major: true,
          email: true,
          username: true,
          isVerified: true,
        }
      }
      ],
      as: 'user'
    }
  },
  { $unwind: '$user' },
  {
    $lookup: {
      from: 'users',
      localField: "comments.userId",
      foreignField: "_id",
      as: 'commentedUsers'
    }
  },
  {
    $project: {
      postId: '$_id',
      _id: false,
      user: true,
      likes: true,
      dislikes: true,
      isRestricted: true,
      isAnonymous: true,
      content: true,
      topics: true,
      comments: {
        $filter: {
          input: '$comments',
          as: 'comment',
          cond: {
            $ne: ['$$comment.isDeleted', true]
          }
        }
      },
      commentedUsers: true,
      commentCount: {
        $cond: {
          if: { $isArray: '$comments' }, then: {
            $size: {
              $filter: {
                input: '$comments',
                as: 'comment',
                cond: {
                  $ne: ['$$comment.isDeleted', true]
                }
              }
            }
          }, else: 0
        }
      },
      createdAt: true,
    }
  },
  ]);
  posts = posts.map(post => {
    if (post.isRestricted && post.user.userId.toString() !== req.user._id.toString() && !req.user.isVerified) {
      post = {
        isRestricted: true,
        postId: post.postId
      };
    } else if (post.user.userId.toString() !== req.user._id.toString()) {
      post.user = post.isAnonymous ? null : post.user;
    }
    if (post.likes) {
      post.currentUserLiked =
        (post.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
        (post.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
        0;
      post.likes = post.likes.length - post.dislikes.length;
      delete post.dislikes;
    }
    if (post.comments) {
      post.comments = post.comments
        .filter(comment => !comment.parentId)
        .sort((ca, cb) => cb.createdAt - ca.createdAt)
        .slice(0, 2)
        .map(comment => {
          comment.commentId = comment._id;
          delete comment._id;

          comment.currentUserLiked =
            (comment.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
            (comment.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
            0;
          comment.likes = comment.likes.length - comment.dislikes.length;
          delete comment.dislikes;
          if (comment.isAnonymous) {
            comment.user = null;
          } else {
            const user = post.commentedUsers.find(user => user._id.toString() === comment.userId.toString());
            comment.user = {
              userId: user._id,
              isVerified: user.isVerified,
              firstName: user.firstName,
              photo: user.photo,
              major: user.major,
              lastName: user.lastName,
              email: user.email,
              username: user.username,
            };
          }
          delete comment.userId;
          delete comment.reports;
          return comment;
        });
      delete post.commentedUsers;
    }
    return post;
  });

  res.status(200).json({
    success: true,
    data: {
      readPosts: {
        posts,
        count
      }
    },
    message: 'Fetch group info successfully',
  });
};

const getGroupUsers = async (req, res) => {
  const users = await UserSeenGroupModel.aggregate([{
    $match: {
      groupId: mongoose.Types.ObjectId(req.params.groupId)
    }
  },
  {
    $lookup: {
      from: 'users',
      let: { uId: '$userId' },
      pipeline: [{
        $match: {
          $expr: {
            $eq: ['$_id', '$$uId']
          }
        }
      },
      {
        $project: {
          _id: false,
          userId: '$_id',
          firstName: true,
          lastName: true,
          photo: true,
          major: true,
          email: true,
          username: true,
          isVerified: true
        }
      }
      ],
      as: 'user'
    }
  },
  { $unwind: '$user' },
  {
    $group: {
      _id: '$groupId',
      list: { $addToSet: '$user' }
    }
  }
  ]);
  res.status(200).json({
    success: true,
    data: users[0].list,
    message: 'Fetch group info successfully',
  });

};

const updateLastSeenGroup = async (req, res) => {
  const userSeenGroup = await UserSeenGroupModel.findOneAndUpdate({
    userId: req.user._id,
    groupId: req.params.groupId
  }, {
    lastSeen: Date.now()
  }, { new: true });
  if (!userSeenGroup) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'Group not found',
      errorCode: 'GROUP_NOT_FOUND'
    });
  }
  res.status(200).json({
    success: true,
    data: { lastSeen: userSeenGroup.lastSeen },
    message: 'lastSeen updated successfully',
  });
};

const leaveGroup = async (req, res) => {
  let group = await UserSeenGroupModel.deleteOne({
    userId: req.user._id,
    groupId: req.params.groupId
  });
  if (group.deletedCount) {
    group = await GroupModel.findById(req.params.groupId);
    if (group.memberCount === 1) {
      group.remove();
    } else {
      group.memberCount--;
      group = await group.save();
    }
    return res.status(200).json({
      success: true,
      data: null,
      message: 'User left from group successfully',
    });
  }
  res.status(404).json({
    success: false,
    data: null,
    message: 'Group not found',
    errorCode: 'GROUP_NOT_FOUND'
  });
};

module.exports = {
  getGroups,
  autoJoinGroup,
  getGroup,
  getGroupTopic,
  getGroupUsers,
  updateLastSeenGroup,
  leaveGroup,
};
