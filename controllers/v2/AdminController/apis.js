const { Admin } = require('../../../services/v2');

const get = async (req, res) => {
  const admin = Admin.get(req.user);
  res.success(admin);
};

const blockStudent = async (req, res) => {
  await Admin.blockStudent(req.params.studentId, req.body.status);
  res.success();
};

const removePost = async (req, res) => {
  await Admin.removePost(req.params.postId);
  res.success();
};

const removeComment = async (req, res) => {
  await Admin.removeComment(req.body.postId, req.params.commentId);
  res.success();
};

module.exports = {
  get,
  blockStudent,
  removePost,
  removeComment,
};
