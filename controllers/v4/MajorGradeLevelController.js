const { MajorAndGradeLevel } = require('../../services/v4');


const getAllMajorAndGradeLevel = async (req, res) => {
    const data = await MajorAndGradeLevel.getAll(req.user);
    res.success(data);
  };

module.exports = {
    getAllMajorAndGradeLevel
    };