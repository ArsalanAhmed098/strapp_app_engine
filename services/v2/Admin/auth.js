const uuid4 = require('uuid/v4');
const { AdminModel } = require('../../../models/v2');
const { Password } = require('../../../util');

const register = async adminData => {
  adminData.password = await Password.hash(adminData.password);
  const admin = await AdminModel.create(adminData);
  admin.password = undefined;
  return admin;
};

const login = async user => {
  if(!user.token)
    user.token = uuid4();
  await user.save();
  user.password = undefined;
  return user;
};

const logout = async user => {
  user.token = '';
  await user.save();
};

const getByToken = token =>
  AdminModel.findOne({ token });

module.exports = {
  register,
  login,
  logout,
  getByToken,
};
