const userAccess = (req, res, next) => {
  const path = req.originalUrl.replace('/api/v1/', '').split('?')[0];
  const method = req.method;
  if (!req.user.hasAccess({ path, method })) {
    console.log('user forbidden:', req.user.block.deniedRoutes);
    return res.status(403).json({
      success: false,
      message: 'You do not have access to this route',
      errorCode: 'FORBIDDEN'
    });
  }
  console.log('user has access to this route');
  next();
};

module.exports = userAccess;
