const mongoose = require('mongoose');

const UserlogsSchema = new mongoose.Schema({
  postId: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
    // required: true,
  },
  groupId: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
    // required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  type: {
    type: String,
  },
  starttime: { 
    type: Date,
    default: 0 
  },
  endtime: { 
    type: Date,
    default: null 
  },
  islatest: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
}, {
  timestamps: true
});

module.exports = mongoose.model('Userlogs', UserlogsSchema);
