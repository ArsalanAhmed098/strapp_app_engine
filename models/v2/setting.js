const mongoose = require('mongoose');

const SettingSchema = new mongoose.Schema({
    addpost_title: {
        type: String,
        required: true,
    },
    addpost_description: {
        type: String,
        required: true,
    },
    addpost_isNotified: {
        type: Boolean,
        default: false
    },
    addpost_isDeleted: {
        type: Boolean,
        default: false
    },

    addcomment_title: {
        type: String,
        required: true,
    },
    addcomment_description: {
        type: String,
        required: true,
    },
    addcomment_isNotified: {
        type: Boolean,
        default: false
    },
    addcomment_isDeleted: {
        type: Boolean,
        default: false
    },

    likepost_title: {
        type: String,
        required: true,
    },
    likepost_description: {
        type: String,
        required: true,
    },
    likepost_isNotified: {
        type: Boolean,
        default: false
    },
    likepost_isNotified: {
        type: Boolean,
        default: false
    },


    unlikepost_title: {
        type: String,
        required: true,
    },
    unlikepost_description: {
        type: String,
        required: true,
    },
    unlikepost_isNotified: {
        type: Boolean,
        default: false
    },
    unlikepost_isDeleted: {
        type: Boolean,
        default: false
    },

}, {
    timestamps: true
});
module.exports = mongoose.model('Setting', SettingSchema);