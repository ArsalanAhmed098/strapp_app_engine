$(document).ready(function () {
  $('.login-form').submit(function (e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var url = $(this).attr('action');
    var method = $(this).attr('method');
    console.log('formData:', formData, 'url:', url, 'method:', method);
    $.ajax({
      method: method,
      url: url,
      data: formData,
      // contentType: `'application/x-www-form-urlencoded`,
      success: function (responseData, textStatus, jqXHR) {
        console.log('responseData:', responseData);
        var result = responseData.success;
        if(result === true) {
          window.location.href = '/admins';
        }
      },
      error: function (XHR, textStatus, errorThrown) {
        console.log('errorThrown:', errorThrown);
        if(XHR.status === 401) {
          alert('authentication problem');
        } else {
          alert('Server problem');
        }
      }
    });
  });
});


