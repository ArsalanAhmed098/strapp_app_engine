const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { SectionController, PostController, UserController } = require('../../../controllers/v2');
const { userAccess, upload } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccess);

router.route('/')
  .get(SectionController.getAll)

router.route('/:sectionId')
  .post(SectionController.join)
  .get(SectionController.get)
  .delete(SectionController.leave);

router.route('/:sectionId/seen')
  .patch(SectionController.updateSeen);

router.route('/:sectionId/users')
  .get(SectionController.getAllMembers);

router.route('/:sectionId/posts')
  .post(
    upload().array('photos'),
    PostController.add
  )
  .get(PostController.getAll);

router.route('/:sectionId/posts/topic')
  .get(PostController.getAllByTopic);

module.exports = router;
