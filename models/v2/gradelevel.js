const mongoose = require('mongoose');

const GradelevelSchema = new mongoose.Schema({
    gradelevel: {
        title: {
            type: String,
            required: true,
        },
    },
    isBlocked: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true
});
module.exports = mongoose.model('Gradelevel', GradelevelSchema);