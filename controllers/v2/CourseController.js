const { Course } = require('../../services/v2');

const add = async (req, res) => {
  const course = await Course.add(req.user, req.body.name);
  res.success(course);
};

const getAll = async (req, res) => {
  const courses = await Course.getAll(req.user);
  res.success(courses);
};

module.exports = {
  add,
  getAll
};
