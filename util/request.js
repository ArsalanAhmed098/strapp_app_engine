const https = require('https');

const request = options =>
  new Promise((resolve, reject) => {
    const req = https.request(options, res => {
      let body = '';
      res.on('data', function (chunk) {
        body += chunk;
      });
      res.on('end', () => {
        resolve(body);
      });
    });
    req.on('error', reject);
    req.end();
  });

module.exports = request;
