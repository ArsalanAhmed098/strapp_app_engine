const { reset } = require('nodemon');
const { Admin } = require('../../../services/v2');

const loginPage = async(req, res) => {
    res.render('pages/login');
};

const homePage = async(req, res) => {
    res.redirect('/admins/students');
};


const studentsPage = async(req, res) => {
    const schoolQuery = (req.query.school || '').trim();
    const courseQuery = (req.query.course || '').trim().toUpperCase();
    const sectionQuery = (req.query.section || '').trim();
    const studentQuery = (req.query.student || '').trim().toLowerCase();
    const limit = 20;
    const page = +req.query.page || 1;
    console.log('schoolQuery:', schoolQuery);
    console.log('courseQuery:', courseQuery);
    console.log('sectionQuery:', sectionQuery);
    console.log('studentQuery:', studentQuery);
    console.log('limit:', limit);
    console.log('page:', page);

    const students = await Admin
        .getAllStudents(
            schoolQuery,
            courseQuery,
            sectionQuery,
            studentQuery,
            page,
            limit
        );

    // return res.json(students);

    res.render('pages/students', {
        user: req.user,
        page: {
            title: 'Students',
            subtitle: 'Students Management'
        },
        sidebar: { active: 'students' },
        breadcrumb: {
            list: [
                { title: 'Home', link: '/admins' },
                { title: 'Students' },
            ]
        },
        list: students.data,
        filter: {
            schoolQuery,
            courseQuery,
            sectionQuery,
            studentQuery,
        },
        pagination: students.pagination
    });
};

const sectionsPage = async(req, res) => {
    const schoolQuery = (req.query.school || '').trim();
    const courseQuery = (req.query.course || '').trim().toUpperCase();
    const sectionQuery = (req.query.section || '').trim().toLowerCase();
    const studentQuery = req.query.student;
    const limit = 20;
    const page = +req.query.page || 1;
    console.log('schoolQuery:', schoolQuery);
    console.log('courseQuery:', courseQuery);
    console.log('sectionQuery:', sectionQuery);
    console.log('studentQuery:', studentQuery);
    console.log('limit:', limit);
    console.log('page:', page);

    const sections = await Admin.getAllSections(schoolQuery, courseQuery, sectionQuery, studentQuery, page, limit);

    res.render('pages/sections', {
        user: req.user,
        page: {
            title: 'Sections',
            subtitle: 'Sections Management'
        },
        sidebar: { active: 'sections' },
        breadcrumb: {
            list: [
                { title: 'Home', link: '/admins' },
                { title: 'Sections' },
            ]
        },
        list: sections.data,
        filter: {
            schoolQuery,
            courseQuery,
            sectionQuery
        },
        pagination: sections.pagination
    });
};

const postsPage = async(req, res) => {
    const schoolQuery = (req.query.school || '').trim();
    const courseQuery = (req.query.course || '').trim().toUpperCase();
    const sectionQuery = (req.query.section || '').trim().toLowerCase();
    const postType = (req.query.type || 'all').trim().toLowerCase();
    const limit = 20;
    const page = +req.query.page || 1;
    console.log('schoolQuery:', schoolQuery);
    console.log('courseQuery:', courseQuery);
    console.log('sectionQuery:', sectionQuery);
    console.log('postType:', postType);
    console.log('limit:', limit);
    console.log('page:', page);

    const posts = await Admin.getAllPosts(schoolQuery, courseQuery, sectionQuery, postType, page, limit);

    res.render('pages/posts', {
        user: req.user,
        page: {
            title: 'Posts',
            subtitle: 'Post list'
        },
        sidebar: { active: 'posts' },
        breadcrumb: {
            list: [
                { title: 'Home', link: '/admins' },
                { title: 'Posts', link: '/admins/Posts' },
            ]
        },
        list: posts.data,
        filter: {
            schoolQuery,
            courseQuery,
            sectionQuery,
            postType
        },
        pagination: posts.pagination
    });

};

const commentsPage = async(req, res) => {
    const { postId } = req.params;
    const commentType = (req.query.type || 'all').trim().toLowerCase();
    const limit = 20;
    const page = +req.query.page || 1;
    console.log('commentType:', commentType);
    console.log('limit:', limit);
    console.log('page:', page);

    const comments = await Admin.getAllCommentsOfPost(postId, commentType, page, limit);

    res.render('pages/comments', {
        user: req.user,
        page: {
            title: 'Comments',
            subtitle: 'Comment list'
        },
        sidebar: { active: 'posts' },
        breadcrumb: {
            list: [
                { title: 'Home', link: '/admins' },
                { title: 'Posts', link: '/admins/posts' },
                { title: 'Comments', link: '/admins/posts/comments' },
            ]
        },
        list: comments.data,
        filter: {
            commentType
        },
        pagination: comments.pagination
    });
};

const exportStudents = async(req, res) => {
    const schoolQuery = (req.query.school || '').trim();
    const courseQuery = (req.query.course || '').trim().toUpperCase();
    const sectionQuery = (req.query.section || '').trim().toLowerCase();
    const studentQuery = (req.query.student || '').trim().toLowerCase();
    console.log('schoolQuery:', schoolQuery);
    console.log('courseQuery:', courseQuery);
    console.log('sectionQuery:', sectionQuery);
    console.log('studentQuery:', studentQuery);

    const filePath = await Admin
        .exportStudents(
            schoolQuery,
            courseQuery,
            sectionQuery,
            studentQuery,
        );

    res.download(filePath);
};

// const groupsCategoryPage = async (req, res) => {


//   res.render('pages/groupscategory', {
//     user: req.user,
//     page: {
//       title: 'Groups Category',
//       subtitle: 'Groups Category Management'
//     },
//     sidebar: { active: 'groupscategory' }

//   });
// };

// const groupCategoryAdd = async (req, res) => {

//   // var dt= req.body;
//   // res.send(dt);

//   const groupCategory = await Admin.addcategory(req.body);
//   // console.log('before:', req.body);
//   // res.success(req.body);
//   res.redirect('back');
// };

// const addgroupviewPage = async (req, res) => {

//   // console.log(await Admin.getAllUniversities());

//   const datatoshow = await Admin.getAllUniversities();

//   res.render('pages/addgroup', {
//     user: req.user,
//     list: datatoshow,
//     response: '',
//     showvalidate: '',
//     oldData: '',
//     page: {
//       title: 'Add Group',
//       subtitle: 'Add Group Management'
//     },
//     sidebar: { active: 'addgroup' }

//   });
// };


// const groupdata = async (req, res) => {

//   // var dt= req.body;
//   // res.send(dt);

//    ///////Validation start/////////
//   const universityname_Required = 'Must select university';

//   const oldData = req.body;

//   req.checkBody('title')
//   .notEmpty().withMessage('Title is required!')
//   .isLength({min:3}).withMessage('Name must be of 3 characters long.')
//   .matches(/^[A-Za-z\s]+$/).withMessage('Name must be alphabetic.')


//   req.checkBody('university.name', universityname_Required).notEmpty();
//   const errors = req.validationErrors();

//    ///////Validation End/////////

//   if(!errors){

//     ///////Data pass to addgroup for insertion in mongo_db/////////

//     const add_group = await Admin.addgroup(req.body);

//     const datatoshow = await Admin.getAllUniversities();

//     let _response = { type: 'error', msg: 'Failed!' }
//     if (add_group != {} && add_group != undefined && add_group != null)
//       _response = { type: 'success', msg: 'Added' }

//     res.render('pages/addgroup', {
//       user: req.user,
//       list: datatoshow,
//       response: _response,
//       showvalidate: '',
//       oldData: '',
//       page: {
//         title: 'Add Group',
//         subtitle: 'Add Group Management'
//       },
//       sidebar: { active: 'addgroup' }

//     });
//     // res.redirect('back', { msg: 'added' });

// }else{

//   ///////errors show to front admin panel/////////

//     const titleRequired = errors.find(el => el.msg === 'title is required!' || el.msg === 'Name must be of 3 characters long.' || el.msg === 'Name must be alphabetic.');
//     const uniRequired = errors.find(el => el.msg === universityname_Required );

//     const datatoshow = await Admin.getAllUniversities();


//     // console.log(oldData);

//     // console.log(errors);

//     res.render('pages/addgroup', {
//       user: req.user,
//       list: datatoshow,
//       response: '',
//       showvalidate: {titleRequired,uniRequired},
//       oldData,
//       page: {
//         title: 'Add Group',
//         subtitle: 'Add Group Management'
//       },
//       sidebar: { active: 'addgroup' }

//     });
// }


// };

const universitylistviewPage = async(req, res) => {

    const limit = 20;
    const page = +req.query.page || 1;

    const datatoshow = await Admin.getAllUniversitieslist(page, limit);

    res.render('pages/universitylistpassingdata', {
        user: req.user,
        // response: '',
        // showvalidate: '',
        // oldData: '',
        page: {
            title: 'View Universities',
            subtitle: 'University Management'
        },
        sidebar: { active: 'universitylistpassingdata' },
        list: datatoshow.data,
        pagination: datatoshow.pagination

    });
};

const editUniversityviewPage = async(req, res) => {


    // console.log(req.params);

    // const datatoshow = await Admin.getAllUniversities();

    const oldData = await Admin.getOneUniversity(req.params);

    console.log(oldData);

    res.render('pages/edituniversity', {
        user: req.user,
        response: '',
        showvalidate: '',
        oldData: oldData,
        page: {
            title: 'Edit University',
            subtitle: 'Edit University Management'
        },
        sidebar: { active: 'edituniversity' }

    });
};

const editUniversitydata = async(req, res) => {

    const oldData = req.body;

    req.checkBody('university.name')
        .notEmpty().withMessage('University Name is required!')
        .isLength({ min: 3 }).withMessage('University Name must be of 3 characters long.')
        // .matches(/^[A-Za-z\s]+$/).withMessage('University Name must be alphabetic.')

    req.checkBody('university.domains')
        .notEmpty().withMessage('Domain Name is required!')
        .isLength({ min: 3 }).withMessage('Domain Name must be of 3 characters long.')
        // .matches(/^[A-Za-z\s]+$/).withMessage('Domain Name must be alphabetic.')

    const errors = req.validationErrors();

    ///////Validation End/////////

    if (!errors) {

        ///////Data pass to edituniversity for updation in mongo_db/////////

        const edit_university = await Admin.updateUniversity(req.body, req.params);

        const oldData = await Admin.getOneUniversity(req.params);

        let _response = { type: 'error', msg: 'Failed!' }
        if (edit_university != {} && edit_university != undefined && edit_university != null)
            _response = { type: 'success', msg: 'Updated' }

        res.render('pages/edituniversity', {
            user: req.user,
            response: _response,
            showvalidate: '',
            oldData: oldData,
            page: {
                title: 'Edit University',
                subtitle: 'Edit University Management'
            },
            sidebar: { active: 'edituniversity' }

        });

    } else {

        ///////errors show to front admin panel/////////

        const uninameRequired = errors.find(el => el.msg === 'University Name is required!' || el.msg === 'University Name must be of 3 characters long.');
        const unidomainRequired = errors.find(el => el.msg === 'Domain Name is required!' || el.msg === 'Domain Name must be of 3 characters long.');

        const oldData = await Admin.getOneUniversity(req.params);

        res.render('pages/edituniversity', {
            user: req.user,
            response: '',
            showvalidate: { unidomainRequired, uninameRequired },
            oldData: oldData,
            page: {
                title: 'Edit University',
                subtitle: 'Edit University Management'
            },
            sidebar: { active: 'edituniversity' }

        });

    }
};

const adduniversityviewPage = async(req, res) => {

    res.render('pages/adduniversity', {
        user: req.user,
        response: '',
        showvalidate: '',
        oldData: '',
        page: {
            title: 'Add University',
            subtitle: 'Add University Management'
        },
        sidebar: { active: 'adduniversity' }

    });
};

const universitydata = async(req, res) => {

    const oldData = req.body;

    req.checkBody('university.name')
        .notEmpty().withMessage('University Name is required!')
        .isLength({ min: 3 }).withMessage('University Name must be of 3 characters long.')
        // .matches(/^[A-Za-z\s]+$/).withMessage('University Name must be alphabetic.')

    req.checkBody('university.domains')
        .notEmpty().withMessage('Domain Name is required!')
        .isLength({ min: 3 }).withMessage('Domain Name must be of 3 characters long.')
        // .matches(/^[A-Za-z\s]+$/).withMessage('Domain Name must be alphabetic.')

    const errors = req.validationErrors();

    ///////Validation End/////////

    if (!errors) {

        ///////Data pass to adduniversity for insertion in mongo_db/////////

        const add_university = await Admin.adduniversity(req.body);

        let _response = { type: 'error', msg: 'Record Already Exists !' }
        if (add_university != {} && add_university != undefined && add_university != null)
            _response = { type: 'success', msg: 'Added' }

        res.render('pages/adduniversity', {
            user: req.user,
            response: _response,
            showvalidate: '',
            oldData: '',
            page: {
                title: 'Add University',
                subtitle: 'Add University Management'
            },
            sidebar: { active: 'adduniversity' }

        });

    } else {

        ///////errors show to front admin panel/////////

        const uninameRequired = errors.find(el => el.msg === 'University Name is required!' || el.msg === 'University Name must be of 3 characters long.');
        const unidomainRequired = errors.find(el => el.msg === 'Domain Name is required!' || el.msg === 'Domain Name must be of 3 characters long.');


        res.render('pages/adduniversity', {
            user: req.user,
            response: '',
            showvalidate: { unidomainRequired, uninameRequired },
            oldData,
            page: {
                title: 'Add University',
                subtitle: 'Add University Management'
            },
            sidebar: { active: 'adduniversity' }

        });

    }
};

const addsectionviewPage = async(req, res) => {


    const datatoshow = await Admin.getAllUniversities();
    const campusdata = await Admin.getAllCampusGroups();

    console.log(datatoshow);

    res.render('pages/addsection', {
        user: req.user,
        list: datatoshow,
        campusdata: campusdata,
        response: '',
        showvalidate: '',
        oldData: '',
        page: {
            title: 'Add Section',
            subtitle: 'Add Section Management'
        },
        sidebar: { active: 'addsection' }

    });
};

const sectiondata = async(req, res, newpath) => {
    // var newpath = '';
    var isGeneral = 'false';

    if (newpath == null) {

        var newpath = '';
    }

    // if(req.file) {

    //   var originalpath= req.file.path;
    //   newpath = originalpath.replace(/\\/g, "/");
    // res.send(originalpath);
    // }

    if (req.body.isGeneral) {
        req.body.isGeneral = 'true';
        isGeneral = true;
    }
    // var fullUrl = req.protocol + '://' + req.get('host') + '/';
    // res.send(fullUrl);   

    ///////Validation start/////////
    const universityname_Required = 'Must select university';

    const oldData = req.body;

    //  req.checkBody('course.name')
    //  .notEmpty().withMessage('Course is required!')
    //  .isLength({min:3}).withMessage('Name must be of 3 characters long.')
    //  .matches(/^[A-Za-z\s]+$/).withMessage('Name must be alphabetic.')

    req.checkBody('section.name')
        .notEmpty().withMessage('Section Name is required!')
        .isLength({ min: 3 }).withMessage('Section Name must be of 3 characters long.')
        //  .matches(/^[A-Za-z\s]+$/).withMessage('Name must be alphabetic.')

    req.checkBody('university.name', universityname_Required).notEmpty();

    //  req.checkBody('file')
    //  .notEmpty().withMessage('Image is required!')

    req.checkBody('group.description')
        .notEmpty().withMessage('Description is required!')
        // .isLength({max:20}).withMessage('Description must be of 20 characters long.')
        //  .matches(/^[A-Za-z\s]+$/).withMessage('Group Name must be alphabetic.')

    req.checkBody('group.name')
        .notEmpty().withMessage('Must select Group!');

    const errors = req.validationErrors();

    ///////Validation End/////////

    if (!errors) {

        ///////Data pass to addsection for insertion in mongo_db/////////

        //  return  res.send(oldData);

        var add_section = await Admin.addsection(req.user, req.body, newpath, isGeneral);

        const datatoshow = await Admin.getAllUniversities();
        const campusdata = await Admin.getAllCampusGroups();

        let _response = { type: 'error', msg: 'Record Already Exists !' }
        if (add_section != {} && add_section != undefined && add_section != null)
            _response = { type: 'success', msg: 'Added' }

        res.render('pages/addsection', {
            user: req.user,
            list: datatoshow,
            campusdata: campusdata,
            response: _response,
            showvalidate: '',
            oldData: '',
            page: {
                title: 'Add Section',
                subtitle: 'Add Section Management'
            },
            sidebar: { active: 'addsection' }

        });
        // res.redirect('back', { msg: 'added' });

    } else {

        ///////errors show to front admin panel/////////

        // const courseRequired = errors.find(el => el.msg === 'Course is required!' || el.msg === 'Name must be of 3 characters long.' || el.msg === 'Name must be alphabetic.');
        const sectionRequired = errors.find(el => el.msg === 'Section Name is required!' || el.msg === 'Section Name must be of 3 characters long.');
        const uniRequired = errors.find(el => el.msg === universityname_Required);
        const groupDescriptionRequired = errors.find(el => el.msg === 'Description is required!' || el.msg === 'Description must be of 20 characters long.');
        const groupNameRequired = errors.find(el => el.msg === 'Must select Group!')
            // const imageRequired = errors.find(el => el.msg === 'Image is required!');


        const datatoshow = await Admin.getAllUniversities();
        const campusdata = await Admin.getAllCampusGroups();

        // console.log(oldData);

        // console.log(errors);

        res.render('pages/addsection', {
            user: req.user,
            list: datatoshow,
            campusdata: campusdata,
            response: '',
            showvalidate: { uniRequired, groupDescriptionRequired, groupNameRequired, sectionRequired },
            oldData,
            page: {
                title: 'Add section',
                subtitle: 'Add Section Management'
            },
            sidebar: { active: 'addsection' }

        });

    }

};

// const sectionlistviewPage1 = async (req, res) => {

//   const datatoshow = await Admin.getAllSectionslist1();

//   res.render('pages/sectionlistpassingdata1', {
//     user: req.user,
//     list: datatoshow,
//     // response: '',
//     // showvalidate: '',
//     // oldData: '',
//     page: {
//       title: 'View Sections',
//       subtitle: 'Sections Management'
//     },
//     sidebar: { active: 'sectionlistpassingdata1' }

//   });
// };

const sectionlistviewPage = async(req, res) => {

    const limit = 20;
    const page = +req.query.page || 1;

    const datatoshow = await Admin.getAllSectionslist(page, limit);

    console.log(datatoshow.data);

    res.render('pages/sectionlistpassingdata', {
        user: req.user,
        // response: '',
        // showvalidate: '',
        // oldData: '',
        page: {
            title: 'View Sections',
            subtitle: 'Sections Management'
        },
        sidebar: { active: 'sectionlistpassingdata' },
        list: datatoshow.data,
        pagination: datatoshow.pagination

    });
};

const editsectionviewPage = async(req, res) => {


    // console.log(req.params);

    const datatoshow = await Admin.getAllUniversities();
    const campusdata = await Admin.getAllCampusGroups();

    const oldData = await Admin.getOneSection(req.params);

    console.log(oldData);

    res.render('pages/editsection', {
        user: req.user,
        list: datatoshow,
        campusdata: campusdata,
        response: '',
        showvalidate: '',
        oldData: oldData,
        page: {
            title: 'Edit Section',
            subtitle: 'Edit Section Management'
        },
        sidebar: { active: 'editsection' }

    });
};

const editsectiondata = async(req, res, newpath) => {

    // var newpath = '';
    var isGeneral = 'false';

    if (newpath == null) {

        var newpath = '';
    }

    // if(req.file) {

    // var originalpath= req.file.path;
    // newpath = originalpath.replace(/\\/g, "/");
    // res.send(originalpath);
    // }

    if (req.body.isGeneral) {
        req.body.isGeneral = 'true';
        isGeneral = true;
    }
    // var fullUrl = req.protocol + '://' + req.get('host') + '/';
    // res.send(fullUrl);   

    ///////Validation start/////////
    const universityname_Required = 'Must select university';

    //  const oldData = req.body;

    //  req.checkBody('course.name')
    //  .notEmpty().withMessage('Course is required!')
    //  .isLength({min:3}).withMessage('Name must be of 3 characters long.')
    //  .matches(/^[A-Za-z\s]+$/).withMessage('Name must be alphabetic.')\

    req.checkBody('section.name')
        .notEmpty().withMessage('Section Name is required!')
        .isLength({ min: 3 }).withMessage('Section Name must be of 3 characters long.')

    req.checkBody('group.description')
        .notEmpty().withMessage('Description is required!')
        // .isLength({max:20}).withMessage('Description must be of 20 characters long.')

    req.checkBody('group.name')
        .notEmpty().withMessage('Must select Group!');

    req.checkBody('university.name', universityname_Required).notEmpty();

    //  req.checkBody('file')
    //  .notEmpty().withMessage('Image is required!')

    const errors = req.validationErrors();

    ///////Validation End/////////

    if (!errors) {

        ///////Data pass to addsection for insertion in mongo_db/////////

        var section_updated = await Admin.updatesection(req.body, req.params, newpath, isGeneral);

        const datatoshow = await Admin.getAllUniversities();
        const campusdata = await Admin.getAllCampusGroups();

        const oldData = await Admin.getOneSection(req.params);

        let _response = { type: 'error', msg: 'isGeneral is Already Exists!' }
        if (section_updated != {} && section_updated != undefined && section_updated != null)
            _response = { type: 'success', msg: 'Updated' }

        res.render('pages/editsection', {
            user: req.user,
            list: datatoshow,
            campusdata: campusdata,
            response: _response,
            showvalidate: '',
            oldData: oldData,
            page: {
                title: 'Edit Section',
                subtitle: 'Edit Section Management'
            },
            sidebar: { active: 'editsection' }

        });
        // res.redirect('back', { msg: 'added' });

    } else {

        ///////errors show to front admin panel/////////

        // const courseRequired = errors.find(el => el.msg === 'Course is required!' || el.msg === 'Name must be of 3 characters long.' || el.msg === 'Name must be alphabetic.');
        const sectionRequired = errors.find(el => el.msg === 'Section Name is required!' || el.msg === 'Section Name must be of 3 characters long.');
        const uniRequired = errors.find(el => el.msg === universityname_Required);
        const groupDescriptionRequired = errors.find(el => el.msg === 'Description is required!' || el.msg === 'Description must be of 20 characters long.');
        const groupNameRequired = errors.find(el => el.msg === 'Must select Group!')
            // const imageRequired = errors.find(el => el.msg === 'Image is required!');

        const datatoshow = await Admin.getAllUniversities();
        const campusdata = await Admin.getAllCampusGroups();

        const oldData = await Admin.getOneSection(req.params);

        // console.log(oldData);

        // console.log(errors);

        res.render('pages/editsection', {
            user: req.user,
            list: datatoshow,
            campusdata: campusdata,
            response: '',
            showvalidate: { sectionRequired, uniRequired, groupDescriptionRequired, groupNameRequired },
            oldData: oldData,
            page: {
                title: 'edit section',
                subtitle: 'edit section Management'
            },
            sidebar: { active: 'editsection' }

        });

    }

};

const grouplistviewPage = async(req, res) => {

    const limit = 20;
    const page = +req.query.page || 1;

    const datatoshow = await Admin.getAllGroupslist(page, limit);

    res.render('pages/grouplistpassingdata', {
        user: req.user,
        // response: '',
        // showvalidate: '',
        // oldData: '',
        page: {
            title: 'View Groups',
            subtitle: 'Groups Management'
        },
        sidebar: { active: 'grouplistpassingdata' },
        response: '',
        list: datatoshow.data,
        pagination: datatoshow.pagination

    });
};

const groupListViewPageStatusEdit = async(req, res) => {

    const limit = 20;
    const page = +req.query.page || 1;

    const datatoshow = await Admin.getAllGroupslist(page, limit);

    const status_updated = await Admin.deleteCampusgroup(req.params);

    console.log(datatoshow.data);

    let _response = { type: 'error', msg: 'Failed!' }
    if (status_updated != {} && status_updated != undefined && status_updated != null)
        _response = { type: 'success', msg: 'Deleted' }

    res.render('pages/grouplistpassingdata', {
        user: req.user,
        // response: '',
        // showvalidate: '',
        // oldData: '',
        page: {
            title: 'View Groups',
            subtitle: 'Groups Management'
        },
        sidebar: { active: 'grouplistpassingdata' },
        list: datatoshow.data,
        response: _response,
        pagination: datatoshow.pagination

    });
};

const editgroupviewPage = async(req, res) => {


    // console.log(req.params);

    const datatoshow = await Admin.getAllUniversities();

    const oldData = await Admin.getOneGroup(req.params);

    console.log(oldData);

    res.render('pages/editgroup', {
        user: req.user,
        list: datatoshow,
        response: '',
        showvalidate: '',
        oldData: oldData,
        page: {
            title: 'Edit Group',
            subtitle: 'Edit Group Management'
        },
        sidebar: { active: 'editgroup' }

    });
};

const editgroupdata = async(req, res, newpath) => {
    // var newpath = '';
    var isDefault = 'false';

    if (newpath == null) {

        var newpath = '';
    }

    // if(req.file) {

    //   var originalpath= req.file.path;
    // console.log('=============image1',originalpath);
    // newpath = originalpath.replace(/\\/g, "/");
    // console.log('=============image2',newpath);
    // newpath = newpath.substring(7);
    // console.log('=============image',newpath);
    // res.send(originalpath);
    // }

    if (req.body.isDefault) {
        req.body.isDefault = 'true';
        isDefault = true;
    }
    // var fullUrl = req.protocol + '://' + req.get('host') + '/';
    // res.send(fullUrl);   

    ///////Validation start/////////
    const universityname_Required = 'Must select university';

    const oldData = req.body;

    req.checkBody('group.name')
        .notEmpty().withMessage('Group Name is required!');
    //  .isLength({min:3}).withMessage('Group Name must be of 3 characters long.')
    //  .matches(/^[A-Za-z\s]+$/).withMessage('Group Name must be alphabetic.')

    req.checkBody('group.description')
        .notEmpty().withMessage('Description is required!')
        .isLength({ max: 20 }).withMessage('Description must be of 20 characters long.');

    req.checkBody('university.name', universityname_Required).notEmpty();

    //  req.checkBody('file')
    //  .notEmpty().withMessage('Image is required!')

    const errors = req.validationErrors();

    ///////Validation End////////

    // return res.send(req.body);
    if (!errors) {

        ///////Data pass to editgroup for insertion in mongo_db/////////
        const group_updated = await Admin.updategroup(req.body, req.params, newpath, isDefault);
        const datatoshow = await Admin.getAllUniversities();
        const oldData = await Admin.getOneGroup(req.params);

        let _response = { type: 'error', msg: 'Failed!' }
        if (group_updated != {} && group_updated != undefined && group_updated != null)
            _response = { type: 'success', msg: 'Updated' }


        res.render('pages/editgroup', {
            user: req.user,
            list: datatoshow,
            response: _response,
            showvalidate: '',
            oldData: oldData,
            page: {
                title: 'Edit Group',
                subtitle: 'Edit Group Management'
            },
            sidebar: { active: 'editgroup' }

        });
        // res.redirect('back', { msg: 'added' });

    } else {

        ///////errors show to front admin panel/////////

        const groupnameRequired = errors.find(el => el.msg === 'Group Name is required!' || el.msg === 'Group Name must be of 3 characters long.' || el.msg === 'Group Name must be alphabetic.');
        const uniRequired = errors.find(el => el.msg === universityname_Required);
        const groupDescriptionRequired = errors.find(el => el.msg === 'Description is required!' || el.msg === 'Description must be of 20 characters long.');


        // const imageRequired = errors.find(el => el.msg === 'Image is required!');

        const datatoshow = await Admin.getAllUniversities();

        const oldData = await Admin.getOneGroup(req.params);

        // console.log(oldData);

        // console.log(errors);


        res.render('pages/editgroup', {
            user: req.user,
            list: datatoshow,
            response: '',
            showvalidate: { groupnameRequired, uniRequired, groupDescriptionRequired },
            oldData: oldData,
            page: {
                title: 'edit group',
                subtitle: 'edit group Management'
            },
            sidebar: { active: 'editgroup' }

        });

    }

};

const addgroupviewPage = async(req, res) => {


    const datatoshow = await Admin.getAllUniversities();

    res.render('pages/addgroup', {
        user: req.user,
        list: datatoshow,
        response: '',
        showvalidate: '',
        oldData: '',
        page: {
            title: 'Add Group',
            subtitle: 'Add Group Management'
        },
        sidebar: { active: 'addgroup' }

    });
};

const groupdata = async(req, res, newpath) => {

    if (newpath == null) {

        var newpath = '';
    }

    // var newpath = '';

    // if(req.file) {

    //   var originalpath= req.file.path;
    //   newpath = originalpath.replace(/\\/g, "/");
    // res.send(originalpath);
    // }
    // var fullUrl = req.protocol + '://' + req.get('host') + '/';
    // res.send(fullUrl);   


    ///////Validation start/////////
    const universityname_Required = 'Must select university';

    const oldData = req.body;

    req.checkBody('group.name')
        .notEmpty().withMessage('Group Name is required!')
        .isLength({ min: 3 }).withMessage('Group Name must be of 3 characters long.')
        .matches(/^[A-Za-z\s]+$/).withMessage('Group Name must be alphabetic.')

    req.checkBody('group.description')
        .notEmpty().withMessage('Description is required!')
        //  .isLength({max:20}).withMessage('Description must be of 20 characters long.')
        //  .matches(/^[A-Za-z\s]+$/).withMessage('Group Name must be alphabetic.')


    req.checkBody('university.name', universityname_Required).notEmpty();

    //  req.checkBody('file')
    //  .notEmpty().withMessage('Image is required!')

    const errors = req.validationErrors();

    ///////Validation End/////////

    if (!errors) {

        ///////Data pass to addsection for insertion in mongo_db/////////
        // return res.send(req.body);

        const add_group = await Admin.addgroup(req.body, newpath);

        // return res.send(req.body);

        const datatoshow = await Admin.getAllUniversities();

        let _response = { type: 'error', msg: 'Failed!' }
        if (add_group != {} && add_group != undefined && add_group != null)
            _response = { type: 'success', msg: 'Added' }

        //--Image upload --//
        // var storage = multer.diskStorage({
        //   destination: function (req, file, cb) {
        //     cb(null, 'uploads/groups')
        //   },
        //   filename: function (req, file, cb) {
        //     cb(null, Date.now() + '.png') //Appending .jpg
        //   }
        // })
        // var upload = multer({ storage: storage });
        // upload.single('image'),
        //--Image upload --//

        res.render('pages/addgroup', {
            user: req.user,
            list: datatoshow,
            response: _response,
            showvalidate: '',
            oldData: '',
            page: {
                title: 'Add Group',
                subtitle: 'Add Group Management'
            },
            sidebar: { active: 'addgroup' }

        });
        // res.redirect('back', { msg: 'added' })

    } else {

        ///////errors show to front admin panel/////////

        const groupnameRequired = errors.find(el => el.msg === 'Group Name is required!' || el.msg === 'Group Name must be of 3 characters long.' || el.msg === 'Group Name must be alphabetic.');
        const groupDescriptionRequired = errors.find(el => el.msg === 'Description is required!' || el.msg === 'Description must be of 20 characters long.');
        const uniRequired = errors.find(el => el.msg === universityname_Required);
        // const imageRequired = errors.find(el => el.msg === 'Image is required!');


        const datatoshow = await Admin.getAllUniversities();


        // console.log(oldData);
        // console.log(errors);
        // console.log(groupnameRequired);
        // console.log(uniRequired);

        console.log('data', req.body);
        res.render('pages/addgroup', {
            user: req.user,
            list: datatoshow,
            response: '',
            showvalidate: { groupnameRequired, uniRequired, groupDescriptionRequired },
            oldData,
            page: {
                title: 'Add Group',
                subtitle: 'Add Group Management'
            },
            sidebar: { active: 'addgroup' }

        });

    }

};

const groupDetailViewPage = async(req, res) => {

    const limit = 20;
    const page = +req.query.page || 1;

    const datatoshow = await Admin.getGroupDetaillist(page, limit, req.params);

    console.log(datatoshow.data);

    res.render('pages/groupdetaillisting', {
        user: req.user,
        // response: '',
        // showvalidate: '',
        // oldData: '',
        page: {
            title: 'View Group Details',
            subtitle: 'Group Details Management'
        },
        sidebar: { active: 'groupdetaillisting' },
        list: datatoshow.data,
        pagination: datatoshow.pagination

    });
};

const postlistviewPage = async(req, res) => {

    const schoolQuery = (req.query.school || '').trim();
    const courseQuery = (req.query.course || '').trim();
    const sectionQuery = (req.query.section || '').trim();
    const postQuery = (req.query.post || '').trim();
    const postType = (req.query.type || 'all').trim().toLowerCase();
    const limit = 100;
    const page = +req.query.page || 1;
    console.log('schoolQuery:', schoolQuery);
    console.log('courseQuery:', courseQuery);
    console.log('sectionQuery:', sectionQuery);
    console.log('postQuery:', postQuery);
    console.log('postType:', postType);
    console.log('limit:', limit);
    console.log('page:', page);

    // const posts = await Admin.getAllPosts(schoolQuery, courseQuery, sectionQuery, postType, page, limit);

    // const limit = 20;
    // const page = +req.query.page || 1;

    const datatoshow = await Admin.getAllPostlist(schoolQuery, courseQuery, sectionQuery, postQuery, postType, page, limit);

    // console.log(datatoshow.data);

    res.render('pages/postlistpassingdata', {
        user: req.user,
        page: {
            title: 'View Post',
            subtitle: 'Post Management'
        },
        sidebar: { active: 'postlistpassingdata' },
        response: '',
        //
        breadcrumb: {
            list: [
                { title: 'Home', link: '/admins' },
                { title: 'Posts', link: '/admins/Posts' },
            ]
        },
        //
        list: datatoshow.data,
        //
        filter: {
            schoolQuery,
            courseQuery,
            sectionQuery,
            postQuery,
            postType
        },
        //
        pagination: datatoshow.pagination

    });
};

const postListViewPageStatusEdit = async(req, res) => {

    const schoolQuery = (req.query.school || '').trim();
    const courseQuery = (req.query.course || '').trim().toUpperCase();
    const sectionQuery = (req.query.section || '').trim().toLowerCase();
    const postQuery = (req.query.post || '').trim();
    const postType = (req.query.type || 'all').trim().toLowerCase();
    const limit = 20;
    const page = +req.query.page || 1;
    console.log('schoolQuery:', schoolQuery);
    console.log('courseQuery:', courseQuery);
    console.log('sectionQuery:', sectionQuery);
    console.log('postType:', postType);
    console.log('postQuery:', postQuery);
    console.log('limit:', limit);
    console.log('page:', page);

    // const posts = await Admin.getAllPosts(schoolQuery, courseQuery, sectionQuery, postType, page, limit);

    // const limit = 20;
    // const page = +req.query.page || 1;

    const pin_updated = await Admin.pinPost(req.params);

    const datatoshow = await Admin.getAllPostlist(schoolQuery, courseQuery, sectionQuery, postQuery ,postType, page, limit);

    console.log(datatoshow.data);

    let _response = { type: 'error', msg: 'Failed!' }
    if (pin_updated != {} && pin_updated != undefined && pin_updated != null)
        _response = { type: 'success', msg: 'Updated' }

    res.render('pages/postlistpassingdata', {
        user: req.user,
        page: {
            title: 'View Post',
            subtitle: 'Post Management'
        },
        sidebar: { active: 'postlistpassingdata' },
        list: datatoshow.data,
        response: _response,
        //
        breadcrumb: {
            list: [
                { title: 'Home', link: '/admins' },
                { title: 'Posts', link: '/admins/Posts' },
            ]
        },
        //
        //
        filter: {
            schoolQuery,
            courseQuery,
            sectionQuery,
            postQuery,
            postType
        },
        //
        pagination: datatoshow.pagination

    });
};

const postListViewPageNotify = async(req, res) => {

    const schoolQuery = (req.query.school || '').trim();
    const courseQuery = (req.query.course || '').trim().toUpperCase();
    const sectionQuery = (req.query.section || '').trim().toLowerCase();
    const postType = (req.query.type || 'all').trim().toLowerCase();
    const limit = 20;
    const page = +req.query.page || 1;
    console.log('schoolQuery:', schoolQuery);
    console.log('courseQuery:', courseQuery);
    console.log('sectionQuery:', sectionQuery);
    console.log('postType:', postType);
    console.log('limit:', limit);
    console.log('page:', page);

    // const posts = await Admin.getAllPosts(schoolQuery, courseQuery, sectionQuery, postType, page, limit);

    // const limit = 20;
    // const page = +req.query.page || 1;

    var pin_updated = await Admin.trendingPostNotification(req.user, req.params);


    // console.log('here',members);
    // return res.send(members);


    const datatoshow = await Admin.getAllPostlist(schoolQuery, courseQuery, sectionQuery, postType, page, limit);

    // console.log(datatoshow.data);


    let _response = { type: 'error', msg: 'Failed!' }
    if (pin_updated != {} && pin_updated != undefined && pin_updated != null)
        _response = { type: 'success', msg: 'Updated' }

    res.send({ response: _response });

    // res.render('pages/postlistpassingdata', {
    //   user: req.user,
    //   page: {
    //     title: 'View Post',
    //     subtitle: 'Post Management'
    //   },
    //   sidebar: { active: 'postlistpassingdata' },
    //   list: datatoshow.data,
    //   response: _response,
    //   pagination: datatoshow.pagination

    // });
};

const postDelete = async(req, res) => {

    const schoolQuery = (req.query.school || '').trim();
    const courseQuery = (req.query.course || '').trim().toUpperCase();
    const sectionQuery = (req.query.section || '').trim().toLowerCase();
    const postType = (req.query.type || 'all').trim().toLowerCase();
    const limit = 20;
    const page = +req.query.page || 1;
    console.log('schoolQuery:', schoolQuery);
    console.log('courseQuery:', courseQuery);
    console.log('sectionQuery:', sectionQuery);
    console.log('postType:', postType);
    console.log('limit:', limit);
    console.log('page:', page);

    const post_deleted = await Admin.removePost(req.params);

    const datatoshow = await Admin.getAllPostlist(schoolQuery, courseQuery, sectionQuery, postType, page, limit);

    console.log(datatoshow.data);

    let _response = { type: 'error', msg: 'Failed!' }
        // if (pin_updated != {} && pin_updated != undefined && pin_updated != null)
    if (post_deleted == true)
        _response = { type: 'success', msg: 'Deleted' }

    res.render('pages/postlistpassingdata', {
        user: req.user,
        page: {
            title: 'View Post',
            subtitle: 'Post Management'
        },
        sidebar: { active: 'postlistpassingdata' },
        list: datatoshow.data,
        response: _response,
        pagination: datatoshow.pagination

    });


}

const editSettingsViewPage = async(req, res) => {

    var oldData = await Admin.getSettingsData();
    // console.log('yahanhai', oldData);
    res.render('pages/editsettings', {
        user: req.user,
        // list: datatoshow,
        response: '',
        showvalidate: '',
        oldData: oldData,
        page: {
            title: 'Edit Settings',
            subtitle: 'Edit Settings Management'
        },
        sidebar: { active: 'editsettings' }

    });

}

const editSettingsData = async(req, res) => {

    var addpost_isNotified = 'false';
    var addcomment_isNotified = 'false';
    var likepost_isNotified = 'false';
    var unlikepost_isNotified = 'false';
    // var isDefault = 'false';


    if (req.body.addpost_isNotified) {
        req.body.addpost_isNotified = 'true';
        addpost_isNotified = true;
    }
    if (req.body.addcomment_isNotified) {
        req.body.addcomment_isNotified = 'true';
        addcomment_isNotified = true;
    }

    if (req.body.likepost_isNotified) {
        req.body.likepost_isNotified = 'true';
        likepost_isNotified = true;
    }

    if (req.body.unlikepost_isNotified) {
        req.body.unlikepost_isNotified = 'true';
        unlikepost_isNotified = true;
    }

    ///////Validation start/////////

    const oldData = req.body;

    // return res.send(req.body);

    // req.checkBody('group.description')
    //     .notEmpty().withMessage('Description is required!')
    //     .isLength({ max: 20 }).withMessage('Description must be of 20 characters long.');

    const errors = req.validationErrors();

    ///////Validation End////////

    // return res.send(req.body);
    if (!errors) {

        ///////Data pass to editsetting for insertion in mongo_db/////////
        const settings_updated = await Admin.updateSetting(req.body, req.params, addpost_isNotified, addcomment_isNotified, likepost_isNotified, unlikepost_isNotified);


        const oldData = await Admin.getSettingsData();


        let _response = { type: 'error', msg: 'Failed!' }
        if (settings_updated != {} && settings_updated != undefined && settings_updated != null)
            _response = { type: 'success', msg: 'Updated' }

        res.render('pages/editsettings', {
            user: req.user,
            response: _response,
            showvalidate: '',
            oldData: oldData,
            page: {
                title: 'Edit Settings',
                subtitle: 'Edit Settings Management'
            },
            sidebar: { active: 'editsettings' }

        });
        // res.redirect('back', { msg: 'added' });

    } else {

        ///////errors show to front admin panel/////////

        // const groupnameRequired = errors.find(el => el.msg === 'Group Name is required!' || el.msg === 'Group Name must be of 3 characters long.' || el.msg === 'Group Name must be alphabetic.');
        // const uniRequired = errors.find(el => el.msg === universityname_Required);
        // const groupDescriptionRequired = errors.find(el => el.msg === 'Description is required!' || el.msg === 'Description must be of 20 characters long.');

        const oldData = await Admin.getSettingsData();

        // console.log(oldData);
        // console.log(errors);
        res.render('pages/editsettings', {
            user: req.user,
            response: '',
            showvalidate: '',
            // showvalidate: { groupnameRequired, uniRequired, groupDescriptionRequired },
            oldData: oldData,
            page: {
                title: 'Edit Settings',
                subtitle: 'Edit Settings Management'
            },
            sidebar: { active: 'editsettings' }

        });

    }

};

const resetPasswordViewPage = async(req, res) => {

    res.render('pages/resetpage', {
        response: '',
        showvalidate: '',
    });
};

const resetPasswordUpdated = async(req,res)=>{

    // console.log(req.body,req.params);
     ///////Validation start////////
    
        req.checkBody('newpassword')
            .notEmpty().withMessage('New password is required!')
            .isLength({ min: 8 }).withMessage('New password must be of 8 characters long.')

        req.checkBody('confirmpassword')
        .notEmpty().withMessage('Confirm password is required!')
        .isLength({ min: 8 }).withMessage('Confirm password must be of 8 characters long.')
    
        req.checkBody('newpassword','Passwords do not match.').equals(req.body.confirmpassword)
        .withMessage('Passwords do not match.');

        const errors = req.validationErrors();
        ///////Validation End/////////
    
        if (!errors) {
    
                const pass_updated = await Admin.resetPasswordUpdated(req.body,req.params);

                let _response = { type: 'error', msg: 'Something went wrong !' }
                if (pass_updated != {} && pass_updated != undefined && pass_updated != null)
                    _response = { type: 'success', msg: 'Updated' }

                    res.render('pages/resetpage', {
                        response: _response,
                        showvalidate: '',
                    });
                // res.redirect(`/api/v4/user/deeplink?url=strapp://open/`);
    
        } else {
    
            ///////errors show to front admin panel/////////
            const newpasswordRequired = errors.find(el => el.msg === 'New password is required!' || el.msg === 'New password must be of 8 characters long.');
            const confirmpasswordRequired = errors.find(el => el.msg === 'Confirm password is required!' || el.msg === 'Confirm password must be of 8 characters long.');
            const notmatch = errors.find(el => el.msg === 'Passwords do not match.');

            res.render('pages/resetpage', {
                response: '',
                showvalidate: {newpasswordRequired, confirmpasswordRequired, notmatch},
            });
    
        }

}

module.exports = {
    loginPage,
    homePage,
    studentsPage,
    exportStudents,
    sectionsPage,
    postsPage,
    commentsPage,
    // groupsCategoryPage,
    // groupCategoryAdd,
    // addgroupviewPage,
    // groupdata,
    universitylistviewPage,
    editUniversityviewPage,
    editUniversitydata,
    adduniversityviewPage,
    universitydata,
    // sectionlistviewPage1,
    sectionlistviewPage,
    addsectionviewPage,
    sectiondata,
    editsectionviewPage,
    editsectiondata,
    grouplistviewPage,
    groupDetailViewPage,
    groupListViewPageStatusEdit,
    editgroupviewPage,
    editgroupdata,
    addgroupviewPage,
    groupdata,
    postlistviewPage,
    postListViewPageStatusEdit,
    postListViewPageNotify,
    postDelete,
    editSettingsViewPage,
    editSettingsData,

    resetPasswordViewPage,
    resetPasswordUpdated,
};