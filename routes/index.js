const router = require('express').Router();
const passport = require('../conf/v2/auth');
const { getPic, responseStateHandler } = require('../middleware');
const api = require('./api');
const adminRouter = require('./admin');

router.use(responseStateHandler);

router.use('/api', api);

router.get(
  '/uploads/:folder/:file',
  // passport.authenticate('user-bearer', { session: false }),
  getPic
);

router.use('/admins', adminRouter);

module.exports = router;
