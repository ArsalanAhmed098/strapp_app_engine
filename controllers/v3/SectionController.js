const { Section } = require('../../services/v3');

const add = async (req, res) => {
  const { courseName } = req.params;
  const {
    name: sectionName,
    number: sectionNumber,
    category: sectionCategory
  } = req.body;
  const section = await Section.add(req.user, courseName, sectionName, sectionNumber, sectionCategory);
  res.success(section);
};

const getAllByCourse = async (req, res) => {
  const { courseName } = req.params;
  const sections = await Section.getAllByCourse(req.user, courseName);
  res.success(sections);
};

const getAll = async (req, res) => {
  const sections = await Section.getAll(req.user);
  res.success(sections);
};

// const getAllCampusSections = async (req, res) => {
//   const campussections = await Section.getAllCampusSections(req.user);
//   res.success(campussections);
// };

const get = async (req, res) => {
  const section = await Section.get(req.user, req.params.sectionId);
  res.success(section);
};

const join = async (req, res) => {
  const section = await Section.join(req.user, req.params.sectionId);
  res.success(section);
};

const leave = async (req, res) => {
  await Section.leave(req.user, req.params.sectionId);
  res.success();
};

const updateSeen = async (req, res) => {
  await Section.updateSeen(req.user, req.params.sectionId);
  res.success();
};

const getAllMembers = async (req, res) => {
  const users = await Section.getAllMembers(req.params.sectionId);
  res.success(users);
};

module.exports = {
  add,
  getAllByCourse,
  getAll,
  get,
  join,
  leave,
  updateSeen,
  getAllMembers,
  // getAllCampusSections,
};
