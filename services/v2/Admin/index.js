const auth = require('./auth');
const action = require('./action');

module.exports = {
  ...auth,
  ...action,
}
