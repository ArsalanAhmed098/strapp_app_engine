const mongoose = require('mongoose');
const { SectionModel, UserSeenGroupModel, CampusGroupsModel } = require('../../models/v2');
const User = require('./User');
const ObjectId = mongoose.Types.ObjectId;

//-----------Old Work Only Category show in this Api Explore List-------------// 

// const getAllCampusGroupsOLD = async (user,fullUrl) => {

//   console.log(fullUrl);
    
//   const campusExploreListing = await CampusGroupsModel.aggregate([
//     {
//       $match: {
//         // userId: user._id,
//         'isDeleted': { $ne: true },
//         'university.name': user.university.name
//       },
//     },
//     {
//       $lookup: {
//         from: "campusgroups",
//         pipeline: [
//           {
//             $match: {
//               $expr: {
//                 $and: [
//                   { $ne: ["$isDeleted", true] },
//                   { $eq: ["$university.name", user.university.name] },
//                   { $eq: ["$grouptype", "campusgroup"] },
                  
//                 ],
//               },
//             },
//           },
//         ],
//         as: "campusgroups",
//       },
//     },
//     {
//       $project: {
//           _id:true,
//           groupname: "$group.name",
//           description:"$group.description",
//           grouptype: "$grouptype",
//           // photo: { $concat: [ fullUrl,"$photo" ] },
//           photo: "$photo",
//           university: true,
//         },
//       },
//   ]).allowDiskUse(true);
//   return campusExploreListing;
// };

const getAllCampusGroups = async (user,fullUrl) => {
  // console.log(fullUrl);
  const campusExploreListing = await CampusGroupsModel.aggregate([
    {
      $match: {
        // userId: user._id,
        'isDeleted': { $ne: true },
        'university.name': user.university.name
      },
    },
    {
      $lookup: {
        from: "campusgroups",
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $ne: ["$isDeleted", true] },
                  { $eq: ["$university.name", user.university.name] },
                  { $eq: ["$grouptype", "campusgroup"] },
                  
                ],
              },
            },
          },
        ],
        as: "campusgroups",
      },
    },
    // {
    //   $lookup: {
    //     from: "sections",
    //     let: { cId: "$_id" },
    //     pipeline: [
    //       {
    //         $match: {
    //           $expr: {
    //             $and: [
    //               { $eq: ["$campusgroupId", "$$cId"] },
    //               { $ne: ["$isDeleted", true] },
    //               { $eq: ["$university.name", user.university.name] },
    //               { $eq: ["$grouptype", "campusgroup"] },
                  
    //             ],
    //           },
    //         },
    //       },
    //       { $project : { _id: 0, id:'$_id', title:'$section.name', photo:1, isGeneral:1, description:'$group.description', grouptype:1,
    //     } }
    //     ],
    //     as: "section"
    //   }
    // },
    { "$lookup": {
      "from": "sections",
      "let": { "camId": "$_id" },
      "pipeline": [
        { "$match": { "$expr": {
                      "$and": [
                        { "$eq": ["$campusgroupId", "$$camId"] },
                        { "$ne": ["$isDeleted", true] },
                        { "$eq": ["$university.name", user.university.name] },
                        { "$eq": ["$grouptype", "campusgroup"] },
                        
                      ],
                    }, } },
        { "$lookup": {
          "from": "userseengroups",
          "let": { "secId": "$_id" },
          "pipeline": [
            { "$match": { "$expr": {
              "$and": [
                { "$eq": ["$groupId", "$$secId"] },
                { "$eq": ["$userId", user._id] },
              ],
            }, } },
          ],
          "as": "member"
        }},
        // { "$unwind": "$member" },
           {
      $unwind: {
        path: '$member',
        preserveNullAndEmptyArrays: true
      }
    },
        { $project : { _id: 0, id:'$_id', title:'$section.name', photo:1, isGeneral:1, description:'$group.description', grouptype:1,
        // member:"$member.userId"
        currentUserJoined: {
              $cond: ['$member.userId', true, false]
           }, 
      } }
      ],
      "as": "section"
    }},
    // { "$unwind": "$section" },
    {
      $project: {
          _id:true,
          sections_data: '$section',
          groupname: "$group.name",
          description:"$group.description",
          grouptype: "$grouptype",
          // photo: { $concat: [ fullUrl,"$photo" ] },
          photo: "$photo",
          university: true,
        },
      },
  ]).allowDiskUse(true);
  return campusExploreListing;
};

// currentUserJoined: {
//   $cond: ['$membership', true, false]
// }

module.exports = {
getAllCampusGroups,
// getAllCampusGroupsOLD
};
