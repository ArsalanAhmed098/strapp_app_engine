
const responseStateHandler = (req, res, next) => {
  const stateHandlers = {
    success: (data, statusCode = 200) =>
      res.status(statusCode).json({
        success: true,
        data,
        // message: 'data fetched successfully'
      }),
    fail: (errorCode = 'INTERNAL_SERVER_ERROR', statusCode = 500) =>
      next({
        errorCode,
        statusCode,
      }),
  };
  Object.assign(res, stateHandlers);
  next();
};

module.exports = responseStateHandler;
