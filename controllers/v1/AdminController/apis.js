const mongoose = require('mongoose');
const { UserModel, GroupModel, UserSeenGroupModel, PostModel } = require('../../../models/v1');

const me = async (req, res) => {
  req.user.password = undefined;
  res.status(200).json({
    data: {
      user: req.user,
    },
    success: true,
    message: 'logged in admin',
    errorCode: null
  });
};

const studentBlock = async (req, res) => {
  let student = await UserModel.findById(req.params.studentId);
  if (!student) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'Student not found',
      errorCode: 'STUDENT_NOT_FOUND'
    });
  }
  student = await student.permission(req.body.status);
  return res.status(200).json({
    success: true,
    data: student,
    message: 'Student block status changed successfully',
  });
};

// const studentUnBlock = async (req, res) => {
//   const student = await UserModel.findByIdAndUpdate(req.params.studentId, {
//     isBlocked: false
//   }, { new: true });
//   if (!student) {
//     return res.status(404).json({
//       success: false,
//       data: null,
//       message: 'Student not found',
//       errorCode: 'STUDENT_NOT_FOUND'
//     });
//   }

//   return res.status(200).json({
//     success: true,
//     data: student,
//     message: 'Student unBlocked successfully',
//   });
// };

// const studentRemove = async (req, res) => {
//   const student = await UserModel.findByIdAndUpdate(req.params.studentId, {
//     isDeleted: true
//   }, { new: true });
//   if (!student) {
//     return res.status(404).json({
//       success: false,
//       data: null,
//       message: 'Student not found',
//       errorCode: 'STUDENT_NOT_FOUND'
//     });
//   }

//   return res.status(200).json({
//     success: true,
//     data: student,
//     message: 'Student removed successfully',
//   });
// };

// const studentUnRemove = async (req, res) => {
//   const student = await UserModel.findByIdAndUpdate(req.params.studentId, {
//     isDeleted: false
//   }, { new: true });
//   if (!student) {
//     return res.status(404).json({
//       success: false,
//       data: null,
//       message: 'Student not found',
//       errorCode: 'STUDENT_NOT_FOUND'
//     });
//   }

//   return res.status(200).json({
//     success: true,
//     data: student,
//     message: 'Student unRemoved successfully',
//   });
// };

// const classBlock = async (req, res) => {
//   const group = await GroupModel.findByIdAndUpdate(req.params.classId, {
//     isBlocked: true
//   }, { new: true });
//   if (!group) {
//     return res.status(404).json({
//       success: false,
//       data: null,
//       message: 'Class not found',
//       errorCode: 'CLASS_NOT_FOUND'
//     });
//   }

//   return res.status(200).json({
//     success: true,
//     data: group,
//     message: 'Class blocked successfully',
//   });
// };

// const classUnBlock = async (req, res) => {
//   const group = await GroupModel.findByIdAndUpdate(req.params.classId, {
//     isBlocked: false
//   }, { new: true });
//   if (!group) {
//     return res.status(404).json({
//       success: false,
//       data: null,
//       message: 'Class not found',
//       errorCode: 'CLASS_NOT_FOUND'
//     });
//   }

//   return res.status(200).json({
//     success: true,
//     data: group,
//     message: 'Class unBlocked successfully',
//   });
// };

// const classRemove = async (req, res) => {
//   const group = await GroupModel.findByIdAndUpdate(req.params.classId, {
//     isDeleted: true
//   }, { new: true });
//   if (!group) {
//     return res.status(404).json({
//       success: false,
//       data: null,
//       message: 'Class not found',
//       errorCode: 'CLASS_NOT_FOUND'
//     });
//   }

//   return res.status(200).json({
//     success: true,
//     data: group,
//     message: 'Class removed successfully',
//   });
// };

// const classUnRemove = async (req, res) => {
//   const group = await GroupModel.findByIdAndUpdate(req.params.classId, {
//     isDeleted: false
//   }, { new: true });
//   if (!group) {
//     return res.status(404).json({
//       success: false,
//       data: null,
//       message: 'Class not found',
//       errorCode: 'CLASS_NOT_FOUND'
//     });
//   }

//   return res.status(200).json({
//     success: true,
//     data: group,
//     message: 'Class unRemoved successfully',
//   });
// };

const postRemove = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(req.params.postId, {
    isDeleted: true
  }, { new: true });
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'Post not found',
      errorCode: 'POST_NOT_FOUND'
    });
  }

  return res.status(200).json({
    success: true,
    data: post,
    message: 'Post removed successfully',
  });
};

const commentRemove = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(
    req.body.postId,
    {
      $set: {'comments.$[elem].isDeleted': true}
    },
    {
      arrayFilters: [{ 'elem._id': req.params.commentId }],
      new: true
    }
  );

  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }

  const comment = post.comments.find(comment => req.params.commentId.toString() === comment._id.toString())._doc;
  if (!comment) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'comment not fount',
      errorCode: 'COMMENT_NOT_FOUND',
    });
  }

  res.status(200).json({
    success: true,
    data: null,
    message: 'Comment removed successfully',
    errorCode: null,
  });
};

module.exports = {
  me,
  studentBlock,
  // studentUnBlock,
  // studentRemove,
  // studentUnRemove,
  // classBlock,
  // classUnBlock,
  // classRemove,
  // classUnRemove,
  postRemove,
  commentRemove,
};
