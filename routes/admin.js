const router = require('express').Router();
const passport = require('../conf/v2/auth');
const { AdminController } = require('../controllers/v2');
// var multer  = require('multer');
const Multer = require('multer');

// var upload = multer({ dest: 'uploads/groups' })

// var storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, 'uploads/groups')
//   },
//   filename: function (req, file, cb) {
//     cb(null, Date.now() + '.png') //Appending .jpg
//   }
// })
// var upload = multer({ storage: storage });

// firebase work
const { Storage } = require('@google-cloud/storage');
const { format } = require('util');
const uuidv4 = require('uuid/v4');
const uuid = uuidv4();

router.route('/register')
    .post(AdminController.register);

router.route('/reset/password/:resettoken')
    .get(AdminController.resetPasswordViewPage);

router.route('/reset/password/:resettoken')
    .post(AdminController.resetPasswordUpdated);

router.route('/login')
    .get(
        AdminController.authLocal,
        AdminController.loginPage
    )
    .post(
        passport.authenticate('admin-local', { session: false }),
        AdminController.login
    );
router.route('/logout')
    .post(
        passport.authenticate('admin-cookie', { session: false }),
        AdminController.logout
    );
router.route('/me')
    .get(
        passport.authenticate('admin-cookie', { session: false }),
        AdminController.get
    );

router.route('/students/export')
    .get(
        passport.authenticate('admin-cookie', { session: false }),
        AdminController.exportStudents
    );

router.route('/students/:studentId/block')
    .patch(
        passport.authenticate('admin-cookie', { session: false }),
        AdminController.blockStudent
    );

// router.route('/posts/:postId/remove')
//     .get(
//         passport.authenticate('admin-cookie', { session: false }),
//         AdminController.postDelete
//     );

router.route('/comments/:commentId/remove')
    .patch(
        passport.authenticate('admin-cookie', { session: false }),
        AdminController.removeComment
    );


router.use(AdminController.authCookie);

router.route('/')
    .get(AdminController.homePage);
router.route('/students')
    .get(AdminController.studentsPage);
router.route('/sections')
    .get(AdminController.sectionsPage);
router.route('/posts')
    .get(AdminController.postsPage);

// router.route('/groupscategory')
//   .get(AdminController.groupsCategoryPage);

// router.route("/groupcategorysave")
//   .post(AdminController.groupCategoryAdd);

// router.post('/groupscategory-form', (req, res) => {
//   // const username = req.body.username
//   //...
//   console.log('data:', req.body.type);
//   res.send('data');
// })

// router.post("/groupcategorysave", function (req, res) {
//   var type= req.body.type;
//   console.log(type);
//   res.send(type);
//  });

// router.route('/addgroup')
// .get(AdminController.addgroupviewPage);

// router.route("/addgroupsave")
// .post(AdminController.groupdata);

// router.route('/sectionlistpassingdata1')
// .get(AdminController.sectionlistviewPage1);

router.route('/postlistpassingdata')
    .get(AdminController.postlistviewPage);

router.route('/postlistpassingdata/:_id/:value')
    .get(AdminController.postListViewPageStatusEdit);

router.route('/postlistpassingdata/:postId')
    .get(AdminController.postDelete);

router.route('/postlistpassingdata/notify/:_id/:groupId/:value')
    .get(AdminController.postListViewPageNotify);

router.route('/universitylistpassingdata')
    .get(AdminController.universitylistviewPage);

router.route('/universitylistpassingdata/:_id')
    .get(AdminController.editUniversityviewPage);

router.route('/edituniversitysave/:_id')
    .post(AdminController.editUniversitydata);

router.route('/adduniversity')
    .get(AdminController.adduniversityviewPage);

router.route("/adduniversitysave")
    .post(AdminController.universitydata);

router.route('/grouplistpassingdata')
    .get(AdminController.grouplistviewPage);

router.route('/grouplistpassingdata/:_id')
    .get(AdminController.editgroupviewPage);

router.route('/groupdetaillisting/groupdelete/:_id')
    .get(AdminController.groupListViewPageStatusEdit);

router.route('/groupdetaillisting/:_id')
    .get(AdminController.groupDetailViewPage);


///-- settings admin routes --////

router.route('/editsettings')
    .get(AdminController.editSettingsViewPage);

router.route('/editsettingsave/:_id')
    .post(AdminController.editSettingsData);


// router.route('/editgroupsave/:_id')
// .post(upload.single('image'),AdminController.editgroupdata);

// uncomment it
// firebase work
const storage = new Storage({
    projectId: "strapp-test",
    keyFilename: "strapp-test-firebase-adminsdk-4191s-5d72ee1f78.json",
});

const bucket = storage.bucket("gs://strapp-test.appspot.com");

const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
        fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
    }
});
// router.route('/editgroupsave/:_id')
// .post(upload.single('file'),AdminController.editgroupdata);
router.route('/editgroupsave/:_id').post(multer.single('file'), (req, res) => {
    let file = req.file;
    if (file) {
        uploadImageToStorage(file).then((success) => {
            AdminController.editgroupdata(req, res, newpath = success)
                // res.status(200).send({
                //   success
                // });
        }).catch((error) => {
            AdminController.editgroupdata(req, res, newpath = '')
                // console.error(error);
        });
    } else {
        AdminController.editgroupdata(req, res, newpath = '')
    }
});


/**
 * Upload the image file to Google Storage
 * @param {File} 
 * file object that will be uploaded to Google Storage
 */
const uploadImageToStorage = (file) => {
    return new Promise((resolve, reject) => {
        if (!file) {
            reject('No image file');
        }
        // var newFileName = `${Date.now()}_${file.originalname}`;
        var newFileName = `${Date.now()}`;
        // console.log('namehere',newFileName);
        var fileUpload = bucket.file(`groups/${newFileName}`);

        const blobStream = fileUpload.createWriteStream({
            metadata: {
                contentType: file.mimetype,
                metadata: { firebaseStorageDownloadTokens: uuid }
            }
        });

        blobStream.on('error', (error) => {
            console.log(error);
            reject('Something is wrong! Unable to upload at the moment.', error);
        });

        blobStream.on('finish', () => {
            // The public URL can be used to directly access the file via HTTP.
            // const url = format(`https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`);
            const newpath = format(`https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/groups%2F${newFileName}?alt=media&token=${uuid}`);

            console.log(newpath);
            resolve(newpath);
        });

        blobStream.end(file.buffer);
    });
}

router.route('/addgroup')
    .get(AdminController.addgroupviewPage);

//uncomment it 
// router.route("/addgroupsave")
// .post(upload.single('image'),AdminController.groupdata);
router.route('/addgroupsave').post(multer.single('file'), (req, res) => {
    let file = req.file;
    if (file) {
        uploadImageToStorage(file).then((success) => {
            AdminController.groupdata(req, res, newpath = success)
                // res.status(200).send({
                //   success
                // });
        }).catch((error) => {
            AdminController.groupdata(req, res, newpath = '')
                // console.error(error);
        });
    } else {
        AdminController.groupdata(req, res, newpath = '')
    }
});

router.route('/sectionlistpassingdata')
    .get(AdminController.sectionlistviewPage);

router.route('/addsection')
    .get(AdminController.addsectionviewPage);


//uncomment it 
// router.route("/addsectionsave")
// .post(upload.single('image'),AdminController.sectiondata);
router.route('/addsectionsave').post(multer.single('file'), (req, res) => {
    let file = req.file;
    if (file) {
        uploadImageToStorage(file).then((success) => {
            AdminController.sectiondata(req, res, newpath = success)
                // res.status(200).send({
                //   success
                // });
        }).catch((error) => {
            AdminController.sectiondata(req, res, newpath = '')
                // console.error(error);
        });
    } else {
        AdminController.sectiondata(req, res, newpath = '')
    }
});

router.route('/sectionlistpassingdata/:_id')
    .get(AdminController.editsectionviewPage);

//uncomment it 
// router.route('/editsectionsave/:_id')
// .post(upload.single('image'),AdminController.editsectiondata);
router.route('/editsectionsave/:_id').post(multer.single('file'), (req, res) => {
    let file = req.file;
    if (file) {
        uploadImageToStorage(file).then((success) => {
            AdminController.editsectiondata(req, res, newpath = success)
                // res.status(200).send({
                //   success
                // });
        }).catch((error) => {
            AdminController.editsectiondata(req, res, newpath = '')
                // console.error(error);
        });
    } else {
        AdminController.editsectiondata(req, res, newpath = '')
    }
});

router.route('/posts/:postId/comments')
    .get(AdminController.commentsPage);

router.use((req, res) => {
    console.log('redirect');
    res.redirect('/admins');
});

module.exports = router;
module.exports = router;