require('dotenv').config();
require('../../conf/db');
require('../../util/throw-error');
// require('./import-universities');
const mongoose = require('mongoose');

const insertUsers = require('./users');
const insertGroups = require('./groups');
const insertPosts = require('./posts');

(async () => {
  try {
    // try {
    //   await mongoose.connection.collections.notifications.drop();
    //   console.log('====================> notifications collection is droped! <====================');
    // } catch { }

    try {
      await mongoose.connection.collections.students.drop();
      console.log('====================> students collection is droped! <====================');
    } catch { }

    await insertUsers();
    console.log('====================> insert new students! <====================');

    try {
      await mongoose.connection.collections.sections.drop();
      console.log('====================> section collection is droped! <====================');
    } catch { }

    // try {
    //   await mongoose.connection.collections.userseengroups.drop();
    //   console.log('====================> userseengroups collection is droped! <====================');
    // } catch { }

    await insertGroups();
    console.log('====================> insert new sections! <====================');

    // try {
    //   await mongoose.connection.collections.posts.drop();
    //   console.log('====================> posts collection is droped! <====================');
    // } catch { }

    await insertPosts();
    console.log('====================> insert new posts! <====================');

  } catch(err) {
    console.log('What is wrong:', err);

  } finally {
    process.exit(0);
  }
})();

