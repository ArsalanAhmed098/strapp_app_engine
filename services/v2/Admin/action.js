const mongoose = require('mongoose');
const moment = require('moment');
const { StudentModel, SectionModel, PostModel, GroupCategoryModel, CampusGroupsModel, SettingModel, UserSeenGroupModel } = require('../../../models/v2');
const { GroupModel } = require('../../../models/v1');
const { UniversityModel } = require('../../../models/v1');
const User = require('../User');
const User1 = require('../../v4/User');
const { json2csv } = require('../../../util');
const fs = require('fs').promises;
const path = require('path');
const { time } = require('console');
const userSeenGroup = require('../../../models/common/user-seen-group');
const { send } = require('process');
const {
    sendMail,
    genExpire,
    Password,
  } = require('../../../util');
const ObjectId = mongoose.Types.ObjectId;
var save;
var saveSection;
var saveGroupAdmin;

const get = user => {
    user.password = undefined;
    return user;
};

const exportStudents = async(schoolQuery, courseQuery, sectionQuery, studentQuery) => {
    const schoolReg = new RegExp(schoolQuery, 'i');
    const studentReg = new RegExp(studentQuery, 'i');
    const pipeline = [{
            $match: {
                'university.name': schoolReg,
                $or: [
                    { firstName: studentReg },
                    { lastName: studentReg },
                    { username: studentReg },
                    { email: studentReg },
                ]
            }
        },
        {
            $match: {
                $expr: {
                    $ne: ['$isDeleted', true],
                }
            }
        },
    ];

    if (courseQuery || sectionQuery) {
        pipeline.push({
            $lookup: {
                from: 'userseengroups',
                let: { uId: '$_id' },
                pipeline: [{
                        $match: {
                            $expr: {
                                $eq: ['$userId', '$$uId']
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: 'sections',
                            let: { gId: '$groupId' },
                            pipeline: [{
                                $match: (() => {
                                    const match = {
                                        $expr: {
                                            $and: [
                                                { $ne: ['$isDeleted', true] },
                                                { $eq: ['$_id', '$$gId'] },
                                            ]
                                        }
                                    };
                                    if (courseQuery)
                                        match['$expr']['$and'].push({ $eq: ['$course.name', courseQuery] })
                                    if (sectionQuery)
                                        match['$expr']['$and'].push({ $eq: ['$section.name', sectionQuery] })
                                    return match;
                                })()
                            }],
                            as: 'group'
                        }
                    },
                    { $unwind: '$group' }
                ],
                as: 'groupSeens'
            }
        }, {
            $match: {
                groupSeens: { $not: { $size: 0 } }
            }
        }, );
    }

    pipeline.push({
        $project: {
            _id: false,
            firstName: {
                $ifNull: ['$firstName', '']
            },
            lastName: {
                $ifNull: ['$lastName', '']
            },
            email: true,
            username: {
                $ifNull: ['$username', '']
            },
            major: {
                $ifNull: ['$major', '']
            },
            verification: '$verification.status',
            createdAt: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
        }
    });

    const result = await StudentModel.aggregate(pipeline).allowDiskUse(true);

    const csvContent = json2csv(result);
    const filePath = path.join(process.cwd(), 'uploads', 'students.csv');
    await fs.writeFile(filePath, csvContent);

    return filePath;
};



const getAllStudents = async(schoolQuery, courseQuery, sectionQuery, studentQuery, page, limit) => {
    const schoolReg = new RegExp(schoolQuery, 'i');
    const studentReg = new RegExp(studentQuery, 'i');
    const pipeline = [{
            $match: {
                'university.name': schoolReg,
                $or: [
                    { firstName: studentReg },
                    { lastName: studentReg },
                    { username: studentReg },
                    { email: studentReg },
                ]
            }
        },
        {
            $match: {
                $expr: {
                    $ne: ['$isDeleted', true],
                }
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                localField: '_id',
                foreignField: 'userId',
                as: 'groupCount'
            }
        }
    ];

    if (courseQuery || sectionQuery) {
        pipeline.push({
            $lookup: {
                from: 'userseengroups',
                let: { uId: '$_id' },
                pipeline: [{
                        $match: {
                            $expr: {
                                $eq: ['$userId', '$$uId']
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: 'sections',
                            let: { gId: '$groupId' },
                            pipeline: [{
                                $match: (() => {
                                    const match = {
                                        $expr: {
                                            $and: [
                                                { $ne: ['$isDeleted', true] },
                                                { $eq: ['$_id', '$$gId'] },
                                            ]
                                        }
                                    };
                                    if (courseQuery)
                                        match['$expr']['$and'].push({ $eq: ['$course.name', courseQuery] })
                                    if (sectionQuery)
                                        match['$expr']['$and'].push({ $eq: ['$section.name', sectionQuery] })
                                    return match;
                                })()
                            }],
                            as: 'group'
                        }
                    },
                    { $unwind: '$group' }
                ],
                as: 'groupSeens'
            }
        }, {
            $match: {
                groupSeens: { $not: { $size: 0 } }
            }
        }, );
    }

    pipeline.push({
        $project: {
            firstName: true,
            lastName: true,
            photo: true,
            email: true,
            username: true,
            major: true,
            verification: true,
            'blockStatus': '$block.status',
            groupCount: { $size: '$groupCount' },
            createdAt: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
        }
    });

    const dataPagination = [];
    const skip = (page - 1) * limit;
    dataPagination.push({ $skip: skip });
    if (limit)
        dataPagination.push({ $limit: limit });
    pipeline.push({
        '$facet': {
            pagination: [{ $count: "total" }, { $addFields: { limit, page } }],
            data: dataPagination
        },
    }, {
        $unwind: '$pagination'
    });

    const result = await StudentModel.aggregate(pipeline).allowDiskUse(true);
    return result.length ? result[0] : {
        pagination: {
            total: 0,
            limit,
            page
        },
        data: []
    };
};


const getAllSections = async(schoolQuery, courseQuery, sectionQuery, studentQuery, page, limit) => {
    const schoolReg = new RegExp(schoolQuery, 'i');
    const pipeline = [{
            $match: (() => {
                const matchQuery = {
                    isDeleted: { $ne: true },
                    'university.name': schoolReg,
                };
                if (courseQuery)
                    matchQuery['course.name'] = courseQuery;
                if (sectionQuery)
                    matchQuery['section.name'] = sectionQuery;
                return matchQuery;
            })()
        },
        {
            $lookup: {
                from: 'userseengroups',
                localField: '_id',
                foreignField: 'groupId',
                as: 'users'
            }
        },
    ];
    if (studentQuery)
        pipeline.push({
            $match: {
                'users.userId': mongoose.Types.ObjectId(studentQuery)
            }
        });
    pipeline.push({
        $lookup: {
            from: 'posts',
            localField: '_id',
            foreignField: 'groupId',
            as: 'posts'
        }
    }, { $sort: { createdAt: -1 } }, {
        $project: {
            school: '$university.name',
            course: true,
            section: true,
            memberCount: { $size: '$users' },
            postCount: { $size: '$posts' },
        }
    });

    const dataPagination = [];
    const skip = (page - 1) * limit;
    dataPagination.push({ $skip: skip });
    if (limit)
        dataPagination.push({ $limit: limit });
    pipeline.push({
        '$facet': {
            pagination: [{ $count: "total" }, { $addFields: { limit, page } }],
            data: dataPagination
        },
    }, {
        $unwind: '$pagination'
    });

    const result = await SectionModel.aggregate(pipeline).allowDiskUse(true);
    console.log('result:', result);
    return result.length ? result[0] : {
        pagination: {
            total: 0,
            limit,
            page
        },
        data: []
    };
};

const getAllPosts = async(schoolQuery, courseQuery, sectionQuery, postType, page, limit) => {
    const schoolReg = new RegExp(schoolQuery, 'i');

    const pipeline = [];
    switch (postType) {
        case 'reported':
            pipeline.push({
                $match: {
                    $or: [
                        { 'reports.abuse': { $not: { $size: 0 } } },
                        { 'reports.recism': { $not: { $size: 0 } } },
                        { 'reports.cheating': { $not: { $size: 0 } } },
                        { 'reports.bad language': { $not: { $size: 0 } } },
                        { 'reports.spam': { $not: { $size: 0 } } },
                        { 'reports.fruad': { $not: { $size: 0 } } },
                        { 'reports.illegal activity': { $not: { $size: 0 } } },
                        { 'reports.other': { $not: { $size: 0 } } },
                    ]
                }
            });
            break;
        case 'removed':
            pipeline.push({
                $match: {
                    isDeleted: { $eq: true }
                }
            });
            break;
    }
    pipeline.push({ $sort: { createdAt: -1 } }, {
        $lookup: {
            from: 'sections',
            let: { gId: '$groupId' },
            pipeline: [{
                    $match: {
                        'university.name': schoolReg,
                    }
                },
                {
                    $match: {
                        $expr: (() => {
                            let match = {
                                $and: [
                                    { $eq: ['$_id', '$$gId'] }
                                ]
                            };
                            if (courseQuery)
                                match['$and'].push({ $eq: ['$course.name', courseQuery] });
                            if (sectionQuery)
                                match['$and'].push({ $eq: ['$section.name', sectionQuery] });
                            return match;
                        })()
                    }
                }
            ],
            as: 'group'
        }
    }, { $unwind: '$group' }, {
        $lookup: {
            from: 'students',
            localField: 'userId',
            foreignField: '_id',
            as: 'student'
        }
    }, { $unwind: '$student' }, {
        $project: {
            content: true,
            group: true,
            isRestricted: true,
            isAnonymous: true,
            isDeleted: true,
            student: true,
            createdAt: true,
            reports: {
                'abuse': { $size: '$reports.abuse' },
                'recism': { $size: '$reports.recism' },
                'cheating': { $size: '$reports.cheating' },
                'bad language': { $size: '$reports.bad language' },
                'spam': { $size: '$reports.spam' },
                'fruad': { $size: '$reports.fruad' },
                'illegal activity': { $size: '$reports.illegal activity' },
                'other': { $size: '$reports.other' },
            },
            reportsCount: {
                $size: {
                    $setUnion: [
                        '$reports.abuse',
                        '$reports.recism',
                        '$reports.cheating',
                        '$reports.bad language',
                        '$reports.spam',
                        '$reports.fruad',
                        '$reports.illegal activity',
                        '$reports.other',
                    ]
                }
            },
            commentCount: { $size: '$comments' },
        }
    });

    const dataPagination = [];
    const skip = (page - 1) * limit;
    dataPagination.push({ $skip: skip });
    if (limit)
        dataPagination.push({ $limit: limit });
    pipeline.push({
        '$facet': {
            pagination: [{ $count: "total" }, { $addFields: { limit, page } }],
            data: dataPagination
        },
    }, {
        $unwind: '$pagination'
    });

    const result = await PostModel.aggregate(pipeline).allowDiskUse(true);

    return (result.length ?
        result
        .map(r => {
            r.data = r.data
                .sort((a, b) => b.createdAt - a.createdAt)
                .map(post => {
                    post.createdAt = moment(post.createdAt).fromNow();
                    return post;
                })
            return r;
        })[0] : {
            pagination: {
                total: 0,
                limit,
                page
            },
            data: []
        });
};

const getAllCommentsOfPost = async(postId, commentType, page, limit) => {
    let result = await PostModel.aggregate([{
            $match: {
                _id: mongoose.Types.ObjectId(postId)
            }
        },
        {
            $lookup: {
                from: 'students',
                localField: 'comments.userId',
                foreignField: '_id',
                as: 'users'
            }
        },
        {
            $project: {
                _id: false,
                comments: true,
                users: true,
            }
        },
    ]);

    const skip = (page - 1) * limit;
    const users = result[0].users;
    result = result[0].comments
        .filter(comment => {
            switch (commentType) {
                case 'reported':
                    return Object.entries(comment.reports).reduce((acc, [, value]) => acc + value.length, 0);
                case 'removed':
                    return comment.isDeleted;
                default:
                    return true;
            }
        })
        .sort((a, b) => b.createdAt - a.createdAt);

    const total = result.length;
    result = result.slice(skip, skip + limit)
        .map(comment => {
            comment.student = users.find(user => user._id.toString() === comment.userId.toString());
            comment.createdAt = moment(comment.createdAt).fromNow();
            const userReported = new Set();
            comment.reports = Object.entries(comment.reports).reduce((acc, [key, value]) => {
                acc[key] = value.length;
                value.forEach(uid => userReported.add(uid.toString()));
                return acc;
            }, {});
            comment.reportsCount = [...userReported].length;
            comment.postId = postId;
            return comment;
        });

    return {
        pagination: {
            total,
            limit,
            page
        },
        data: result
    };
};

const blockStudent = async(studentId, status) => {
    const student = await StudentModel.findById(studentId);
    if (!student)
        throwError('STUDENT_NOT_FOUND');
    if (!student.verification.isVerified && status === 'NOT_BLOCKED')
        status = 'UNVERIFIED';
    await User.permission(student, status);
};

// const removePost = async postId => {

//     try {
//         const post = await PostModel.findByIdAndUpdate(postId, {
//             isDeleted: true
//         }, { new: true });

//         return true;

//         if (!post)
//             throwError('POST_NOT_FOUND');

//     } catch (e) {
//         return false;
//         // console.log('notdone', e);
//         // return [{'error': true , 'Message':e}];
//     }
// };


const removePost = async(Postdata) => {

    // if (Postdata.value == 1) {

    //     const pin_updated = await PostModel.findByIdAndUpdate(
    //         Postdata._id, { isPin: true, isPinCreatedAt: Date.now() }
    //     );
    //     return pin_updated;

    // } else {
    //     const pin_updated = await PostModel.findByIdAndUpdate(
    //         Postdata._id, { isPin: false, isPinUpdatedAt: Date.now() }
    //     );
    //     return pin_updated;
    // }

    try {
        const post = await PostModel.findByIdAndUpdate(Postdata.postId, {
            isDeleted: true
        }, { new: true });

        return true;

        if (!post)
            throwError('POST_NOT_FOUND');

    } catch (e) {
        return false;
        // console.log('notdone', e);
        // return [{'error': true , 'Message':e}];
    }
};

const removeComment = async(postId, commentId) => {
    const post = await PostModel.findByIdAndUpdate(
        postId, {
            $set: { 'comments.$[elem].isDeleted': true }
        }, {
            arrayFilters: [{ 'elem._id': commentId }],
            new: true
        }
    );
    if (!post)
        throwError('POST_NOT_FOUND');
    const comment = post.comments.find(comment => commentId.toString() === comment._id.toString())._doc;
    if (!comment)
        throwError('COMMENT_NOT_FOUND');
};


// const save1 = GroupCategoryModel.create.bind(GroupCategoryModel);

// const addcategory = async (groupCategorydata) => {
//   const save = GroupCategoryModel.create.bind(GroupCategoryModel);

//   // console.log('add category', groupCategorydata);

//   // res.send(groupCategorydata);
//   // const add = async (groupcategorydata) => {
//   const groupcategory = await save(groupCategorydata);
//   // await groupcategory.save();

//   // const save = GroupcategoryModel.create.bind(GroupcategoryModel);
//   return groupcategory;

// };


const getAllUniversities = async() => {
    const result = await UniversityModel.find({});
    //  .then(function(data){

    //   return {data:result};
    // });
    // console.log('result:', result);
    return result;
};


// const save = GroupModel.create.bind(GroupModel);


// const addgroup = async (addgroupdata) => {

//   // console.log(addgroupdata);

//   save = GroupModel.create.bind(GroupModel);

//   let a = { grouptype: 'campusgroup', ...addgroupdata };
//   var group_add = await save(a)

//   return group_add;

// };

// const save2 = UniversityModel.create.bind(UniversityModel);

const getAllUniversitieslist = async(page, limit) => {

    const pipeline = [];
    const dataPagination = [];
    const skip = (page - 1) * limit;
    dataPagination.push({ $skip: skip });
    if (limit)
        dataPagination.push({ $limit: limit });
    pipeline.push({
        '$facet': {
            pagination: [{ $count: "total" }, { $addFields: { limit, page } }],
            data: dataPagination
        },
    }, {
        $unwind: '$pagination'
    });

    const result = await UniversityModel.aggregate(pipeline).allowDiskUse(true);
    // const result = await CampusGroupsModel.find({});
    console.log('result:', result);
    return result.length ? result[0] : {
        pagination: {
            total: 0,
            limit,
            page
        },
        data: []
    };
    //  .then(function(data){

    //   return {data:result};
    // });
    // console.log('result:', result);
    // return result;
};

const getOneUniversity = async(findUniversityData) => {

    // console.log(findsectiondata);
    const result = await UniversityModel.findOne(findUniversityData);
    return result;
};

const updateUniversity = async(updateUniversityData, id) => {

    // console.log(updateUniversityData);

    // console.log(id);

    const university_updated = await UniversityModel.findByIdAndUpdate(
        id._id,
        updateUniversityData
    );
    return university_updated;
};

const adduniversity = async(adduniversitydata) => {

    save = UniversityModel.create.bind(UniversityModel);
    try {

        const group_university = await save(adduniversitydata);
        return group_university

    } catch (e) {
        console.log('notdone', e);
        // return [{'error': true , 'Message':e}];
        // return e;
    }
};

const addsection = async(user, addsectiondata, newpath, isGeneral) => {

    try {


        if (isGeneral == true) {

            const findgeneralSection = await SectionModel.find({
                campusgroupId: mongoose.Types.ObjectId(addsectiondata.campusgroupId),
                'university.domains': addsectiondata['university.domains'],
                // 'section.name': addsectiondata['section.name'],
                isGeneral: true
            }, { _id: 1, contribs: 1 }).countDocuments();


            console.log('here count', findgeneralSection);


            if (findgeneralSection > 0) {


                console.log('check====================================', section_add)
                return section_add = null;


            } else {

                saveSection = SectionModel.create.bind(SectionModel);
                saveGroupAdmin = UserSeenGroupModel.create.bind(UserSeenGroupModel);

                console.log('yahan hai', addsectiondata);

                var coursename = addsectiondata['section.name'];
                const firstInsert = { photo: newpath, grouptype: 'campusgroup', memberCount: 1, 'course.name': coursename, ...addsectiondata };
                // if (addsectiondata !== undefined)
                var section_add = await saveSection(firstInsert);

                const getlastObjectId = await SectionModel.find().sort({ _id: -1 }).limit(1);
                // console.log('yehai',getlastObjectId[0]._id,'userid',user._id);
                const secondInsert = { userId: user._id, groupId: getlastObjectId[0]._id };
                const admin_add_to_group = await saveGroupAdmin(secondInsert)
                return section_add;
            }
        } else {

            saveSection = SectionModel.create.bind(SectionModel);
            saveGroupAdmin = UserSeenGroupModel.create.bind(UserSeenGroupModel);

            console.log('yahan hai', addsectiondata);

            var coursename = addsectiondata['section.name'];
            const firstInsert = { photo: newpath, grouptype: 'campusgroup', memberCount: 1, 'course.name': coursename, ...addsectiondata };
            // if (addsectiondata !== undefined)
            var section_add = await saveSection(firstInsert);

            const getlastObjectId = await SectionModel.find().sort({ _id: -1 }).limit(1);
            // console.log('yehai',getlastObjectId[0]._id,'userid',user._id);
            const secondInsert = { userId: user._id, groupId: getlastObjectId[0]._id };
            const admin_add_to_group = await saveGroupAdmin(secondInsert)
            return section_add;
        }

    } catch (e) {
        console.log('donenot', e);
    }

};

// const getAllSectionslist1 = async () => {
//   const result = await SectionModel.find({});
//   //  .then(function(data){

//   //   return {data:result};
//   // });
//   // console.log('result:', result);
//   return result;
// };

const getAllSectionslist = async(page, limit) => {


    const pipeline = [{
            $match: {
                //     'campusgroupId': ObjectId('5fc0ddd6ef959116f888d88f'),
                //     // 'university.name': user.university.name,
                //     'grouptype': 'campusgroup',
                //     memberCount: { $gt: 0 },
                'isDeleted': { $ne: true }
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                localField: '_id',
                foreignField: 'groupId',
                as: 'users'
            }
        },
    ];
    pipeline.push(
        // {
        //   $lookup: {
        //     from: 'posts',
        //     localField: '_id',
        //     foreignField: 'groupId',
        //     as: 'posts',
        //   }
        // },
        {
            $lookup: {
                from: "posts",
                let: { gId: "$_id" },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ["$groupId", "$$gId"] },
                                    { $ne: ["$isDeleted", true] },
                                ],
                            },
                        },
                    },
                    {
                        $project: { "_id": 1 }
                    }
                ],
                as: "posts",
            },
        }, { $sort: { createdAt: -1 } }, {
            $project: {
                university: true,
                course: true,
                section: true,
                grouptype: true,
                isDeleted: true,
                ['group.description']: true,
                ['group.name']: true,
                memberCount: { $size: '$users' },
                postCount: { $size: '$posts' },
            }
        }
    );

    const dataPagination = [];
    const skip = (page - 1) * limit;
    dataPagination.push({ $skip: skip });
    if (limit)
        dataPagination.push({ $limit: limit });
    pipeline.push({
        '$facet': {
            pagination: [{ $count: "total" }, { $addFields: { limit, page } }],
            data: dataPagination
        },
    }, {
        $unwind: '$pagination'
    });

    const result = await SectionModel.aggregate(pipeline).allowDiskUse(true);
    console.log('result:', result);
    return result.length ? result[0] : {
        pagination: {
            total: 0,
            limit,
            page
        },
        data: []
    };

};

const getOneSection = async(findsectiondata) => {

    // console.log(findsectiondata);
    const result = await SectionModel.findOne(findsectiondata);
    return result;
};

const updatesection = async(updatesectiondata, id, newpath, isGeneral) => {

    // console.log(updatesectiondata);
    // console.log(id);
    // console.log('image',newpath);

    if (isGeneral == true) {
        const findgeneralSection = await SectionModel.find({
            _id: { $ne: id._id },
            campusgroupId: mongoose.Types.ObjectId(updatesectiondata.campusgroupId),
            'university.domains': updatesectiondata['university.domains'],
            // 'section.name': addsectiondata['section.name'],
            isGeneral: true
        }, { _id: 1, contribs: 1 }).count();


        console.log('here count', findgeneralSection);

        if (findgeneralSection > 0) {

            return section_updated = null;

        } else {


            if (newpath !== '') {

                // var coursename = updatesectiondata['section.name'];

                var section_updated = await SectionModel.findByIdAndUpdate(
                    id._id, { photo: newpath, ...updatesectiondata }
                );

                return section_updated;

            } else {
                var section_updated = await SectionModel.findByIdAndUpdate(
                    id._id,
                    updatesectiondata
                );

                return section_updated;

            }

        }

    } else {

        if (newpath !== '') {

            // var coursename = updatesectiondata['section.name'];

            const section_updated = await SectionModel.findByIdAndUpdate(
                id._id, { photo: newpath, isGeneral: isGeneral, ...updatesectiondata }
            );

            return section_updated;

        } else {
            const section_updated = await SectionModel.findByIdAndUpdate(
                id._id, { isGeneral: isGeneral, ...updatesectiondata }
            );

            return section_updated;

        }

    }


};

const getAllCampusGroups = async() => {
    const result = await CampusGroupsModel.find({ 'isDeleted': { $ne: true }, }).sort({ createdAt: -1 });
    //  .then(function(data){

    //   return {data:result};
    // });
    // console.log('result:', result);
    return result;
};

const getAllGroupslist = async(page, limit) => {

    const pipeline = [{
        $match: {
            'isDeleted': { $ne: true }
        }
    }, { $sort: { createdAt: -1 } }];
    // const pipeline = [ { $sort : { createdAt: -1  } }];
    const dataPagination = [];
    const skip = (page - 1) * limit;
    dataPagination.push({ $skip: skip });
    if (limit)
        dataPagination.push({ $limit: limit });
    pipeline.push({
        '$facet': {
            pagination: [{ $count: "total" }, { $addFields: { limit, page } }],
            data: dataPagination
        },
    }, {
        $unwind: '$pagination'
    });

    const result = await CampusGroupsModel.aggregate(pipeline).allowDiskUse(true);
    // const result = await CampusGroupsModel.find({});
    console.log('result:', result);
    return result.length ? result[0] : {
        pagination: {
            total: 0,
            limit,
            page
        },
        data: []
    };
    //  .then(function(data){

    //   return {data:result};
    // });
    // console.log('result:', result);
    // return result;
};

const deleteCampusgroup = async(Sectiondata) => {

    try {

        const statusUpdated_sections = await SectionModel.updateMany({ 'campusgroupId': Sectiondata._id }, { $set: { isDeleted: true } });
        // console.log('==================================',statusUpdated_sections)
        const status_updated = await CampusGroupsModel.findByIdAndUpdate(
            Sectiondata._id, { isDeleted: true }
        );
        return status_updated;
    } catch (e) {
        return status_updated;
    }

};

const getOneGroup = async(findgroupdata) => {

    // console.log(findsectiondata);
    const result = await CampusGroupsModel.findOne(findgroupdata);
    return result;
};

const updategroup = async(updategroupdata, id, newpath, isDefault) => {
    // console.log(updategroupdata);
    // console.log(id);
    // console.log('image',newpath);
    if (newpath !== '') {

        if (isDefault == true) {
            const statusUpdated = await CampusGroupsModel.updateMany({ 'university.name': updategroupdata['university.name'] }, { $set: { isDefault: false } });
            const group_updated = await CampusGroupsModel.findByIdAndUpdate(
                id._id, { photo: newpath, ...updategroupdata }
            );
            return group_updated;

        } else {

            const group_updated = await CampusGroupsModel.findByIdAndUpdate(
                id._id, { photo: newpath, isDefault: isDefault, ...updategroupdata }
            );
            return group_updated;

        }
    } else {

        if (isDefault == true) {

            const statusUpdated = await CampusGroupsModel.updateMany({ 'university.name': updategroupdata['university.name'] }, { $set: { isDefault: false } });

            const group_updated = await CampusGroupsModel.findByIdAndUpdate(
                id._id,
                updategroupdata
            );
            return group_updated;

        } else {

            const group_updated = await CampusGroupsModel.findByIdAndUpdate(
                id._id, { isDefault: isDefault, ...updategroupdata }
            );
            return group_updated;

        }
    }
};

const addgroup = async(addgroupdata, newpath) => {

    if (addgroupdata.isDefault == 'true') {
        const statusUpdated = await CampusGroupsModel.updateMany({ 'university.name': addgroupdata['university.name'] }, { $set: { isDefault: false } });
    }
    save = CampusGroupsModel.create.bind(CampusGroupsModel);

    let a = { photo: newpath, ...addgroupdata };
    if (addgroupdata !== undefined)
        var group_add = await save(a)

    return group_add;

};

const getGroupDetaillist = async(page, limit, findGroupDetailData) => {

    const pipeline = [{
            $match: {
                'campusgroupId': ObjectId(findGroupDetailData._id),
                // 'university.name': user.university.name,
                'grouptype': 'campusgroup',
                memberCount: { $gt: 0 },
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                localField: '_id',
                foreignField: 'groupId',
                as: 'users'
            }
        },
    ];
    pipeline.push({
        $lookup: {
            from: 'posts',
            localField: '_id',
            foreignField: 'groupId',
            as: 'posts'
        }
    }, { $sort: { createdAt: -1 } }, {
        $project: {
            university: true,
            course: true,
            section: true,
            grouptype: true,
            isDeleted: true,
            ['group.description']: true,
            ['group.name']: true,
            memberCount: { $size: '$users' },
            postCount: { $size: '$posts' },
        }
    });

    const dataPagination = [];
    const skip = (page - 1) * limit;
    dataPagination.push({ $skip: skip });
    if (limit)
        dataPagination.push({ $limit: limit });
    pipeline.push({
        '$facet': {
            pagination: [{ $count: "total" }, { $addFields: { limit, page } }],
            data: dataPagination
        },
    }, {
        $unwind: '$pagination'
    });

    const result = await SectionModel.aggregate(pipeline).allowDiskUse(true);
    console.log('result:', result);
    return result.length ? result[0] : {
        pagination: {
            total: 0,
            limit,
            page
        },
        data: []
    };

};

const getAllPostlist = async(schoolQuery, courseQuery, sectionQuery, postQuery, postType, page, limit) => {
    const schoolReg = new RegExp(schoolQuery, 'i');
    const sectionReg = new RegExp(sectionQuery, 'i');
    const courseReg = new RegExp(courseQuery, 'i');
    const postReg = new RegExp(postQuery, 'i');

    console.log('para', postReg);

    const pipeline = [{
        $match: {
            'content.title': postReg,
        }

    }];
    switch (postType) {
        case 'reported':
            pipeline.push({
                $match: {
                    $or: [
                        { 'reports.abuse': { $not: { $size: 0 } } },
                        { 'reports.recism': { $not: { $size: 0 } } },
                        { 'reports.cheating': { $not: { $size: 0 } } },
                        { 'reports.bad language': { $not: { $size: 0 } } },
                        { 'reports.spam': { $not: { $size: 0 } } },
                        { 'reports.fruad': { $not: { $size: 0 } } },
                        { 'reports.illegal activity': { $not: { $size: 0 } } },
                        { 'reports.other': { $not: { $size: 0 } } },
                    ]
                }
            });
            break;
        case 'removed':
            pipeline.push({
                $match: {
                    isDeleted: { $eq: true }
                }
            });
            break;
    }
    pipeline.push({ $sort: { createdAt: -1 } }, {
        $lookup: {
            from: 'sections',
            let: { gId: '$groupId' },
            pipeline: [{
                    $match: {
                        'university.name': schoolReg,
                        'section.name': sectionReg,
                        'course.name': courseReg,
                        // 'content.title': postReg,
                    }
                },
                {
                    $match: {
                        $expr: (() => {
                            let match = {
                                $and: [
                                    { $eq: ['$_id', '$$gId'] }
                                ]
                            };
                            // if (courseQuery)
                            //     match['$and'].push({ $eq: ['$course.name', courseQuery] });
                            // if (sectionQuery)
                            //     match['$and'].push({ $eq: ['$section.name', sectionQuery] });
                            return match;
                        })()
                    }
                }
            ],
            as: 'group'
        }
    }, { $unwind: '$group' }, {
        $lookup: {
            from: 'students',
            localField: 'userId',
            foreignField: '_id',
            as: 'student'
        }
    }, { $unwind: '$student' }, {
        $project: {
            content: true,
            group: true,
            isRestricted: true,
            isAnonymous: true,
            isDeleted: true,
            student: true,
            createdAt: true,
            isPin: true,
            isNotified: true,
            groupId: true,
            reports: {
                'abuse': { $size: '$reports.abuse' },
                'recism': { $size: '$reports.recism' },
                'cheating': { $size: '$reports.cheating' },
                'bad language': { $size: '$reports.bad language' },
                'spam': { $size: '$reports.spam' },
                'fruad': { $size: '$reports.fruad' },
                'illegal activity': { $size: '$reports.illegal activity' },
                'other': { $size: '$reports.other' },
            },
            reportsCount: {
                $size: {
                    $setUnion: [
                        '$reports.abuse',
                        '$reports.recism',
                        '$reports.cheating',
                        '$reports.bad language',
                        '$reports.spam',
                        '$reports.fruad',
                        '$reports.illegal activity',
                        '$reports.other',
                    ]
                }
            },
            commentCount: { $size: '$comments' },
        }
    });

    const dataPagination = [];
    const skip = (page - 1) * limit;
    dataPagination.push({ $skip: skip });
    if (limit)
        dataPagination.push({ $limit: limit });
    pipeline.push({
        '$facet': {
            pagination: [{ $count: "total" }, { $addFields: { limit, page } }],
            data: dataPagination
        },
    }, {
        $unwind: '$pagination'
    });

    const result = await PostModel.aggregate(pipeline).allowDiskUse(true);

    return (result.length ?
        result
        .map(r => {
            r.data = r.data
                .sort((a, b) => b.createdAt - a.createdAt)
                .map(post => {
                    post.createdAt = moment(post.createdAt).fromNow();
                    return post;
                })
            return r;
        })[0] : {
            pagination: {
                total: 0,
                limit,
                page
            },
            data: []
        });
};

// const pinPost1 = async postId => {
//   const post = await PostModel.findByIdAndUpdate(postId, {
//     isDeleted: true
//   }, { new: true });
//   if (!post)
//     throwError('POST_NOT_FOUND');
// };

const pinPost = async(Postdata) => {

    if (Postdata.value == 1) {

        const pin_updated = await PostModel.findByIdAndUpdate(
            Postdata._id, { isPin: true, isPinCreatedAt: Date.now() }
        );
        return pin_updated;

    } else {
        const pin_updated = await PostModel.findByIdAndUpdate(
            Postdata._id, { isPin: false, isPinUpdatedAt: Date.now() }
        );
        return pin_updated;
    }
};

const trendingPostNotification = async(user, notifyData) => {

    if (notifyData.value == 1) {

        try {
            const findSection = await SectionModel.findOne({
                _id: mongoose.Types.ObjectId(notifyData.groupId),
            }, { _id: 1, contribs: 1, 'university.domains': 1, contribs: 1 });

            const findPostDetails = await PostModel.findOne({
                _id: mongoose.Types.ObjectId(notifyData._id),
            }, { _id: 1, contribs: 1, 'content.title': 1, contribs: 1 });

            // console.log('here',findSection.university.domains);
            // return findSection.university.domains;

            const members = await StudentModel.find({
                'university.domain': findSection.university.domains
            });

            // console.log('here',members);
            // return members

            var pin_updated = {
                sender: user._id,
                notification: {
                    title: `New Trending Post`,
                    body: findPostDetails.content.title.substr(0, 80) + '...'
                },
                metadata: {
                    groupId: notifyData.groupId,
                    postId: notifyData._id,
                }
            };

            User1.bulkPushNotification(members, pin_updated);

            const isNotifiedStatus = await PostModel.findByIdAndUpdate(
                notifyData._id, { isNotified: true }
            );
            return pin_updated;

        } catch (e) {

            // return {pin_updated: false};

            return pin_updated;

        }

    } else {
        // const pin_updated = await PostModel.findByIdAndUpdate(
        //   notifyData._id,
        //   { isPin: false , isPinUpdatedAt : Date.now() }
        // );
        return pin_updated;
    }
};


const getSettingsData = async() => {

    // console.log(findsectiondata);
    const result = await SettingModel.find();
    return result;

}

const updateSetting = async(updatesettingdata, id, addpost_isNotified, addcomment_isNotified, likepost_isNotified, unlikepost_isNotified) => {


    // console.log(updatesettingdata);
    // console.log(id);
    const setting_updated = await SettingModel.findByIdAndUpdate(
        id._id, {
            addpost_isNotified: addpost_isNotified,
            addcomment_isNotified: addcomment_isNotified,
            likepost_isNotified: likepost_isNotified,
            unlikepost_isNotified: unlikepost_isNotified,
            ...updatesettingdata
        }
    );
    return setting_updated;

}

const resetPasswordUpdated = async(data,resettokenr)=>{

    // console.log(resettokenr);
    var pass =  await Password.hash(data.confirmpassword);
    console.log('password',pass);

    try {
        var userRecord = await StudentModel.find({resettoken: resettokenr.resettoken})
        // console.log(latestRecord[0]._id);
        // return latestRecord;
        // return userRecord;
        if(userRecord == null || userRecord == undefined || userRecord == ''){

            return [{'error': true , 'message':'Password Reset Token has Expired'}];
        }
        const pass_Updated = await StudentModel.findByIdAndUpdate(
            userRecord[0]._id, { password: pass , resettoken : null}
           
        );

        return pass_Updated;

    } catch (e) {
        // console.log('notdone', e);
        return [{'error': false , 'Message':e}];
        // return e;
    }

};


module.exports = {
    // addcategory,
    get,
    getAllStudents,
    exportStudents,
    getAllSections,
    getAllPosts,
    getAllPostlist,
    pinPost,
    getAllCommentsOfPost,
    blockStudent,
    removePost,
    removeComment,
    // addgroup,
    getAllUniversitieslist,
    adduniversity,
    getAllUniversities,
    getOneUniversity,
    updateUniversity,
    addsection,
    getAllSectionslist,
    // getAllSectionslist1,
    getOneSection,
    updatesection,
    getAllCampusGroups,
    getAllGroupslist,
    getGroupDetaillist,
    deleteCampusgroup,
    getOneGroup,
    updategroup,
    addgroup,
    trendingPostNotification,
    getSettingsData,
    updateSetting,

    resetPasswordUpdated,

    // save2,
    // save1,
    save,
    saveSection,
    saveGroupAdmin,
};