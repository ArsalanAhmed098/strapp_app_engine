const fs = require('fs').promises;
const path = require('path');
const User = require('./User');
const Notification = require('./Notification');
const uuid4 = require('uuid/v4');
const {
    sendMail,
    sendForgotPasswordMail,
    genExpire,
    notificationHandler,
    Password,
    verifyByUniversityV3,
} = require('../../util');
const { StudentModel, BlockStudentModel, UserLogsModel } = require('../../models/v2');
const mongoose = require('mongoose');
var save;

const get = (user, currAuthToken) => {
    user.password = undefined;
    user.confirmation = undefined;
    user.notification = undefined;
    user.block = undefined;
    user.isDeleted = undefined;
    user.university = undefined;
    user.tokens = undefined;
    user.reports = undefined;
    // user._doc.blockedUsers = [...new Set(
    //   [
    //     ...user.regularBlockedByMe.map(id => id.toString()),
    //     ...user.anonymousBlockedByMe.map(id => id.toString())
    //   ]
    // )];
    // user.regularBlockedByMe = undefined;
    // user.anonymousBlockedByMe = undefined;
    user._doc.token = currAuthToken;
    return user;
};

const getUserById = async userId => {
    let user = await StudentModel.findById(userId);
    if (!user)
        throwError('STUDENT_NOT_FOUND');
    user.token = undefined;
    user.createdAt = undefined;
    user.updatedAt = undefined;
    user._doc.userId = user._id;
    delete user._doc._id;
    user = get(user);
    user._doc.blockedUsers = undefined;
    return user;
};

const updatePhoto = async(user, photo) => {
    // if (!file)
    //   throwError('PHOTO_REQUIRED');
    // try {
    //   const photoPath = path.join(require.main.filename, '../../', user.photo);
    //   await fs.unlink(photoPath);
    // } catch (err) { console.log('unlink err:', err) }
    // user.photo = `/${file.destination}/${file.filename}`;

    user.photo = photo;
    await user.save();
    return user.photo;
};

const updateProfile = async(user, major, gradelevel, photo) => {
    // if (file)
    //   user.photo = `/${file.destination}/${file.filename}`;
    // console.log('major:', major);
    user.major = major;
    user.gradelevel = gradelevel;
    user.photo = photo;
    console.log('update user:', user);
    return get(await user.save());
};

// const updateProfile = async (user, file, major, gradelevel) => {
//   if (file)
//     user.photo = `/${file.destination}/${file.filename}`;
//   console.log('major:', major);
//   user.major = major;
//   user.gradelevel = gradelevel;
//   console.log('update user:', user);
//   return get(await user.save());
// };

const updatePassword = async(user, oldPassword, newPassword, confirmNewPassword) => {
    if (newPassword !== confirmNewPassword)
        throwError('NEW_PASSWORDS_NOT_MATCH');

    const comparedResult = await Password.compare(oldPassword, user.password);
    if (!comparedResult)
        throwError('OLD_PASSWORD_INCORRECT');

    user.password = await Password.hash(newPassword);
    await user.save();
};

// const updateInfo = async (user, firstName, lastName, major) => {
//   firstName && (user.firstName = firstName);
//   lastName && (user.lastName = lastName);
//   major && (user.major = major);

//   const newInfo = await user.save();
//   const returnedData = {};

//   firstName && (returnedData.firstName = newInfo.firstName);
//   lastName && (returnedData.lastName = newInfo.lastName);
//   major && (returnedData.major = newInfo.major);

//   return returnedData;
// };

const updateInfo = async(user, gradelevel, major) => {
    gradelevel && (user.gradelevel = gradelevel);
    major && (user.major = major);

    const newInfo = await user.save();
    const returnedData = {};

    gradelevel && (returnedData.gradelevel = newInfo.gradelevel);
    major && (returnedData.major = newInfo.major);

    return returnedData;
};

const sendVerification = async(user, hostURL) => {
    if (user.verification.isVerified)
        throwError('USER_CONFIRMED');
    const confirmCode = user.confirmation.code || uuid4();
    const link = `${hostURL}/api/v4/user/verify/${confirmCode}`;
    user.confirmation = {
        code: confirmCode,
        expire: genExpire(5)
    };
    user = await user.save();
    const verifyMail = await sendMail({
        // firstName: user.firstName,
        email: user.email,
        link
    });
    console.log('verifyMail state:', verifyMail);
    return verifyMail;
};

const verifyUserByEmail = async(email, status) => {
    const user = await StudentModel.findOne({ email });
    if (!user)
        throwError('STUDENT_NOT_FOUND');
    const isVerified = status === 'UNVERIFIED_ID' ? false : true;
    user.verification.isVerified = isVerified;
    user.verification.status = status;
    if (isVerified) {
        user.block.status = 'NOT_BLOCKED';
        user.block.deniedRoutes = [];
    } else {
        user.block.status = 'UNVERIFIED';
        user.block.deniedRoutes = [{
                path: 'user/*',
                method: 'PUT'
            },
            {
                path: 'user/notifications',
                method: '*'
            },
            {
                path: 'user/notifications/*',
                method: '*'
            },
            {
                path: 'courses',
                method: '*'
            },
            {
                path: 'sections',
                method: '*'
            },
            {
                path: 'posts',
                method: '*'
            },
            {
                path: 'feed',
                method: 'GET'
            },
        ];
    }
    await user.save();
    return get(user);
};

const verifyUser = async verifyCode => {
    let user = await StudentModel.findOne({ 'confirmation.code': verifyCode });
    try {
        if (!user)
            throwError('STUDENT_NOT_FOUND');

        if (!user.verification.isVerified) {
            if (user.confirmation.code !== verifyCode || user.confirmation.expire < Date.now())
                throwError('CODE_INCORRECT');

            const verifyResult = await verifyByUniversityV3(user);

            user.confirmation = undefined;
            user.verification.isVerified = true;
            user.verification.status = verifyResult.status;
            user.firstName = verifyResult.firstName;
            user.lastName = verifyResult.lastName;
            user.username = verifyResult.username;
            if (user.block.status === 'UNVERIFIED') {
                user.block.status = 'NOT_BLOCKED';
                user.block.deniedRoutes = [];
            }
        }
    } catch (err) {
        console.log('student verification error:', err);
    } finally {
        return user && get(await user.save());
    }
};

const report = async(user, reportUserId, reasons) => {
    if (user._id.toString() === reportUserId)
        throwError('STUDENT_NOT_FOUND');

    const student = await StudentModel.findById(reportUserId);
    if (!student)
        throwError('STUDENT_NOT_FOUND');

    reasons.forEach(reason => {
        if (!student.reports[reason])
            reason = 'other';
        if (!student.reports[reason].find(userId => userId.toString() === user._id.toString()))
            student.reports[reason].push(user._id);
    });

    await student.save();
};

const block = async(user, blockUserId, isAnonymousPost) => {
    const blockUser = await StudentModel.findById(blockUserId);
    if (user._id.toString() === blockUserId || !blockUser)
        throwError('STUDENT_NOT_FOUND');

    const blockRecord = await BlockStudentModel.findOne({
        blockerUser: user._id,
        blockedUser: mongoose.Types.ObjectId(blockUserId),
        isAnonymousPost,
    });

    if (!blockRecord) {
        await BlockStudentModel.create({
            blockerUser: user._id,
            blockedUser: mongoose.Types.ObjectId(blockUserId),
            isAnonymousPost,
        });
    }
};

const unblock = async(user, blockUserId) => {
    if (user._id.toString() === blockUserId)
        throwError('STUDENT_NOT_FOUND');
    for (const i in user.anonymousBlockedByMe) {
        console.log('user.anonymousBlockedByMe[i]:', i, user.anonymousBlockedByMe[i]);
        if (user.anonymousBlockedByMe[i].toString() === blockUserId) {
            user.anonymousBlockedByMe.splice(i, 1);
        }
    }
    for (const i in user.regularBlockedByMe) {
        console.log('user.regularBlockedByMe[i]:', i, user.regularBlockedByMe[i]);
        if (user.regularBlockedByMe[i].toString() === blockUserId) {
            user.regularBlockedByMe.splice(i, 1);
        }
    }
    await user.save();
};

const registerfirebaseToken = async(user, firebaseToken) => {
    for (const token of user.tokens)
        if (token.authToken === user.currAuthToken)
            token.firebaseToken = firebaseToken;
    await user.save();
};

const getfirebaseTokens = async user => {
    return user.tokens.reduce((acc, { firebaseToken }) => {
        if (firebaseToken) acc.push(firebaseToken);
        return acc;
    }, []);
};

const updateNotificationSeen = async user => {
    user.notification.lastSeen = Date.now();
    await user.save();
};

const updateNotificationState = async(user, state) => {
    user.notification.state = state;
    await user.save();
};

const pushNotification = async(user, payload, options) => {
    const { sender, metadata } = payload;
    payload.data = {
        body: metadata.postId || metadata.groupId
            // sender ? (sender.photo || '') : ''
    };
    delete payload.sender;
    delete payload.metadata;
    const notification = {
        receiver: user._id,
        sender: sender ? sender._id : null,
        metadata,
        payload: JSON.stringify(payload),
        firebaseResponses: [],
    };
    try {
        const firebaseTokens = user.tokens.reduce((acc, { firebaseToken }) => {
            firebaseToken && acc.push(firebaseToken);
            // console.log(firebaseTokens);
            return acc;
        }, []);

        const tochange = firebaseTokens;
        const unique = [...new Set(tochange)];
        // console.log('dublicate-tokens', firebaseTokens);
        // console.log('unique-tokens', unique);
        if (user.notification && user.notification.state && unique.length) {
            for (const token of unique) {
                const firebaseState = { token };
                const response = await notificationHandler(token)(payload, options);
                if (response.successCount) {
                    firebaseState.status = true;
                    firebaseState.messageId = response.results[0].messageId;
                } else {
                    firebaseState.status = false;
                    firebaseState.error = response.results[0].error;
                }
                firebaseState.multicastId = response.multicastId;
                notification.firebaseResponses.push(firebaseState);
            }
        }
    } catch (err) {
        notification.firebaseResponses.push({
            status: false,
            error: err,
        });
    }
    await Notification.save(notification);

    console.log('Push notification to:', user.email);
    console.log('Push notification status:', notification.status);

    return notification.status;
};

const bulkPushNotification = async(users, payload) => {
    for (const user of users) {
        const notificationPayload = JSON.parse(JSON.stringify(payload));
        // console.log('======================payload:', notificationPayload);
        await pushNotification(user, notificationPayload);
    }
}

const hasAccess = (user, { path, method }) =>
    !(user.block.deniedRoutes && user.block.deniedRoutes.find(route => {
        if (route.method !== '*' && route.method !== method) return false;
        const path1 = route.path.split('/');
        const path2 = path.split('/');
        if (path1.length > path2.length) return false;
        for (let i = 0; i < path1.length; i++)
            if (path1[i] !== '*' && path1[i] !== path2[i]) return false;
        return true;
    })) || throwError(user.block.status === 'UNVERIFIED' ? 'UNVERIFIED' : 'FORBIDDEN');

const permission = async(user, status) => userBlock[status] && userBlock[status](user);

const userBlock = {
    ['NOT_BLOCKED'](user) {
        user.block.status = 'NOT_BLOCKED';
        user.block.deniedRoutes = [];
        return user.save();
    },
    ['WRITE_BLOCKED'](user) {
        user.block.status = 'WRITE_BLOCKED';
        user.block.deniedRoutes = [{
                path: 'sections/*/posts',
                method: 'POST'
            },
            {
                path: 'posts/*/comments',
                method: 'POST'
            }
        ];
        return user.save();
    },
    ['TOTAL_BLOCKED'](user) {
        user.block.status = 'TOTAL_BLOCKED';
        user.block.deniedRoutes = [{
            path: '*',
            method: '*'
        }];
        return user.save();
    },
    ['UNVERIFIED'](user) {
        user.block.status = 'UNVERIFIED';
        user.block.deniedRoutes = [{
                path: 'user/*',
                method: 'PUT'
            },
            {
                path: 'user/notifications',
                method: '*'
            },
            {
                path: 'user/notifications/*',
                method: '*'
            },
            {
                path: 'courses',
                method: '*'
            },
            {
                path: 'sections',
                method: '*'
            },
            {
                path: 'posts',
                method: '*'
            },
            {
                path: 'feed',
                method: 'GET'
            },
        ];
        return user.save();
    },
};

const updateStudentInfo = async(user, gradelevel, major) => {
    const studentUpdated = await StudentModel.findByIdAndUpdate(
        user._id, { gradelevel: gradelevel, major: major }
    );
    return true;
};

const updateStudentVerification = async(userId) => {
    const studentUpdated = await StudentModel.findByIdAndUpdate(
        userId, { 'verification.isVerified': true, 'verification.status': 'VERIFIED_STUDENT', 'block.status': 'NOT_BLOCKED' }
    );
    return true;
};

const updateStudentVerificationEmail = async(email) => {
    const studentUpdated = await StudentModel.findOneAndUpdate({ email: email }, { $set: { 'verification.isVerified': true, 'verification.status': 'VERIFIED_STUDENT', 'block.status': 'NOT_BLOCKED' } });
    return true;
};

const deleteStudent = async(userId) => {
    var delete_Student = await StudentModel.deleteOne({
        _id: userId,
    });
    return true;
};

const userLogsAdd = async(user,datatoadd) => {

    // console.log('userId',user._id);
    // console.log('data',datatoadd);
    save = UserLogsModel.create.bind(UserLogsModel);
    try {

        const userlogadded = await save({starttime:Date.now(),...datatoadd});
        return userlogadded;

    } catch (e) {
        // console.log('notdone', e);
        return [{'error': false , 'Message':e}];
        // return e;
    }

};

const userLogsUpdate = async(user) => {

    console.log('userId',user._id);
    try {

        const latestRecord = await UserLogsModel.find({userId: user._id}).sort({'createdAt':-1}).limit(1)
        // console.log(latestRecord[0]._id);
        // return latestRecord;
        const logUpdated = await UserLogsModel.findByIdAndUpdate(
            latestRecord[0]._id, { endtime: Date.now() }
        );
        const responsedata = await UserLogsModel.find({userId: user._id}).sort({'createdAt':-1}).limit(1)
        return responsedata;

    } catch (e) {
        // console.log('notdone', e);
        return [{'error': false , 'Message':e}];
        // return e;
    }
};

const forgotPassword = async(datatoadd,hostURL) => {

    try{
    console.log('email: ',hostURL,datatoadd);
    const confirmCode = uuid4();

    const userRecord   = await StudentModel.find({email: datatoadd.email});
    const tokenUpdated = await StudentModel.findByIdAndUpdate(
        userRecord[0]._id, { resettoken: confirmCode }
    );

    // const link = `${hostURL}/api/v4/forgot/password/${datatoadd.email}/${confirmCode}`;
    // const link = `${hostURL}/api/v4/forgot/password/check`;
    const link = `${hostURL}/admins/reset/password/${confirmCode}`;
    const verifyMail = await sendForgotPasswordMail({
      // firstName: userData.firstName,
      email: datatoadd.email,
      link
    });
    console.log('verifyMail state:', verifyMail);

    return {'success': true , 'message':'kindly check your email'};
    
        } catch (e) {
        // console.log('notdone', e);
    return {'error': false ,  'message':'something went wrong!'};
        // return e;
    }

};

const chattNotify = async (chattdata) =>{

    // console.log(chattdata);
    var user_receiver = await StudentModel.find({_id: mongoose.Types.ObjectId(chattdata.receiverId)});
    // console.log('result', user_receiver[0]);
        var notificationPayload = {
        sender: chattdata.senderId,
        notification: {
            title: `New Message`,
            // body: `A ${chattdata.title} message you "${chattdata.msg.substr(0,50)+'...'}"`,
            body: `${chattdata.msg.substr(0,50)+'...'}`,
        },
        metadata: {
            chatId:     chattdata.chatId,
            receiverId: chattdata.receiverId,
            senderId:   chattdata.senderId,
        }
    };
        if (user_receiver != null || user_receiver != undefined ) {
            await chattPushNotification(user_receiver[0], notificationPayload);
    }
}

const chattPushNotification = async(user, payload, options) => {
    const { sender, metadata } = payload;
    payload.data = {
        body: metadata.chatId,
        type: 'chat'
    };
    delete payload.sender;
    delete payload.metadata;
    const notification = {
        receiver: user._id,
        sender: sender ? sender : null,
        metadata,
        payload: JSON.stringify(payload),
        firebaseResponses: [],
    };
    try {
        const firebaseTokens = user.tokens.reduce((acc, { firebaseToken }) => {
            firebaseToken && acc.push(firebaseToken);
            return acc;
        }, []);

        const tochange = firebaseTokens;
        const unique = [...new Set(tochange)];
        // console.log('dublicate-tokens', firebaseTokens);
        // console.log('unique-tokens', unique);
        if (user.notification && user.notification.state && unique.length) {
            for (const token of unique) {
                const firebaseState = { token };
                const response = await notificationHandler(token)(payload, options);
                if (response.successCount) {
                    firebaseState.status = true;
                    firebaseState.messageId = response.results[0].messageId;
                } else {
                    firebaseState.status = false;
                    firebaseState.error = response.results[0].error;
                }
                firebaseState.multicastId = response.multicastId;
                notification.firebaseResponses.push(firebaseState);
            }
        }
    } catch (err) {
        notification.firebaseResponses.push({
            status: false,
            error: err,
        });
    }
    console.log('Push notification to:', user.email);
    console.log('Push notification status:', notification.status);

    return notification.status;
};

const project = () => ({
    userId: '$_id',
    _id: false,
    firstName: true,
    lastName: true,
    photo: true,
    major: true,
    gradelevel: true,
    email: true,
    username: true,
    verification: true,
});

module.exports = {
    get,
    getUserById,

    updatePhoto,
    updatePassword,
    updateInfo,
    updateProfile,

    sendVerification,
    verifyUserByEmail,
    verifyUser,

    report,
    block,
    unblock,

    registerfirebaseToken,
    getfirebaseTokens,
    updateNotificationSeen,
    updateNotificationState,
    pushNotification,
    bulkPushNotification,

    hasAccess,
    permission,
    project,
    updateStudentInfo,
    updateStudentVerification,
    updateStudentVerificationEmail,
    deleteStudent,
    userLogsAdd,
    userLogsUpdate,

    forgotPassword,

    chattNotify,
    chattPushNotification,
    save,
};