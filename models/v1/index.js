const UserModel = require('./user');
const GroupModel = require('./group');
const CommonModels = require('../common');

module.exports = {
  UserModel,
  GroupModel,
  ...CommonModels,
};
