const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { PostController, CommentController } = require('../../../controllers/v3');
const { userAccessV3, upload } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);

router.route('/:postId')
  .get(PostController.get)
  .put(PostController.update)
  .delete(PostController.remove);

router.route('/:postId/report')
  .patch(PostController.report);

router.route('/:postId/like')
  .patch(PostController.like)
  .delete(PostController.unlike);

router.route('/:postId/dislike')
  .patch(PostController.dislike)
  .delete(PostController.undislike);

router.route('/:postId/comments')
  .post(
    upload().array('photos'),
    CommentController.add
  )
  .get(CommentController.getAll);

router.route('/:postId/comments/:commentId')
  .delete(CommentController.remove);

router.route('/:postId/comments/:commentId/report')
  .patch(CommentController.report);

router.route('/:postId/comments/:commentId/like')
  .patch(CommentController.like)
  .delete(CommentController.unlike);

router.route('/:postId/comments/:commentId/dislike')
  .patch(CommentController.dislike)
  .delete(CommentController.undislike);

module.exports = router;
