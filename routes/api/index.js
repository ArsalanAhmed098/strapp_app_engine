const router  = require('express').Router();
const v1      = require('./v1');
const v2      = require('./v2');
const v3      = require('./v3');
const v4      = require('./v4');

router.use('/v1', v1);
router.use('/v2', v2);
router.use('/v3', v3);
router.use('/v4', v4);

module.exports = router;
