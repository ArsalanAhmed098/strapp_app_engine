const mongoose = require('mongoose');

const NotificationSchema = new mongoose.Schema({
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  sender: {
    type: mongoose.Schema.Types.ObjectId,
  },
  metadata: {
    groupId: mongoose.Schema.Types.ObjectId,
    postId: mongoose.Schema.Types.ObjectId,
    commentId: mongoose.Schema.Types.ObjectId,
  },
  payload: {
    type: String,
  },
  firebaseResponses: [{
    status: {
      type: Boolean,
    },
    messageId: {
      type: String,
    },
    error: {
      type: String,
    },
    multicastId: {
      type: String,
    },
    token: String
  }]
}, {
  timestamps: true
});

module.exports = mongoose.model('Notification', NotificationSchema);
