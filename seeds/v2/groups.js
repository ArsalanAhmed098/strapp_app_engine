const { genRandom } = require('../../util');
const { StudentModel } = require('../../models/v2');
const { Course, Section } = require('../../services/v2');

const getCourseName = () => ['EECS101', 'MATH101', 'ANTHRO101'].slice(0, genRandom(1, 4));
const getNumber = () => ['006', 'EX001', '007', '5', '123', 'M100'][genRandom(0, 6)];
const getCategory = () => ['none', 'lecture', 'discussion', 'lab', 'tutorial', 'practical', 'seminar'][genRandom(0, 7)];
const getSections = () => [
  {
    name: 'Marilyn Monroe',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Abraham Lincoln',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Nelson Mandela',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'John F. Kennedy',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Martin Luther King',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Queen Elizabeth II',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Winston Churchill',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Donald Trump',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Bill Gates',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Muhammad Ali',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Mahatma Gandhi',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Mother Teresa',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Christopher Columbus',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Charles Darwin',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Elvis Presley',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Albert Einstein',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Paul McCartney',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Queen Victoria',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Pope Francis',
    number: getNumber(),
    category: getCategory(),
  },
  {
    name: 'Jawaharlal Nehru',
    number: getNumber(),
    category: getCategory(),
  },
].slice(0, genRandom(1, 6));

const insertGroups = async () => {
  const users = await StudentModel.find();
  for (let index = 0; index < users.length; index++) {
    const user = users[index];
    if (index % 6) {
      const courseNames = getCourseName();
      for (let courseName of courseNames) {
        const { course } = await Course.add(user, courseName);
        const sections = getSections();
        const existingSections = await Section.getAllByCourse(user, course.name);
        for (let section of sections) {
          const exSection = existingSections.find(exSection => exSection.section.name.toLowerCase() === section.name.toLowerCase());
          if (exSection) {
            await Section.join(user, exSection.groupId);
          } else {
            const sec = await Section.add(user, course.name, section.name, section.number, section.category)
          }
        }
      }
    }
  }
};

module.exports = insertGroups;
