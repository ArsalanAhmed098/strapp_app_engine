const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { MajorGradeLevelController } = require('../../../controllers/v4');
const { userAccessV3 } = require('../../../middleware');

// router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);


router.route('/list')
  .get(MajorGradeLevelController.getAllMajorAndGradeLevel);

  

module.exports = router;
