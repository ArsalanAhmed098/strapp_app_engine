const { Section } = require('../../services/v4');

const add = async (req, res) => {
  const { courseName } = req.params;
  const {
    name: sectionName,
    number: sectionNumber,
    category: sectionCategory
  } = req.body;
  const section = await Section.add(req.user, courseName, sectionName, sectionNumber, sectionCategory);
  res.success(section);
};

const getAllByCourse = async (req, res) => {
  const { courseName } = req.params;
  const sections = await Section.getAllByCourse(req.user, courseName);
  res.success(sections);
};

const getAll = async (req, res) => {
  const sections = await Section.getAll(req.user);
  res.success(sections);
};

const getAllCampusSections = async (req, res) => {
  var fullUrl = req.protocol + '://' + req.get('host') + '/';
  const campussections = await Section.getAllCampusSections(req.user,fullUrl);
  res.success(campussections);
};

const getAllSectionsByExplore = async (req, res) => {

  var fullUrl = req.protocol + '://' + req.get('host') + '/';

  console.log('hello',fullUrl);
  const { exploreName } = req.params;
  const campusExploreSections = await Section.getAllSectionsByExplore(req.user,fullUrl,exploreName);
  res.success(campusExploreSections);
};


const getAllSectionsByGroup = async (req, res) => {

  var fullUrl = req.protocol + '://' + req.get('host') + '/';

  console.log('hello',fullUrl);
  const { groupId } = req.params;
  const campusGroupSections = await Section.getAllSectionsByGroup(req.user,fullUrl,groupId);
  res.success(campusGroupSections);
};

// const getAllCampusExplorelist = async (req, res) => {
//   var fullUrl = req.protocol + '://' + req.get('host') + '/';
//   const campusExplorelisting = await Section.getAllCampusExplore(req.user,fullUrl);
//   res.success(campusExplorelisting);
// };



const get = async (req, res) => {
  const section = await Section.get(req.user, req.params.sectionId);
  res.success(section);
};

const join = async (req, res) => {
  const section = await Section.join(req.user, req.params.sectionId);
  res.success(section);
};

const leave = async (req, res) => {
  await Section.leave(req.user, req.params.sectionId);
  res.success();
};

const joinCampusGroup = async (req, res) => {
  const section = await Section.joinCampusGroup(req.user, req.params.sectionId,req.params.campusgroupId);
  res.success(section);
};

const leaveCampusGroup = async (req, res) => {
  await Section.leaveCampusGroup(req.user, req.params.sectionId);
  res.success();
};

const joinCampusGroupOnBoarding = async (req, res) => {
  const section = await Section.joinCampusGroupOnBoarding(req.user, req.params.campusGroupId);
  res.success(section);
};

const leaveCampusGroupOnBoarding = async (req, res) => {
  const section = await Section.leaveCampusGroupOnBoarding(req.user, req.params.campusGroupId);
  res.success(section);
};

const joinDefaultCampusGroupOnBoarding = async (req, res) => {
  const {email } = req.body;
  const section = await Section.joinDefaultCampusGroupOnBoarding(req.user, req.params.userId,email);
  res.success(section);
};

const updateSeen = async (req, res) => {
  await Section.updateSeen(req.user, req.params.sectionId);
  res.success();
};

const getAllMembers = async (req, res) => {
  const users = await Section.getAllMembers(req.params.sectionId);
  res.success(users);
};

const getalljoinedgroups = async (req, res) => {
  const posts = await Section.getalljoinedgroups(req.user);
  res.success(posts);
};

module.exports = {
  add,
  getAllByCourse,
  getAll,
  get,
  join,
  leave,
  joinCampusGroupOnBoarding,leaveCampusGroupOnBoarding,joinDefaultCampusGroupOnBoarding,joinCampusGroup,leaveCampusGroup,
  updateSeen,
  getAllMembers,
  getAllCampusSections,
  getAllSectionsByExplore,
  // getAllCampusExplorelist,
  getAllSectionsByGroup,
  getalljoinedgroups
};
