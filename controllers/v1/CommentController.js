const mongoose = require('mongoose');
const { PostModel, UserSeenGroupModel, UserModel } = require('../../models/v1');

const createComment = async (req, res) => {
  const group = await UserSeenGroupModel.findOne({
    userId: req.user._id,
    groupId: req.body.groupId
  });
  if (!group) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'group not fount',
      errorCode: 'GROUP_NOT_FOUND',
    });
  }
  const post = await PostModel.findOneAndUpdate({
    _id: req.body.postId,
    groupId: req.body.groupId,
  }, {
    $push: {
      comments: {
        parentId: req.body.parentId,
        userId: req.user._id,
        description: req.body.description,
        isAnonymous: req.body.isAnonymous,
      },
    },
  }, { new: true });

  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }

  const comment = post.comments[post.comments.length - 1];
  if (req.body.parentId) {
    await PostModel.findByIdAndUpdate(
      post._id,
      {
        $addToSet: { 'comments.$[elem].children': comment._id }
      },
      {
        arrayFilters: [{ 'elem._id': req.body.parentId }],
        new: true
      }
    );
  }

  let userId = null;
  const notificationPayload = {
    // data: {
      // body: req.user.photo || ''
    // },
    sender: req.user,
    notification: {
      body: comment.description
    }
  };
  if (comment.parentId) {
    userId = post.comments.find(cmt => cmt._id.toString() === comment.parentId.toString()).userId;
    notificationPayload.notification.title = 'There is a new reply';
  } else {
    userId = post.userId;
    notificationPayload.notification.title = 'There is a new comment';
  }

  if (userId.toString() !== req.user._id.toString()) {
    const user = await UserModel.findById(userId);
    user.pushNotification(notificationPayload);
  }

  comment._doc.currentUserLiked =
    (comment._doc.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (comment._doc.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  comment._doc.likes = comment._doc.likes.length - comment._doc.dislikes.length;
  delete comment._doc.dislikes;
  comment._doc.commentId = comment._doc._id;
  delete comment._doc._id;

  res.status(200).json({
    success: true,
    data: comment,
    message: 'comment created successfully',
    errorCode: null,
  });
};

const reportComment = async (req, res) => {
  const { reasons, postId } = req.body;
  const post = await PostModel.findById(postId);
  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }

  let commentFound = false;
  post.comments.forEach(comment => {
    if (comment._id.toString() === req.params.commentId.toString()) {
      commentFound = true;
      reasons.forEach(reason => {
        if (!comment.reports[reason])
          reason = 'other';
        if (!comment.reports[reason].find(userId => userId.toString() === req.user._id.toString()))
          comment.reports[reason].push(req.user._id);
      });
    }
  });

  if (commentFound) {
    await post.save();
    console.log('comment reported:', post);
  } else {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'comment not fount',
      errorCode: 'COMMENT_NOT_FOUND',
    });
  }

  res.status(200).json({
    success: true,
    data: null,
    message: 'Comment reported successfully',
    errorCode: null,
  });
};

const removeComment = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(
    req.body.postId,
    {
      $set: { 'comments.$[elem].isDeleted': true }
    },
    {
      arrayFilters: [{ 'elem._id': req.params.commentId }],
      new: true
    }
  );

  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }

  const comment = post.comments.find(comment => req.params.commentId.toString() === comment._id.toString())._doc;
  if (!comment) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'comment not fount',
      errorCode: 'COMMENT_NOT_FOUND',
    });
  }

  res.status(200).json({
    success: true,
    data: null,
    message: 'Comment removed successfully',
    errorCode: null,
  });
};

const likeComment = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(
    req.body.postId,
    {
      $pull: { 'comments.$[elem].dislikes': req.user._id },
      $addToSet: { 'comments.$[elem].likes': req.user._id }
    },
    {
      arrayFilters: [{ 'elem._id': req.params.commentId }],
      new: true
    }
  );

  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }

  const comment = post.comments.find(comment => req.params.commentId.toString() === comment._id.toString())._doc;
  if (!comment) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'comment not fount',
      errorCode: 'COMMENT_NOT_FOUND',
    });
  }

  comment.commentId = comment._id;
  comment.currentUserLiked =
    (comment.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (comment.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  comment.likes = comment.likes.length - comment.dislikes.length;
  delete comment.dislikes;
  delete comment._id;
  delete comment.isAnonymous;
  delete comment.userId;
  delete comment.description;
  delete comment.updatedAt;
  delete comment.createdAt;

  res.status(200).json({
    success: true,
    data: comment,
    message: 'comment liked successfully',
    errorCode: null,
  });
};

const removeLikeComment = async (req, res) => {
  const postId = req.query.post;
  const post = await PostModel.findByIdAndUpdate(
    postId,
    {
      $pull: { 'comments.$[elem].likes': req.user._id },
    },
    {
      arrayFilters: [{ 'elem._id': req.params.commentId }],
      new: true
    }
  );

  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }

  const comment = post.comments.find(comment => req.params.commentId.toString() === comment._id.toString())._doc;
  if (!comment) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'comment not fount',
      errorCode: 'COMMENT_NOT_FOUND',
    });
  }

  comment.commentId = comment._id;
  comment.currentUserLiked =
    (comment.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (comment.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  comment.likes = comment.likes.length - comment.dislikes.length;
  delete comment.dislikes;
  delete comment._id;
  delete comment.isAnonymous;
  delete comment.userId;
  delete comment.description;
  delete comment.updatedAt;
  delete comment.createdAt;

  res.status(200).json({
    success: true,
    data: comment,
    message: 'comment liked successfully',
    errorCode: null,
  });
};

const dislikeComment = async (req, res) => {
  const post = await PostModel.findByIdAndUpdate(
    req.body.postId,
    {
      $pull: { 'comments.$[elem].likes': req.user._id },
      $addToSet: { 'comments.$[elem].dislikes': req.user._id }
    },
    {
      arrayFilters: [{ 'elem._id': req.params.commentId }],
      new: true
    }
  );

  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }

  const comment = post.comments.find(comment => req.params.commentId.toString() === comment._id.toString())._doc;
  if (!comment) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'comment not fount',
      errorCode: 'COMMENT_NOT_FOUND',
    });
  }

  comment.commentId = comment._id;
  comment.currentUserLiked =
    (comment.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (comment.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  comment.likes = comment.likes.length - comment.dislikes.length;
  delete comment.dislikes;
  delete comment._id;
  delete comment.isAnonymous;
  delete comment.userId;
  delete comment.description;
  delete comment.updatedAt;
  delete comment.createdAt;

  res.status(200).json({
    success: true,
    data: comment,
    message: 'comment disliked successfully',
    errorCode: null,
  });
};

const removeDislikeComment = async (req, res) => {
  const postId = req.query.post;
  const post = await PostModel.findByIdAndUpdate(
    postId,
    {
      $pull: { 'comments.$[elem].dislikes': req.user._id },
    },
    {
      arrayFilters: [{ 'elem._id': req.params.commentId }],
      new: true
    }
  );

  if (!post) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'post not fount',
      errorCode: 'POST_NOT_FOUND',
    });
  }

  const comment = post.comments.find(comment => req.params.commentId.toString() === comment._id.toString())._doc;
  if (!comment) {
    return res.status(404).json({
      success: false,
      data: null,
      message: 'comment not fount',
      errorCode: 'COMMENT_NOT_FOUND',
    });
  }

  comment.commentId = comment._id;
  comment.currentUserLiked =
    (comment.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
    (comment.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
    0;
  comment.likes = comment.likes.length - comment.dislikes.length;
  delete comment.dislikes;
  delete comment._id;
  delete comment.isAnonymous;
  delete comment.userId;
  delete comment.description;
  delete comment.updatedAt;
  delete comment.createdAt;

  res.status(200).json({
    success: true,
    data: comment,
    message: 'comment disliked successfully',
    errorCode: null,
  });
};

module.exports = {
  createComment,
  reportComment,
  removeComment,
  likeComment,
  removeLikeComment,
  dislikeComment,
  removeDislikeComment,
};
