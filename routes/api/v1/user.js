const router = require('express').Router();
const passport = require('../../../conf/v1/auth');
const { UserController } = require('../../../controllers/v1');
const GenericController = require('../../../controllers/generic');
const { upload, userAccess } = require('../../../util');

router.post('/register', UserController.register);
router.post('/login', (req, res, next) => {
  req.body.username = req.body.username || '\0';
  req.body.email = req.body.email || '\0';
  next();
}, passport.authenticate('user-local', { session: false }), userAccess, GenericController.login);
router.post('/logout', passport.authenticate('user-bearer', { session: false }), GenericController.logout);

router.get('/me', passport.authenticate('user-bearer', { session: false }), userAccess, GenericController.currentUser);
router.post('/token', UserController.tokenValidation);
router.post('/unique/username', UserController.checkUniqueness);
router.post('/unique/email', UserController.checkUniqueness);

router.put('/uploads',
  passport.authenticate('user-bearer', { session: false }),
  (req, res, next) => upload('profiles').single('photo')(req, res, next),
  userAccess,
  UserController.uploadUserPic
);
router.put('/change/password', passport.authenticate('user-bearer', { session: false }), userAccess, UserController.changePassword);
router.put('/change/info', passport.authenticate('user-bearer', { session: false }), userAccess, UserController.changeInfo);

router.post('/confirm', passport.authenticate('user-bearer', { session: false }), userAccess, UserController.confirmUser);
router.get('/confirm/resend', passport.authenticate('user-bearer', { session: false }), userAccess, UserController.resendConfirmUser);

router.post('/firebase', passport.authenticate('user-bearer', { session: false }), userAccess, UserController.registerfirebaseTokens);
router.get('/notifications', passport.authenticate('user-bearer', { session: false }), userAccess, UserController.getNotifications);
router.patch('/notifications/state', passport.authenticate('user-bearer', { session: false }), userAccess, UserController.changeNotificationState);
router.patch('/notifications/seen', passport.authenticate('user-bearer', { session: false }), userAccess, UserController.updateNotificationSeen);

module.exports = router;


