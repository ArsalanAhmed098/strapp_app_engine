const { UniversityModel } = require('../../models/v2');

// const getByDomain1 = domain =>
// UniversityModel.findOne({ domains: { $elemMatch: { $eq: domain } } });

const getByDomain = async (domain) => {
  let universitydata = await  UniversityModel.findOne({ 'university.domains': { $elemMatch: { $eq: domain } } });
  return universitydata;
}
module.exports = {
  getByDomain,
  // getByDomain1,

};
