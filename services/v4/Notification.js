const { NotificationModel } = require('../../models/v2');

const getAll = async user => {
  const result = await NotificationModel.aggregate([
    {
      $match: {
        receiver: user._id
      }
    },
    {
      $lookup: {
        from: 'students',
        let: { uId: '$sender' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$uId']
              }
            }
          },
          {
            $project: {
              userId: '$_id',
              _id: false,
              firstName: true,
              lastName: true,
              photo: true,
              major: true,
              email: true,
              username: true,
              verification: true,
            }
          }
        ],
        as: 'userSender'
      }
    },
    {
      $unwind: {
        path: '$userSender',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'sections',
        let: { uId: '$metadata.groupId' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$uId']
              }
            }
          },
          {
            $project: {
              _id: false,
              grouptype: true,
              photo: true,
            }
          }
        ],
        as: 'section'
      }
    },
    {
      $unwind: {
        path: '$section',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project: {
        _id: false,
        notificationId: '$_id',
        section: {
          $ifNull: ['$section', null]
        },
        metadata: true,
        payload: true,
        sender: {
          $ifNull: ['$userSender', null]
        },
        createdAt: true
      }
    },
    {
      $group: {
        _id: { $cond: { if: { $gt: ['$createdAt', user.notification.lastSeen] }, then: 'unseen', else: 'seen' } },
        notificationCount: { $sum: 1 },
        notifications: { $addToSet: '$$ROOT' }
      }
    },
  ]);
  return result.reduce((acc, curr) => {
    curr.notifications = curr.notifications.map(noti => {
      noti.payload = JSON.parse(noti.payload);
      noti.content = {
        title: noti.payload.notification.title,
        description: noti.payload.notification.body,
      };
      delete noti.payload;
      return noti;
    }).sort((ca, cb) => cb.createdAt - ca.createdAt);
    acc[curr._id] = {
      count: curr.notificationCount,
      notifications: curr.notifications
    };
    return acc;
  }, {});
};

const getAll1 = async user => {
  const result = await NotificationModel.aggregate([
    {
      $match: {
        receiver: user._id
      }
    },
    {
      $lookup: {
        from: 'students',
        let: { uId: '$sender' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$uId']
              }
            }
          },
          {
            $project: {
              userId: '$_id',
              _id: false,
              firstName: true,
              lastName: true,
              photo: true,
              major: true,
              email: true,
              username: true,
              verification: true,
            }
          }
        ],
        as: 'userSender'
      }
    },
    {
      $unwind: {
        path: '$userSender',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project: {
        _id: false,
        notificationId: '$_id',
        metadata: true,
        payload: true,
        sender: {
          $ifNull: ['$userSender', null]
        },
        createdAt: true
      }
    },
    {
      $group: {
        _id: { $cond: { if: { $gt: ['$createdAt', user.notification.lastSeen] }, then: 'unseen', else: 'seen' } },
        notificationCount: { $sum: 1 },
        notifications: { $addToSet: '$$ROOT' }
      }
    },
  ]);
  return result.reduce((acc, curr) => {
    curr.notifications = curr.notifications.map(noti => {
      noti.payload = JSON.parse(noti.payload);
      noti.content = {
        title: noti.payload.notification.title,
        description: noti.payload.notification.body,
      };
      delete noti.payload;
      return noti;
    }).sort((ca, cb) => cb.createdAt - ca.createdAt);
    acc[curr._id] = {
      count: curr.notificationCount,
      notifications: curr.notifications
    };
    return acc;
  }, {});
};

// ========== Utilities =============
const save = NotificationModel.create.bind(NotificationModel);

module.exports = {
  getAll,
  getAll1,
  save,
};
