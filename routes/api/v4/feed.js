const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
// const { FeedController } = require('../../../controllers/v3');
const { FeedController } = require('../../../controllers/v4');
const { userAccessV3 } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);

router.route('/')
  .get(FeedController.getAll);


  //---Feed of all campus Joined Sections ---//
  // router.route('/:campusCategoryId/campusfeed')
  // .get(FeedController.getAllCampusFeed);
   

  //---All Feed of  Joined Sections return ---//
  router.route('/campusfeed')
  .get(FeedController.getAllCampusFeed);

  router.route('/allfeed')
  .get(FeedController.getAllFeed);

  router.route('/campusfeed/exploresection')
  .get(FeedController.getAllCampusExploreFeed);

module.exports = router;
