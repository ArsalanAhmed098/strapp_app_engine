const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { FeedController } = require('../../../controllers/v2');
const { userAccess } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccess);

router.route('/')
  .get(FeedController.getAll);

module.exports = router;
