const router        = require('express').Router();
const userRouter    = require('./user');
const groupRouter   = require('./group');
const postRouter    = require('./post');
const commentRouter = require('./comment');
const feedRouter    = require('./feed');

router.use('/users',    userRouter);
router.use('/groups',   groupRouter);
router.use('/posts',    postRouter);
router.use('/comments', commentRouter);
router.use('/feed',     feedRouter);

module.exports = router;
