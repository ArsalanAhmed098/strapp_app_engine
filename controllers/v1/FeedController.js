const { UserSeenGroupModel } = require('../../models/v1');

const getFeed = async (req, res) => {
  const limit = +req.query.limit > 0 ? +req.query.limit : 20;
  const skip = +req.query.skip > 0 ? +req.query.skip : 0;
  let feed = await UserSeenGroupModel.aggregate([
    {
      $match: {
        userId: req.user._id,
      }
    },
    {
      $lookup: {
        from: 'groups',
        let: {
          gId: '$groupId'
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$_id', '$$gId'] },
                  { $ne: ['$isDeleted', true] }
                ]
              },
            }
          },
        ],
        as: 'group'
      }
    },
    { $unwind: '$group' },
    {
      $group:
      {
        _id: '$userId',
        groups: { $addToSet: { groupId: '$groupId', title: '$group.title' } }
      }
    },
    {
      $lookup: {
        from: 'posts',
        let: {
          gs: '$groups',
          gIds: '$groups.groupId',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $in: ['$groupId', '$$gIds'] },
                  { $ne: ['$isDeleted', true] }
                ]
              }
            }
          },
          {
            $sort: { _id: -1 }
          },
          {
            $skip: skip
          },
          {
            $limit: limit
          },
          {
            $lookup: {
              from: 'users',
              let: {
                uId: '$userId',
              },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $eq: ['$_id', '$$uId']
                    }
                  }
                },
                {
                  $project: {
                    userId: '$_id',
                    _id: false,
                    isVerified: true,
                    firstName: true,
                    lastName: true,
                    photo: true,
                    major: true,
                    email: true,
                    username: true,
                  }
                }
              ],
              as: 'user'
            }
          },
          {
            $lookup: {
              from: 'users',
              localField: "comments.userId",
              foreignField: "_id",
              as: 'commentedUsers'
            }
          },
          {
            $project: {
              postId: '$_id',
              _id: false,
              likes: true,
              dislikes: true,
              isRestricted: true,
              isAnonymous: true,
              content: true,
              user: true,
              comments: {
                $filter: {
                  input: '$comments',
                  as: 'comment',
                  cond: {
                    $ne: ['$$comment.isDeleted', true]
                  }
                }
              },
              commentedUsers: true,
              commentCount: {
                $cond: {
                  if: { $isArray: '$comments' }, then: {
                    $size: {
                      $filter: {
                        input: '$comments',
                        as: 'comment',
                        cond: {
                          $ne: ['$$comment.isDeleted', true]
                        }
                      }
                    }
                  }, else: 0
                }
              },
              createdAt: true,
              group: {
                $filter: {
                  input: '$$gs',
                  as: 'g',
                  cond: {
                    $eq: ['$$g.groupId', '$groupId']
                  }
                }
              }
            }
          },
          { $unwind: '$user' },
          { $unwind: '$group' },
        ],
        as: 'posts'
      }
    },
    {
      $project: {
        _id: false,
        posts: true
      }
    }
  ]);

  feed = feed.length ? feed[0].posts.map(post => {
    if (post.isRestricted && post.user.userId.toString() !== req.user._id.toString() && !req.user.isVerified) {
      post = {
        isRestricted: true,
        postId: post.postId
      };
    } else if (post.user.userId.toString() !== req.user._id.toString()) {
      post.user = post.isAnonymous ? null : post.user;
    }
    if (post.likes) {
      post.currentUserLiked =
        (post.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
        (post.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
        0;
      post.likes = post.likes.length - post.dislikes.length;
      delete post.dislikes;
    }
    if (post.comments) {
      post.comments = post.comments
        .filter(comment => !comment.parentId)
        .sort((ca, cb) => cb.createdAt - ca.createdAt)
        .slice(0, 2)
        .map(comment => {
          comment.commentId = comment._id;
          delete comment._id;

          comment.currentUserLiked =
            (comment.likes.find(like => like.toString() === req.user._id.toString()) && 1) ||
            (comment.dislikes.find(dislike => dislike.toString() === req.user._id.toString()) && -1) ||
            0;
          comment.likes = comment.likes.length - comment.dislikes.length;
          delete comment.dislikes;
          if (comment.isAnonymous) {
            comment.user = null;
          } else {
            const user = post.commentedUsers.find(user => user._id.toString() === comment.userId.toString());
            comment.user = {
              userId: user._id,
              isVerified: user.isVerified,
              firstName: user.firstName,
              photo: user.photo,
              major: user.major,
              lastName: user.lastName,
              email: user.email,
              username: user.username,
            };
          }
          delete comment.userId;
          delete comment.reports;
          return comment;
        });
      delete post.commentedUsers;
    }

    return post;
  }) : [];

  res.status(200).json({
    success: true,
    data: feed,
    message: 'feed fetched successfully',
    errorCode: null,
  });
};


module.exports = {
  getFeed,
};
