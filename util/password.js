const bcrypt = require('bcrypt');

const compare = bcrypt.compare;

const hash = password => bcrypt.hash(password, 8);

module.exports = {
  compare,
  hash,
}
