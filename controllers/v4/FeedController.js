const { Feed } = require('../../services/v4');

const getAll = async (req, res) => {
  const posts = await Feed.getAll(req.user, req.query);
  res.success(posts);
};

const getAllCampusFeed = async (req, res) => {
  const posts = await Feed.getAllCampusFeed(req.user, req.query);
  res.success(posts);
};

const getAllFeed = async (req, res) => {
  var fullUrl = req.protocol + '://' + req.get('host') + '/';
  const posts = await Feed.getAllFeed(req.user, req.query,fullUrl);
  res.success(posts);
};

const getAllCampusExploreFeed = async (req, res) => {
  var fullUrl = req.protocol + '://' + req.get('host') + '/';
  const posts = await Feed.getAllCampusExploreFeed(req.user, req.query, fullUrl);
  res.success(posts);
};

module.exports = {
  getAll,
  getAllFeed,
  getAllCampusFeed,
  getAllCampusExploreFeed,
};
