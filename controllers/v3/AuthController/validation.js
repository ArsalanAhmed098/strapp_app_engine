const { Auth } = require('../../../services/v3');

const token = async (req, res) => {
  await Auth.tokenValidation(req.body.token);
  res.success();
};

const email = async (req, res) => {
  await Auth.emailValidation(req.body.email);
  res.success();
};

const username = async (req, res) => {
  await Auth.usernameValidation(req.body.username);
  res.success();
};

module.exports = {
  token,
  email,
  username,
};
