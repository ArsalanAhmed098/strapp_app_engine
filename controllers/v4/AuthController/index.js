const { Auth } = require('../../../services/v4');
const validation = require('./validation');

const register = async (req, res) => {
  const hostURL = req.protocol + '://' + req.get('host');
  const user = await Auth.register(req.body, hostURL);
  res.success(user);
};

const login = async (req, res) => {
  const user = await Auth.login(req.user);
  res.success(user);
};

const logout = async (req, res) => {
  await Auth.logout(req.user);
  res.success();
};

module.exports = {
  register,
  login,
  logout,
  validation,
};
