const StudentModel = require('./student');
const BlockStudentModel = require('./block-student');
const GroupCategoryModel = require('./groupcategory');
const CampusGroupsModel = require('./campusgroup');
const GradeLevelsModel = require('./gradelevel');
const MajorsModel = require('./major');
const SectionModel = require('./section');
const SettingModel = require('./setting');
const UserLogsModel = require('./userlog');
const CommonModels = require('../common');

module.exports = {
    StudentModel,
    BlockStudentModel,
    SectionModel,
    GroupCategoryModel,
    CampusGroupsModel,
    GradeLevelsModel,
    MajorsModel,
    SettingModel,
    UserLogsModel,
    ...CommonModels,
};