const { Feed } = require('../../services/v3');

const getAll = async (req, res) => {
  const posts = await Feed.getAll(req.user, req.query);
  res.success(posts);
};

module.exports = {
  getAll,
};
