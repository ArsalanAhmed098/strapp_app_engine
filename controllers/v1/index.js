const asyncHandler = require('../../util/async-handler');
const UserController = require('./UserController');
const GroupController = require('./GroupController');
const PostController = require('./PostController');
const CommentController = require('./CommentController');
const FeedController = require('./FeedController');
const AdminController = require('./AdminController');

module.exports = asyncHandler({
  UserController,
  GroupController,
  CommentController,
  PostController,
  FeedController,
  AdminController,
});
