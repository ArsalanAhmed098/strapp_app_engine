const genRandom = (start, end) => start + Math.floor(Math.random() * (end - start));

module.exports = genRandom;
