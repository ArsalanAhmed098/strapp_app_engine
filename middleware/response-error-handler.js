const { errorMessage } = require('../util');

const responseErrorHandler = (err, req, res, next) => {
  console.log('Response error handler:', err);

  let { status, errorCode = errorMessage.getByStatusCode(status) } = err;

  const { message, statusCode } = errorMessage.getByErrorCode(errorCode);

  console.log(`statusCode: ${statusCode}, errorCode: ${errorCode}, message: ${message}`);

  res.status(statusCode).json({
    success: false,
    message,
    errorCode
  });
};

module.exports = responseErrorHandler;
