# Strapp

Platform for handling and management of michigan university assignment and student social media

## Requirement

- [node:v12](https://nodejs.org/en/)
- [mongo](https://www.mongodb.com/)

## Installation

1. Copy `.env` from `.env.example` and put your own information

2. Install dependencies by running the command below:

```bash
$ yarn install
```
or:
```bash
$ npm install
```

## Available Scripts

In the project directory, you can run:

```bash
$ yarn run dev
```
Run the project for development by using nodemon

```bash
$ yarn run seeds
```
Generate and import some fake data to database that can be useful in development

```bash
$ yarn run import-uni-info
```
Import just some sample universities information by fetching the data from a [github repository](https://raw.githubusercontent.com/Hipo/university-domains-list/master/world_universities_and_domains.json)

