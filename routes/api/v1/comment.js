const router = require('express').Router();
const passport = require('../../../conf/v1/auth');
const { CommentController } = require('../../../controllers/v1');
const { userAccess } = require('../../../util');

router.use(passport.authenticate('user-bearer', { session: false }), userAccess);

router.route('/')
  .post(CommentController.createComment);

router.route('/:commentId/report')
  .patch(CommentController.reportComment);

router.route('/:commentId/remove')
  .patch(CommentController.removeComment);

router.route('/:commentId/like')
  .patch(CommentController.likeComment)
  .delete(CommentController.removeLikeComment);

router.route('/:commentId/dislike')
  .patch(CommentController.dislikeComment)
  .delete(CommentController.removeDislikeComment);

module.exports = router;
