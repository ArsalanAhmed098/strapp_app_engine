const mongoose = require('mongoose');
const schedule = require('node-schedule');
const { PostModel, StudentModel, SectionModel, SettingModel, UserSeenGroupModel } = require('../../models/v2');
const { getMembership } = require('./Section');
const User = require('./User');
const Comment = require('./Comment');
const ObjectId = mongoose.Types.ObjectId;

const add = async(user, postData) => {
    const { groupId } = postData;
    user._id, groupId


    const sectionEntity = await UserSeenGroupModel.aggregate([{
            $match: {
                groupId: mongoose.Types.ObjectId(groupId),
                userId: user._id
            }
        },
        {
            $lookup: {
                from: 'sections',
                localField: 'groupId',
                foreignField: '_id',
                as: 'section'
            }
        },
        {
            $unwind: '$section'
        }
    ]);

    if (!sectionEntity.length)
        throwError('SECTION_NOT_FOUND');

    const { course, section } = sectionEntity[0].section;



    const post = await save({ userId: user._id, ...postData });

    return post;
    
    var checknotify = await SettingModel.find();
   // console.log('result', result[0].addpost_isNotified);

    // const startTime = new Date(Date.now() + 5000);
    // const endTime = new Date(startTime.getTime() + 5000);
    // const job = schedule.scheduleJob({ start: startTime, end: endTime, rule: '*/1 * * * * *' }, function() {
    // console.log('Time for tea!');

        
    const members =   UserSeenGroupModel.aggregate([{
        $match: {
            groupId: mongoose.Types.ObjectId(groupId),
            userId: { $ne: user._id }
        }
    },
    {
        $lookup: {
            from: 'students',
            let: {
                uId: '$userId',
            },
            pipeline: [{
                    $match: {
                        $expr: {
                            $and: (() => {
                                const query = [
                                    { $eq: ['$_id', '$$uId'] }
                                ];
                                if (post.isRestricted) {
                                    query.push({
                                        $eq: ['$verification.status', 'VERIFIED_STUDENT']
                                    })
                                }
                                return query;
                            })()
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'blockstudents',
                        let: {
                            postType: post.isAnonymous,
                            sId: user._id
                        },
                        pipeline: [{
                            $match: {
                                $expr: {
                                    $or: [{
                                            $and: [{
                                                    $or: [{
                                                            $and: [
                                                                { $eq: ['$blockerUser', '$$uId'] },
                                                                { $eq: ['$blockedUser', '$$sId'] },
                                                            ]
                                                        },
                                                        {
                                                            $and: [
                                                                { $eq: ['$blockerUser', '$$sId'] },
                                                                { $eq: ['$blockedUser', '$$uId'] },
                                                            ]
                                                        }
                                                    ]
                                                },
                                                { $eq: ['$isAnonymousPost', false] },
                                                { $eq: ['$$postType', false] },
                                            ]
                                        },
                                        {
                                            $and: [
                                                { $eq: ['$blockerUser', '$$uId'] },
                                                { $eq: ['$blockedUser', '$$sId'] },
                                                { $eq: ['$isAnonymousPost', true] },
                                                { $eq: ['$$postType', true] },
                                            ]
                                        }
                                    ]
                                }
                            }
                        }],
                        as: 'blockedUser'
                    }
                },
                {
                    $unwind: {
                        path: '$blockedUser',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: {
                        'blockedUser': { '$exists': false },
                    }
                },
            ],
            as: 'user'
        }
    },
    {
        $unwind: '$user'
    },
    {
        $replaceRoot: {
            newRoot: '$user'
        }
    },
]);

// return members;

// var checknotify = await SettingModel.find();
// // console.log('result', result[0].addpost_isNotified);

if (checknotify[0].addpost_isNotified === true) {

    if (postData.groupType == 'classgroup') {
        const notificationPayload = {
            sender: post.isAnonymous ? null : user,
            notification: {
                title: `${course.name}`,
                // body: post.content.title
                body: `A ${user.gradelevel} posted: "${post.content.title.substr(0,50)+'...'}"`
            },
            metadata: {
                // groupId,
                // postId: post._id,
                groupId: `${groupId}`,
                postId: `${post._id}`,
            }
        };
        User.bulkPushNotification(members, notificationPayload);
        return normalize(user, post);
    } else {
        const notificationPayload = {
            sender: post.isAnonymous ? null : user,
            notification: {
                title: `${section.name[0].toUpperCase() + section.name.substr(1)}`,
                // body: post.content.title
                body: `A ${user.gradelevel} posted: "${post.content.title.substr(0,50)+'...'}"`
            },
            metadata: {
                // groupId,
                // postId: post._id,
                groupId: `${groupId}`,
                postId: `${post._id}`,
            }
        };
        User.bulkPushNotification(members, notificationPayload);
        return normalize(user, postData);
    }

}

    // });

    // return post;

};

const addCampusPost = async(user, postData) => {
    const { groupId } = postData;
    user._id, groupId
    const sectionEntity = await UserSeenGroupModel.aggregate([{
            $match: {
                groupId: mongoose.Types.ObjectId(groupId),
                userId: user._id
            }
        },
        {
            $lookup: {
                from: 'sections',
                localField: 'groupId',
                foreignField: '_id',
                as: 'section'
            }
        },
        {
            $unwind: '$section'
        }
    ]);

    if (!sectionEntity.length)
        throwError('SECTION_NOT_FOUND');

    const { course, section } = sectionEntity[0].section;

    const post = await save({ userId: user._id, ...postData });

    const members = await UserSeenGroupModel.aggregate([{
            $match: {
                groupId: mongoose.Types.ObjectId(groupId),
                userId: { $ne: user._id }
            }
        },
        {
            $lookup: {
                from: 'students',
                let: {
                    uId: '$userId',
                },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: (() => {
                                    const query = [
                                        { $eq: ['$_id', '$$uId'] }
                                    ];
                                    if (post.isRestricted) {
                                        query.push({
                                            $eq: ['$verification.status', 'VERIFIED_STUDENT']
                                        })
                                    }
                                    return query;
                                })()
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: 'blockstudents',
                            let: {
                                postType: post.isAnonymous,
                                sId: user._id
                            },
                            pipeline: [{
                                $match: {
                                    $expr: {
                                        $or: [{
                                                $and: [{
                                                        $or: [{
                                                                $and: [
                                                                    { $eq: ['$blockerUser', '$$uId'] },
                                                                    { $eq: ['$blockedUser', '$$sId'] },
                                                                ]
                                                            },
                                                            {
                                                                $and: [
                                                                    { $eq: ['$blockerUser', '$$sId'] },
                                                                    { $eq: ['$blockedUser', '$$uId'] },
                                                                ]
                                                            }
                                                        ]
                                                    },
                                                    { $eq: ['$isAnonymousPost', false] },
                                                    { $eq: ['$$postType', false] },
                                                ]
                                            },
                                            {
                                                $and: [
                                                    { $eq: ['$blockerUser', '$$uId'] },
                                                    { $eq: ['$blockedUser', '$$sId'] },
                                                    { $eq: ['$isAnonymousPost', true] },
                                                    { $eq: ['$$postType', true] },
                                                ]
                                            }
                                        ]
                                    }
                                }
                            }],
                            as: 'blockedUser'
                        }
                    },
                    {
                        $unwind: {
                            path: '$blockedUser',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $match: {
                            'blockedUser': { '$exists': false },
                        }
                    },
                ],
                as: 'user'
            }
        },
        {
            $unwind: '$user'
        },
        {
            $replaceRoot: {
                newRoot: '$user'
            }
        },
    ]);

    const notificationPayload = {
        sender: post.isAnonymous ? null : user,
        notification: {
            title: `${course.name} • ${section.name[0].toUpperCase() + section.name.substr(1)}`,
            body: post.content.title
        },
        metadata: {
            groupId,
            postId: post._id,
        }
    };

    User.bulkPushNotification(members, notificationPayload);
    return normalize(user, post);
};

const getAllCampusPost = async(user, sectionId, query) => {
    const parseQuery = query => {
        const normalizeQuery = Object.entries(query).map(([key, value]) => `${key}=${value}`);
        const parsedQuery = normalizeQuery.reduce((acc, curr) => {
            if (curr.startsWith('read')) {
                const { limit = -1, skip = 0 } = Object.fromEntries(curr
                    .split('read')[1]
                    .replace(/(\]|\[)/g, '')
                    .split(',')
                    .reduce((acc, curr) => {
                        const [key, value] = curr.split('=');
                        acc.push([key, +value]);
                        return acc;
                    }, []));
                acc.read = { limit, skip };
            } else if (curr.startsWith('unread')) {
                const { limit = -1, skip = 0 } = Object.fromEntries(curr
                    .split('unread')[1]
                    .replace(/(\]|\[)/g, '')
                    .split(',')
                    .reduce((acc, curr) => {
                        const [key, value] = curr.split('=');
                        acc.push([key, +value]);
                        return acc;
                    }, []));
                acc.unread = { limit, skip };
            } else if (curr.startsWith('topic')) {
                acc.topic = curr.split('=')[1];
            }
            return acc;
        }, {
            read: {
                limit: -1,
                skip: 0
            },
            unread: {
                limit: -1,
                skip: 0
            },
            topic: '',
        });
        return parsedQuery;
    };

    const genPostPipeline = filterQuery => postType => {
        const pipeline = [{
                $sort: { createdAt: postType === 'read' ? -1 : 1 }
            },
            {
                $lookup: {
                    from: 'blockstudents',
                    let: {
                        postUserId: '$userId',
                        postType: '$isAnonymous',
                        postCreatedAt: '$createdAt',
                    },
                    pipeline: [{
                        $match: {
                            $expr: {
                                $or: [{
                                        $and: [
                                            { $eq: ['$blockerUser', user._id] },
                                            { $eq: ['$blockedUser', '$$postUserId'] },
                                            { $eq: ['$isAnonymousPost', '$$postType'] },
                                            { $lt: ['$blockDate', '$$postCreatedAt'] },
                                        ]
                                    },
                                    {
                                        $and: [
                                            { $eq: ['$blockedUser', user._id] },
                                            { $eq: ['$blockerUser', '$$postUserId'] },
                                            { $eq: ['$$postType', false] },
                                            { $eq: ['$isAnonymousPost', false] },
                                            { $lt: ['$blockDate', '$$postCreatedAt'] },
                                        ]
                                    }
                                ]
                            }
                        }
                    }],
                    as: 'blockedPost'
                }
            },
            {
                $unwind: {
                    path: '$blockedPost',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match: {
                    'blockedPost': { '$exists': false },
                    $expr: {
                        $and: (() => {
                            const query = [
                                { $eq: ['$groupId', '$$gId'] },
                                { $ne: ['$isDeleted', true] },
                                postType === 'read' ? { $lte: ['$createdAt', '$$lastSeen'] } : { $gt: ['$createdAt', '$$lastSeen'] }
                            ];
                            if (user.verification.status === 'UNIVERSITY_FACULTY') {
                                query.push({
                                    $or: [{
                                            $eq: ['$isRestricted', false]
                                        },
                                        {
                                            $and: [
                                                { $eq: ['$isRestricted', true] },
                                                { $eq: ['$userId', user._id] },
                                            ]
                                        },
                                    ]
                                })
                            }
                            return query;
                        })()
                    }
                }
            },
        ];
        pipeline.push({
                $lookup: {
                    from: 'students',
                    let: {
                        uId: '$userId',
                    },
                    pipeline: [{
                            $match: {
                                $expr: {
                                    $eq: ['$_id', '$$uId']
                                }
                            }
                        },
                        {
                            $project: User.project()
                        }
                    ],
                    as: 'user'
                }
            }, { $unwind: '$user' }, {
                $lookup: {
                    from: 'students',
                    localField: "comments.userId",
                    foreignField: "_id",
                    as: 'commentedUsers'
                }
            },
            // {
            //   $lookup: {
            //     from: 'blockstudents',
            //     localField: 'comments.userId',
            //     foreignField: 'blockedUser',
            //     as: 'blockedCommentedUsers'
            //   }
            // },
            // {
            //   $lookup: {
            //     from: 'blockstudents',
            //     localField: 'comments.userId',
            //     foreignField: 'blockerUser',
            //     as: 'blockerCommentedUsers'
            //   }
            // },
            {
                $project: {
                    postId: '$_id',
                    _id: false,
                    userId: true,
                    user: true,
                    likes: true,
                    dislikes: true,
                    isRestricted: true,
                    isAnonymous: true,
                    content: true,
                    topics: true,
                    // comments: Comment.project(),
                    // commentedUsers: true,
                    commentCount: Comment.countProject(),
                    // blockedCommentedUsers: true,
                    // blockerCommentedUsers: true,
                    createdAt: true,
                }
            }
        );

        filterQuery[postType].skip > 0 && pipeline.push({ $skip: filterQuery[postType].skip });
        filterQuery[postType].limit > 0 && pipeline.push({ $limit: filterQuery[postType].limit });

        return filterQuery[postType].limit ?
            pipeline : [{
                $match: {
                    $expr: false
                }
            }];
    };

    const postPipeline = genPostPipeline(parseQuery(query));
    let section = await SectionModel.aggregate([{
            $match: {
                _id: mongoose.Types.ObjectId(sectionId),
                isBlocked: false,
                isDeleted: false,
                grouptype: 'campusgroup'
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                let: { gId: '$_id' },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$groupId', '$$gId'] },
                                { $eq: ['$userId', user._id] }
                            ]
                        }
                    }
                }],
                as: 'userseengroups'
            }
        },
        { $unwind: '$userseengroups' },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                pipeline: postPipeline('read'),
                as: 'reads'
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                pipeline: postPipeline('unread'),
                as: 'unreads'
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$groupId', '$$gId'] },
                                    { $ne: ['$isDeleted', true] }
                                ]
                            }
                        }
                    },
                    {
                        $group: {
                            _id: { $cond: { if: { $gt: ['$createdAt', '$$lastSeen'] }, then: 'unread', else: 'read' } },
                            postCount: { $sum: 1 }
                        }
                    }
                ],
                as: 'totalPosts'
            }
        },
        {
            $project: {
                _id: false,
                'groupId': '$_id',
                // title: '$section.name',
                memberCount: true,
                university: '$university.name',
                // topics: false,
                // course: false,
                section: true,
                // sectionphoto: { $concat: [ fullUrl,"photo" ] },
                'lastSeen': '$userseengroups.lastSeen',
                readPosts: {
                    posts: '$reads',
                    count: {
                        '$filter': {
                            input: '$totalPosts',
                            as: 'elem',
                            cond: { $eq: ['$$elem._id', 'read'] }
                        }
                    }
                },
                unreadPosts: {
                    posts: '$unreads',
                    count: {
                        '$filter': {
                            input: '$totalPosts',
                            as: 'elem',
                            cond: { $eq: ['$$elem._id', 'unread'] }
                        }
                    }
                },
            }
        }
    ]).allowDiskUse(true);

    if (!section.length)
        throwError('SECTION_NOT_FOUND');

    section = section[0];
    const modifePostFields = (acc, post) => {
        if (
            post.isRestricted &&
            post.user.userId.toString() !== user._id.toString() &&
            user.verification.status !== 'VERIFIED_STUDENT'
        ) {
            post = user.verification.status === 'UNVERIFIED_ID' ? {
                    isRestricted: true,
                    postId: post.postId
                } :
                null;
        } else {
            post.userVerification = post.user.verification;
            // if (post.user.userId.toString() !== user._id.toString() && post.isAnonymous) {
            //   post.user = null;
            // }
        }
        if (post && post.likes) {
            post.currentUserLiked =
                (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
                (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
                0;
            post.likes = post.likes.length - post.dislikes.length;
            delete post.dislikes;
        }
        // if (post && post.comments) {
        //   post.comments = post.comments
        //     .filter(comment => !comment.parentId)
        //     .reduce(Comment.normalize(user, post), [])
        //     .sort((ca, cb) => cb.createdAt - ca.createdAt)
        //     .slice(0, 2);
        //   delete post.commentedUsers;
        //   delete post.blockedCommentedUsers;
        //   delete post.blockerCommentedUsers;
        // }
        if (post) acc.push(post);
        return acc;
    };
    section.readPosts.count = section.readPosts.count.length && section.readPosts.count[0].postCount;
    section.unreadPosts.count = section.unreadPosts.count.length && section.unreadPosts.count[0].postCount;
    section.readPosts.posts = section.readPosts.count ? section.readPosts.posts.reduce(modifePostFields, []) : [];
    section.unreadPosts.posts = section.unreadPosts.count ? section.unreadPosts.posts.reduce(modifePostFields, []) : [];

    return section;
};

const get = async(user, postId) => {
    const post = await PostModel.aggregate([{
            $match: {
                _id: mongoose.Types.ObjectId(postId),
                isDeleted: false
            }
        },
        {
            $lookup: {
                from: 'students',
                let: {
                    uId: '$userId',
                },
                pipeline: [{
                        $match: {
                            $expr: {
                                $eq: ['$_id', '$$uId']
                            }
                        }
                    },
                    {
                        $project: User.project()
                    }
                ],
                as: 'user'
            }
        },
        { $unwind: '$user' },
        {
            $lookup: {
                from: 'sections',
                localField: 'groupId',
                foreignField: '_id',
                as: 'group'
            }
        },
        { $unwind: '$group' },
        {
            $project: project()
        }
    ]);
    if (!post.length)
        throwError('POST_NOT_FOUND');

    return normalize(user, post[0]);
};

const getAll = async(user, sectionId, query) => {
    const parseQuery = query => {
        const normalizeQuery = Object.entries(query).map(([key, value]) => `${key}=${value}`);
        const parsedQuery = normalizeQuery.reduce((acc, curr) => {
            if (curr.startsWith('read')) {
                const { limit = -1, skip = 0 } = Object.fromEntries(curr
                    .split('read')[1]
                    .replace(/(\]|\[)/g, '')
                    .split(',')
                    .reduce((acc, curr) => {
                        const [key, value] = curr.split('=');
                        acc.push([key, +value]);
                        return acc;
                    }, []));
                acc.read = { limit, skip };
            } else if (curr.startsWith('unread')) {
                const { limit = -1, skip = 0 } = Object.fromEntries(curr
                    .split('unread')[1]
                    .replace(/(\]|\[)/g, '')
                    .split(',')
                    .reduce((acc, curr) => {
                        const [key, value] = curr.split('=');
                        acc.push([key, +value]);
                        return acc;
                    }, []));
                acc.unread = { limit, skip };
            } else if (curr.startsWith('topic')) {
                acc.topic = curr.split('=')[1];
            }
            return acc;
        }, {
            read: {
                limit: -1,
                skip: 0
            },
            unread: {
                limit: -1,
                skip: 0
            },
            topic: '',
        });
        return parsedQuery;
    };

    const genPostPipeline = filterQuery => postType => {
        const pipeline = [{
                $sort: { createdAt: postType === 'read' ? -1 : 1 }
            },
            {
                $lookup: {
                    from: 'blockstudents',
                    let: {
                        postUserId: '$userId',
                        postType: '$isAnonymous',
                        postCreatedAt: '$createdAt',
                    },
                    pipeline: [{
                        $match: {
                            $expr: {
                                $or: [{
                                        $and: [
                                            { $eq: ['$blockerUser', user._id] },
                                            { $eq: ['$blockedUser', '$$postUserId'] },
                                            { $eq: ['$isAnonymousPost', '$$postType'] },
                                            { $lt: ['$blockDate', '$$postCreatedAt'] },
                                        ]
                                    },
                                    {
                                        $and: [
                                            { $eq: ['$blockedUser', user._id] },
                                            { $eq: ['$blockerUser', '$$postUserId'] },
                                            { $eq: ['$$postType', false] },
                                            { $eq: ['$isAnonymousPost', false] },
                                            { $lt: ['$blockDate', '$$postCreatedAt'] },
                                        ]
                                    }
                                ]
                            }
                        }
                    }],
                    as: 'blockedPost'
                }
            },
            {
                $unwind: {
                    path: '$blockedPost',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match: {
                    'blockedPost': { '$exists': false },
                    $expr: {
                        $and: (() => {
                            const query = [
                                { $eq: ['$groupId', '$$gId'] },
                                { $ne: ['$isDeleted', true] },
                                postType === 'read' ? { $lte: ['$createdAt', '$$lastSeen'] } : { $gt: ['$createdAt', '$$lastSeen'] }
                            ];
                            if (user.verification.status === 'UNIVERSITY_FACULTY') {
                                query.push({
                                    $or: [{
                                            $eq: ['$isRestricted', false]
                                        },
                                        {
                                            $and: [
                                                { $eq: ['$isRestricted', true] },
                                                { $eq: ['$userId', user._id] },
                                            ]
                                        },
                                    ]
                                })
                            }
                            return query;
                        })()
                    }
                }
            },
        ];
        pipeline.push({
                $lookup: {
                    from: 'students',
                    let: {
                        uId: '$userId',
                    },
                    pipeline: [{
                            $match: {
                                $expr: {
                                    $eq: ['$_id', '$$uId']
                                }
                            }
                        },
                        {
                            $project: User.project()
                        }
                    ],
                    as: 'user'
                }
            }, { $unwind: '$user' }, {
                $lookup: {
                    from: 'students',
                    localField: "comments.userId",
                    foreignField: "_id",
                    as: 'commentedUsers'
                }
            },
            // {
            //   $lookup: {
            //     from: 'blockstudents',
            //     localField: 'comments.userId',
            //     foreignField: 'blockedUser',
            //     as: 'blockedCommentedUsers'
            //   }
            // },
            // {
            //   $lookup: {
            //     from: 'blockstudents',
            //     localField: 'comments.userId',
            //     foreignField: 'blockerUser',
            //     as: 'blockerCommentedUsers'
            //   }
            // },
            {
                $lookup: {
                    from: 'sections',
                    localField: 'groupId',
                    foreignField: '_id',
                    as: 'sectionsnew'
                }
            }, { $unwind: '$sectionsnew' }, {
                $project: {
                    postId: '$_id',
                    _id: false,
                    section: {
                        // 'sectionphoto': { $concat: [ fullUrl,"$sectionsnew.photo" ] },
                        'sectionphoto': "$sectionsnew.photo",
                        'name': "$sectionsnew.section.name",
                        'grouptype': "$sectionsnew.grouptype"
                    },
                    userId: true,
                    user: true,
                    likes: true,
                    dislikes: true,
                    isRestricted: true,
                    isAnonymous: true,
                    content: true,
                    topics: true,
                    // comments: Comment.project(),
                    // commentedUsers: true,
                    commentCount: Comment.countProject(),
                    // blockedCommentedUsers: true,
                    // blockerCommentedUsers: true,
                    createdAt: true,
                }
            }
        );

        filterQuery[postType].skip > 0 && pipeline.push({ $skip: filterQuery[postType].skip });
        filterQuery[postType].limit > 0 && pipeline.push({ $limit: filterQuery[postType].limit });

        return filterQuery[postType].limit ?
            pipeline : [{
                $match: {
                    $expr: false
                }
            }];
    };

    const postPipeline = genPostPipeline(parseQuery(query));
    let section = await SectionModel.aggregate([{
            $match: {
                _id: mongoose.Types.ObjectId(sectionId),
                isBlocked: false,
                isDeleted: false
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                let: { gId: '$_id' },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$groupId', '$$gId'] },
                                { $eq: ['$userId', user._id] }
                            ]
                        }
                    }
                }],
                as: 'userseengroups'
            }
        },
        { $unwind: '$userseengroups' },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                pipeline: postPipeline('read'),
                as: 'reads'
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                pipeline: postPipeline('unread'),
                as: 'unreads'
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$groupId', '$$gId'] },
                                    { $ne: ['$isDeleted', true] }
                                ]
                            }
                        }
                    },
                    {
                        $group: {
                            _id: { $cond: { if: { $gt: ['$createdAt', '$$lastSeen'] }, then: 'unread', else: 'read' } },
                            postCount: { $sum: 1 }
                        }
                    }
                ],
                as: 'totalPosts'
            }
        },
        {
            $project: {
                _id: false,
                'groupId': '$_id',
                title: '$course.name',
                memberCount: true,
                university: '$university.name',
                topics: true,
                course: true,
                section: true,
                'lastSeen': '$userseengroups.lastSeen',
                readPosts: {
                    posts: '$reads',
                    count: {
                        '$filter': {
                            input: '$totalPosts',
                            as: 'elem',
                            cond: { $eq: ['$$elem._id', 'read'] }
                        }
                    }
                },
                unreadPosts: {
                    posts: '$unreads',
                    count: {
                        '$filter': {
                            input: '$totalPosts',
                            as: 'elem',
                            cond: { $eq: ['$$elem._id', 'unread'] }
                        }
                    }
                },
            }
        }
    ]).allowDiskUse(true);

    if (!section.length)
        throwError('SECTION_NOT_FOUND');

    section = section[0];
    const modifePostFields = (acc, post) => {
        if (
            post.isRestricted &&
            post.user.userId.toString() !== user._id.toString() &&
            user.verification.status !== 'VERIFIED_STUDENT'
        ) {
            post = user.verification.status === 'UNVERIFIED_ID' ? {
                    isRestricted: true,
                    postId: post.postId
                } :
                null;
        } else {
            post.userVerification = post.user.verification;
            // if (post.user.userId.toString() !== user._id.toString() && post.isAnonymous) {
            //   post.user = null;
            // }
        }
        if (post && post.likes) {
            post.currentUserLiked =
                (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
                (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
                0;
            post.likes = post.likes.length - post.dislikes.length;
            delete post.dislikes;
        }
        // if (post && post.comments) {
        //   post.comments = post.comments
        //     .filter(comment => !comment.parentId)
        //     .reduce(Comment.normalize(user, post), [])
        //     .sort((ca, cb) => cb.createdAt - ca.createdAt)
        //     .slice(0, 2);
        //   delete post.commentedUsers;
        //   delete post.blockedCommentedUsers;
        //   delete post.blockerCommentedUsers;
        // }
        if (post) acc.push(post);
        return acc;
    };
    section.readPosts.count = section.readPosts.count.length && section.readPosts.count[0].postCount;
    section.unreadPosts.count = section.unreadPosts.count.length && section.unreadPosts.count[0].postCount;
    section.readPosts.posts = section.readPosts.count ? section.readPosts.posts.reduce(modifePostFields, []) : [];
    section.unreadPosts.posts = section.unreadPosts.count ? section.unreadPosts.posts.reduce(modifePostFields, []) : [];

    return section;
};

//-------------- allposts detailed work-------------
const getAllCatrgoryPost = async(user, query, campusCategoryId, fullUrl) => {
    const parseQuery = query => {
        const normalizeQuery = Object.entries(query).map(([key, value]) => `${key}=${value}`);
        const parsedQuery = normalizeQuery.reduce((acc, curr) => {
            if (curr.startsWith('read')) {
                const { limit = -1, skip = 0 } = Object.fromEntries(curr
                    .split('read')[1]
                    .replace(/(\]|\[)/g, '')
                    .split(',')
                    .reduce((acc, curr) => {
                        const [key, value] = curr.split('=');
                        acc.push([key, +value]);
                        return acc;
                    }, []));
                acc.read = { limit, skip };
            } else if (curr.startsWith('unread')) {
                const { limit = -1, skip = 0 } = Object.fromEntries(curr
                    .split('unread')[1]
                    .replace(/(\]|\[)/g, '')
                    .split(',')
                    .reduce((acc, curr) => {
                        const [key, value] = curr.split('=');
                        acc.push([key, +value]);
                        return acc;
                    }, []));
                acc.unread = { limit, skip };
            } else if (curr.startsWith('topic')) {
                acc.topic = curr.split('=')[1];
            }
            return acc;
        }, {
            read: {
                limit: -1,
                skip: 0
            },
            unread: {
                limit: -1,
                skip: 0
            },
            topic: '',
        });
        return parsedQuery;
    };


    // let user_id=[];
    let section_id = [];
    let id = user._id;
    let campusgroupid = campusCategoryId;
    let sectionId = await UserSeenGroupModel.find({
        'userId': mongoose.Types.ObjectId(id),
        // 'campusgroupId': { $ne: null } ,
        'campusgroupId': mongoose.Types.ObjectId(campusgroupid),

    }, { groupId: 1, contribs: 1 });

    if (!sectionId)
        throwError('SECTIONS_NOT_FOUND1');

    sectionId.forEach(async(element) => {
        // user_id.push(element.userId);
        section_id.push(element.groupId);

        // console.log('user',element.userId,'sectionId',element.groupId);
    });


    const genPostPipeline = filterQuery => postType => {
        const pipeline = [
            // {
            //   $sort: { createdAt: postType === 'read' ? -1 : 1 }
            // },
            // {
            //   $lookup: {
            //     from: 'blockstudents',
            //     let: {
            //       postUserId: '$userId',
            //       postType: '$isAnonymous',
            //       postCreatedAt: '$createdAt',
            //     },
            //     pipeline: [
            //       {
            //         $match: {
            //           $expr: {
            //             $or: [
            //               {
            //                 $and: [
            //                   { $eq: ['$blockerUser', user._id] },
            //                   { $eq: ['$blockedUser', '$$postUserId'] },
            //                   { $eq: ['$isAnonymousPost', '$$postType'] },
            //                   { $lt: ['$blockDate', '$$postCreatedAt'] },
            //                 ]
            //               },
            //               {
            //                 $and: [
            //                   { $eq: ['$blockedUser', user._id] },
            //                   { $eq: ['$blockerUser', '$$postUserId'] },
            //                   { $eq: ['$$postType', false] },
            //                   { $eq: ['$isAnonymousPost', false] },
            //                   { $lt: ['$blockDate', '$$postCreatedAt'] },
            //                 ]
            //               }
            //             ]
            //           }
            //         }
            //       }
            //     ],
            //     as: 'blockedPost'
            //   }
            // },
            // {
            //   $unwind: {
            //     path: '$blockedPost',
            //     preserveNullAndEmptyArrays: true
            //   }
            // },
            {
                $match: {
                    // 'blockedPost': { '$exists': false },
                    $expr: {
                        $and: (() => {
                            const query = [
                                // { $eq: ['$groupId', '$$gId'] },
                                { $in: ["$groupId", section_id] },
                                { $ne: ['$isDeleted', true] },
                                // postType === 'read' ? { $lte: ['$createdAt', '$$lastSeen'] } : { $gt: ['$createdAt', '$$lastSeen'] }
                            ];
                            if (user.verification.status === 'UNIVERSITY_FACULTY') {
                                query.push({
                                    $or: [{
                                            $eq: ['$isRestricted', false]
                                        },
                                        {
                                            $and: [
                                                { $eq: ['$isRestricted', true] },
                                                { $eq: ['$userId', user._id] },
                                            ]
                                        },
                                    ]
                                })
                            }
                            console.log(query);
                            return query;
                        })()
                    }
                }
            }
        ];
        pipeline.push({
                $lookup: {
                    from: 'students',
                    let: {
                        uId: '$userId',
                    },
                    pipeline: [{
                            $match: {
                                $expr: {
                                    $eq: ['$_id', '$$uId']
                                }
                            }
                        },
                        {
                            $project: User.project()
                        }
                    ],
                    as: 'user'
                }
            }, { $unwind: '$user' }, {
                $lookup: {
                    from: 'students',
                    localField: "comments.userId",
                    foreignField: "_id",
                    as: 'commentedUsers'
                }
            },
            // {
            //   $lookup: {
            //     from: 'blockstudents',
            //     localField: 'comments.userId',
            //     foreignField: 'blockedUser',
            //     as: 'blockedCommentedUsers'
            //   }
            // },
            // {
            //   $lookup: {
            //     from: 'blockstudents',
            //     localField: 'comments.userId',
            //     foreignField: 'blockerUser',
            //     as: 'blockerCommentedUsers'
            //   }
            // },
            {
                $lookup: {
                    from: 'sections',
                    localField: 'groupId',
                    foreignField: '_id',
                    as: 'sectionsnew'
                }
            }, { $unwind: '$sectionsnew' }, {
                $project: {
                    postId: '$_id',
                    // groupId: true,
                    section: {
                        'sectionphoto': { $concat: [fullUrl, "$sectionsnew.photo"] },
                        'name': "$sectionsnew.section.name",
                        'grouptype': "$sectionsnew.grouptype"
                    },
                    _id: false,
                    userId: true,
                    user: true,
                    likes: true,
                    dislikes: true,
                    isRestricted: true,
                    isAnonymous: true,
                    content: true,
                    // topics: true,
                    topics: [],
                    // comments: Comment.project(),
                    // commentedUsers: true,
                    commentCount: Comment.countProject(),
                    // blockedCommentedUsers: true,
                    // blockerCommentedUsers: true,
                    createdAt: true,
                }
            }
        );

        filterQuery[postType].skip > 0 && pipeline.push({ $skip: filterQuery[postType].skip });
        filterQuery[postType].limit > 0 && pipeline.push({ $limit: filterQuery[postType].limit });

        return filterQuery[postType].limit ?
            pipeline : [{
                $match: {
                    $expr: false
                }
            }];
    };

    const postPipeline = genPostPipeline(parseQuery(query));
    let section = await SectionModel.aggregate([{
            $match: {
                // _id: { $in: 
                //   section_id},
                // 'campusgroupId': { $ne: null } ,
                'campusgroupId': mongoose.Types.ObjectId(campusCategoryId),
                'grouptype': 'campusgroup',
                isBlocked: false,
                isDeleted: false
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                let: { gId: '$_id' },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$groupId', '$$gId'] },
                                // { $in: ["$groupId", section_id] },
                                { $eq: ['$userId', ObjectId(user._id)] },
                                // { $eq: ['$campusgroupId', ObjectId(campusCategoryId)] }
                            ]
                        }
                    }
                }],
                as: 'userseengroups'
            }
        },
        { $unwind: '$userseengroups' },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                // let: { gId: section_id, lastSeen: '$userseengroups.lastSeen' },
                pipeline: postPipeline('read'),
                as: 'reads'
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                // let: { gId: section_id, lastSeen: '$userseengroups.lastSeen' },
                pipeline: postPipeline('unread'),
                as: 'unreads'
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
                // let: { gId: section_id, lastSeen: '$userseengroups.lastSeen' },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: [

                                    // { $eq: ['$groupId', '$$gId'] },
                                    { $in: ["$groupId", section_id] },
                                    { $ne: ['$isDeleted', true] }
                                ]
                            }
                        }
                    },
                    {
                        $group: {
                            _id: { $cond: { if: { $gt: ['$createdAt', '$$lastSeen'] }, then: 'unread', else: 'read' } },
                            postCount: { $sum: 1 }
                        }
                    }
                ],
                as: 'totalPosts'
            }
        },
        {
            $project: {
                _id: false,
                // groupId:true,
                // 'groupId': '$_id',
                title: '$course.name',
                memberCount: true,
                university: '$university.name',
                // topics: true,
                // course: true,
                topics: [],
                course: [],
                section: true,
                'lastSeen': '$userseengroups.lastSeen',
                readPosts: {
                    posts: '$reads',
                    count: {
                        '$filter': {
                            input: '$totalPosts',
                            as: 'elem',
                            cond: { $eq: ['$$elem._id', 'read'] }
                        }
                    }
                },
                unreadPosts: {
                    posts: '$unreads',
                    count: {
                        '$filter': {
                            input: '$totalPosts',
                            as: 'elem',
                            cond: { $eq: ['$$elem._id', 'unread'] }
                        }
                    }
                },
            }
        },
    ]).allowDiskUse(true);
    // console.log('idhar hai ',section_id);

    if (!section.length)
        throwError('SECTION_NOT_FOUND2');

    section = section[0];
    const modifePostFields = (acc, post) => {
        if (
            post.isRestricted &&
            post.user.userId.toString() !== user._id.toString() &&
            user.verification.status !== 'VERIFIED_STUDENT'
        ) {
            post = user.verification.status === 'UNVERIFIED_ID' ? {
                    isRestricted: true,
                    postId: post.postId
                } :
                null;
        } else {
            post.userVerification = post.user.verification;
            // if (post.user.userId.toString() !== user._id.toString() && post.isAnonymous) {
            //   post.user = null;
            // }
        }
        if (post && post.likes) {
            post.currentUserLiked =
                (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
                (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
                0;
            post.likes = post.likes.length - post.dislikes.length;
            delete post.dislikes;
        }
        // if (post && post.comments) {
        //   post.comments = post.comments
        //     .filter(comment => !comment.parentId)
        //     .reduce(Comment.normalize(user, post), [])
        //     .sort((ca, cb) => cb.createdAt - ca.createdAt)
        //     .slice(0, 2);
        //   delete post.commentedUsers;
        //   delete post.blockedCommentedUsers;
        //   delete post.blockerCommentedUsers;
        // }
        if (post) acc.push(post);
        return acc;
    };
    section.readPosts.count = section.readPosts.count.length && section.readPosts.count[0].postCount;
    section.unreadPosts.count = section.unreadPosts.count.length && section.unreadPosts.count[0].postCount;
    section.readPosts.posts = section.readPosts.count ? section.readPosts.posts.reduce(modifePostFields, []) : [];
    section.unreadPosts.posts = section.unreadPosts.count ? section.unreadPosts.posts.reduce(modifePostFields, []) : [];

    return section;
};

const getAllByTopic = async(user, sectionId, query) => {
    const limit = +query.limit > 0 ? +query.limit : 20;
    const skip = +query.skip > 0 ? +query.skip : 0;
    const topics = query.topics.split(',').map(topic => topic.trim());
    const matchQuery = (() => ([{
            $lookup: {
                from: 'blockstudents',
                let: {
                    postUserId: '$userId',
                    postType: '$isAnonymous',
                    postCreatedAt: '$createdAt',
                },
                pipeline: [{
                    $match: {
                        $expr: {
                            $or: [{
                                    $and: [
                                        { $eq: ['$blockerUser', user._id] },
                                        { $eq: ['$blockedUser', '$$postUserId'] },
                                        { $eq: ['$isAnonymousPost', '$$postType'] },
                                        { $lt: ['$blockDate', '$$postCreatedAt'] },
                                    ]
                                },
                                {
                                    $and: [
                                        { $eq: ['$blockedUser', user._id] },
                                        { $eq: ['$blockerUser', '$$postUserId'] },
                                        { $eq: ['$$postType', false] },
                                        { $eq: ['$isAnonymousPost', false] },
                                        { $lt: ['$blockDate', '$$postCreatedAt'] },
                                    ]
                                }
                            ]
                        }
                    }
                }],
                as: 'blockedPost'
            }
        },
        {
            $unwind: {
                path: '$blockedPost',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: {
                'blockedPost': { '$exists': false },
                $and: (() => {
                    const query = [
                        { isDeleted: { $ne: true } },
                        { groupId: mongoose.Types.ObjectId(sectionId) },
                        { topics: { $exists: true } },
                        { topics: { $all: topics } },
                    ];
                    if (user.verification.status === 'UNIVERSITY_FACULTY') {
                        query.push({
                            $or: [{
                                    isRestricted: false
                                },
                                {
                                    $and: [
                                        { isRestricted: true },
                                        { userId: user._id }
                                    ]
                                },
                            ]
                        })
                    }
                    return query;
                })()
            }
        },
    ]))
    const count = (await PostModel.aggregate(matchQuery())).length;

    let posts = await PostModel.aggregate([
        ...matchQuery(),
        {
            $sort: { _id: -1 }
        },
        {
            $skip: skip
        },
        {
            $limit: limit
        },
        {
            $lookup: {
                from: 'students',
                let: {
                    uId: '$userId',
                },
                pipeline: [{
                        $match: {
                            $expr: {
                                $eq: ['$_id', '$$uId']
                            }
                        }
                    },
                    {
                        $project: User.project(),
                    }
                ],
                as: 'user'
            }
        },
        { $unwind: '$user' },
        {
            $lookup: {
                from: 'students',
                localField: "comments.userId",
                foreignField: "_id",
                as: 'commentedUsers'
            }
        },
        // {
        //   $lookup: {
        //     from: 'blockstudents',
        //     localField: 'comments.userId',
        //     foreignField: 'blockedUser',
        //     as: 'blockedCommentedUsers'
        //   }
        // },
        // {
        //   $lookup: {
        //     from: 'blockstudents',
        //     localField: 'comments.userId',
        //     foreignField: 'blockerUser',
        //     as: 'blockerCommentedUsers'
        //   }
        // },
        {
            $project: {
                postId: '$_id',
                _id: false,
                userId: true,
                user: true,
                likes: true,
                dislikes: true,
                isRestricted: true,
                isAnonymous: true,
                content: true,
                topics: true,
                // comments: Comment.project(),
                // commentedUsers: true,
                commentCount: Comment.countProject(),
                // blockedCommentedUsers: true,
                // blockerCommentedUsers: true,
                createdAt: true,
            }
        },
    ]);
    posts = posts.reduce((acc, post) => {
        if (
            post.isRestricted &&
            post.user.userId.toString() !== user._id.toString() &&
            user.verification.status !== 'VERIFIED_STUDENT'
        ) {
            post = user.verification.status === 'UNVERIFIED_ID' ? {
                    isRestricted: true,
                    postId: post.postId
                } :
                null;
        } else {
            post.userVerification = post.user.verification;
            // if (post.user.userId.toString() !== user._id.toString() && post.isAnonymous) {
            //   post.user = null;
            // }
        }
        if (post && post.likes) {
            post.currentUserLiked =
                (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
                (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
                0;
            post.likes = post.likes.length - post.dislikes.length;
            delete post.dislikes;
        }
        // if (post && post.comments) {
        //   post.comments = post.comments
        //     .filter(comment => !comment.parentId)
        //     .reduce(Comment.normalize(user, post), [])
        //     .sort((ca, cb) => cb.createdAt - ca.createdAt)
        //     .slice(0, 2);
        //   delete post.commentedUsers;
        //   delete post.blockedCommentedUsers;
        //   delete post.blockerCommentedUsers;
        // }
        if (post) acc.push(post);
        return acc;
    }, []);

    return {
        readPosts: {
            posts,
            count
        }
    };
};

const update = async(user, postId, postData) => {
    const post = await PostModel.findOneAndUpdate({
        userId: user._id,
        _id: postId,
    }, postData, { new: true });
    if (!post)
        throwError('POST_NOT_FOUND');

    post._doc.postId = post._doc._id;
    delete post._doc._id;
    post._doc.currentUserLiked =
        (post._doc.likes.find(like => like.toString() === user._id.toString()) && 1) ||
        (post._doc.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
        0;
    post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
    delete post._doc.dislikes;
    delete post._doc.comments;

    return post;
};

const remove = async(user, postId) => {
    const post = await PostModel.findOneAndUpdate({
        userId: user._id,
        _id: postId,
    }, {
        isDeleted: true
    });
    if (!post)
        throwError('POST_NOT_FOUND');
};

const report = async(user, postId, reasons) => {
    const post = await PostModel.findById(postId);
    if (!post)
        throwError('POST_NOT_FOUND');

    reasons.forEach(reason => {
        if (!post.reports[reason])
            reason = 'other';
        if (!post.reports[reason].find(userId => userId.toString() === user._id.toString()))
            post.reports[reason].push(user._id);
    });

    await post.save();
};

const likeOperation = likeType => async(user, postId, groupType) => {
    var post = await PostModel.findByIdAndUpdate(postId, (() => {
        switch (likeType) {
            case 'like':
                return {
                    $pull: { dislikes: user._id },
                    $addToSet: { likes: user._id }
                };
            case 'unlike':
                return {
                    $pull: { likes: user._id },
                };
            case 'dislike':
                return {
                    $pull: { likes: user._id },
                    $addToSet: { dislikes: user._id },
                };
            case 'undislike':
                return {
                    $pull: { dislikes: user._id },
                };
        }
    })(), { new: true }).select('likes dislikes');
    if (!post)
        throwError('POST_NOT_FOUND');

    post._doc.postId = post._doc._id;
    delete post._doc._id;
    post._doc.currentUserLiked =
        (post._doc.likes.find(like => like.toString() === user._id.toString()) && 1) ||
        (post._doc.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
        0;
    post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
    delete post._doc.dislikes;

    ////--- My Work Start to notifiy User ---//////
    var postid = post._doc.postId
        // console.log('check',
        //     postid);

    var getPostData = await PostModel.find({

        _id: mongoose.Types.ObjectId(postid),
    });
    // console.log('result', getPostData[0].userId);

    var customuserid = getPostData[0].userId
        // console.log('check',
        //     postid);

    var getReceiverData = await StudentModel.find({

        _id: mongoose.Types.ObjectId(customuserid),
    });

    // console.log('result', getReceiverData[0]);
    // console.log('checklast', getPostData[0].groupId, getPostData[0]._id)

    var getSectionData = await SectionModel.find({

        _id: mongoose.Types.ObjectId(getPostData[0].groupId),
    });

    // console.log('result', getSectionData[0]);
    // console.log('checklast', getSectionData[0].section.name, getSectionData[0].course.name)

    if (groupType == 'classgroup') {
        var notificationPayload = {
            sender: user,
            // sender: commentData.isAnonymous ? null : user,
            notification: {
                title: `${getSectionData[0].course.name}`,
                // title: `Test class `,
                body: `A ${user.gradelevel} liked your post"${getPostData[0].content.title.substr(0,50)+'...'}"`,

            },
            metadata: {
                groupId: `${getPostData[0].groupId}`,
                postId: `${getPostData[0]._id}`,
            }
        };
    } else {
        var notificationPayload = {
            // sender: commentData.isAnonymous ? null : user,
            sender: user,
            notification: {
                // title: `${getSectionData[0].section.name.toUpperCase() + getSectionData[0].section.name.substr(1)}`,
                title: `${getSectionData[0].section.name.toUpperCase()}`,

                // title: `Test campus`,
                body: `A ${user.gradelevel} liked your post: ${getPostData[0].content.title.substr(0,50)+'...'}`,
            },
            metadata: {
                groupId: `${getPostData[0].groupId}`,
                postId: `${getPostData[0]._id}`,
            }
        };
    }

    var checknotify = await SettingModel.find();
    console.log('result', checknotify[0].likepost_isNotified);

    if (checknotify[0].likepost_isNotified === true && user._id != customuserid) {
        User.pushNotification(getReceiverData[0], notificationPayload);
    }

    return post;
};

const like = likeOperation('like');

const unlike = likeOperation('unlike');

const dislike = likeOperation('dislike');

const undislike = likeOperation('undislike');

// ========== Utilities =============
const save = PostModel.create.bind(PostModel);

const normalize = (user, post) => {
    if (post.isRestricted && post.userId.toString() !== user._id.toString() && !user.verification.isVerified) {
        post = {
            isRestricted: post.isRestricted,
            postId: post.postId
        }
    } else {
        post.currentUserLiked =
            (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
            (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
            0;
        if (post._doc) {
            post._doc.likes = post.likes.length - post.dislikes.length;
            delete post._doc.dislikes;
        } else {
            post.likes = post.likes.length - post.dislikes.length;
            delete post.dislikes;
        }
        if (post._id) {
            post._doc.postId = post._doc._id;
            delete post._doc._id;
        }
        // if (post.groupId) {
        //   post._doc.sectionId = post._doc.groupId;
        //   delete post._doc.groupId;
        // }
        if (post.reports)
            delete post._doc.reports;
    }
    return post;
};

const project = () => ({
    _id: false,
    user: true,
    postId: '$_id',
    likes: true,
    dislikes: true,
    isRestricted: true,
    isAnonymous: true,
    userId: true,
    topics: true,
    groupId: '$group._id',
    // section : '$group.section',
    // photo: '$group.photo',
    section: {
        grouptype: '$group.grouptype',
        name: '$group.section.name',
        number: '$group.section.number',
        category: '$group.section.category',
        sectionphoto: '$group.photo'
    },
    course: {
        name: '$group.course.name'
    },
    content: true,
    commentCount: Comment.countProject(),
    createdAt: true,
});

module.exports = {
    add,
    addCampusPost,
    getAllCampusPost,
    getAllCatrgoryPost,
    get,
    getAll,
    getAllByTopic,
    update,
    remove,
    report,
    like,
    unlike,
    dislike,
    undislike,
    save,
};