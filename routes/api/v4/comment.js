const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
// const { CommentController } = require('../../../controllers/v3');
const { CommentController } = require('../../../controllers/v4');
const { userAccessV3 } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);

router.route('/')
  .post(CommentController.add)
  .get(CommentController.getAll);

router.route('/:commentId')
  .delete(CommentController.remove);

router.route('/:commentId/report')
  .patch(CommentController.report);

router.route('/:commentId/like')
  .patch(CommentController.like)
  .delete(CommentController.unlike);

router.route('/:commentId/dislike')
  .patch(CommentController.dislike)
  .delete(CommentController.undislike);

module.exports = router;
