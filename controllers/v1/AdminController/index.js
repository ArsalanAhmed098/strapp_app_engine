const auth = require('./auth');
const pages = require('./pages');
const apis = require('./apis');

module.exports = {
  ...auth,
  ...pages,
  ...apis,
}
