const mongoose = require('mongoose');

const UniversitySchema = new mongoose.Schema({
  university:{
    name: {
    type: String,
    unique: true,
    required: true,
  },
  domains: [{
    type: String
  }]
},
  isBlocked: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
},{
    timestamps: true
  });

UniversitySchema.statics.findByDomain = function (domain) {
  return this.findOne({ domains: { $elemMatch: { $eq: domain } } });
};

module.exports = mongoose.model('University', UniversitySchema);
