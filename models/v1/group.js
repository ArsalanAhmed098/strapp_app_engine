const mongoose = require('mongoose');

const GroupSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    uppercase: true
  },
  memberCount: {
    type: Number,
    default: 0
  },
  university: {
    name: {
      type: String,
      required: true,
    },
    domains: [{
      type: String
    }]
  },
  isBlocked: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  topics: {
    type: Array,
    default: ['general', 'notes', 'homework']
  },
  grouptype: {
    type: String,
    default: 'classgroup'
  }
}, {
  timestamps: true
});

GroupSchema.statics.autoJoin = async function ({ user, body: groupData }) {
  // TODO: What would be the response of joining a blocked/removed group?

  let group = await this.findOneAndUpdate({
    title: groupData.title,
    'university.name': user.university.name
  }, {
    $addToSet: { 'university.domains': user.university.domain }
  }, { new: true });
  if (!group) {
    groupData.university = {
      name: user.university.name,
      domains: [user.university.domain]
    };
    group = await this.create(groupData);
  }

  let joinGroup = await mongoose.model('UserSeenGroup').findOne({
    userId: user._id,
    groupId: group._id,
  });

  if (!joinGroup) {
    joinGroup = await mongoose.model('UserSeenGroup').create({
      userId: user._id,
      groupId: group._id,
    });
    group.memberCount++;
    await group.save();
  }

  group._doc.groupId = group._doc._id;
  delete group._doc._id;
  group._doc.lastSeen = joinGroup._doc.lastSeen;

  return {
    data: group,
    message: 'you joined successfully'
  };
};

module.exports = mongoose.model('Group', GroupSchema);
