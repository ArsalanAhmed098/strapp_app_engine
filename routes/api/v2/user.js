const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { UserController, NotificationController } = require('../../../controllers/v2');
const { userAccess, upload } = require('../../../middleware');
const deeplink = require('node-deeplink');

router.route('/verify/:verifyCode')
  .get(UserController.verifyUser);

router.route('/deeplink')
  .get(
    deeplink({
      fallback: 'https://example.com',
      android_package_name: 'com.trillapp',
      ios_store_link: 'https://itunes.apple.com/us/app/cups-unlimited-coffee/id556462755?mt=8&uo=4'
    })
  );

router.use(passport.authenticate('student-bearer', { session: false }), userAccess);

router.route('/')
  .get(UserController.get);

router.route('/photo')
  .put(
    (req, res, next) => upload('profiles').single('photo')(req, res, next),
    UserController.update.photo
  );
router.route('/password')
  .put(UserController.update.password);
router.route('/info')
  .put(UserController.update.info);

router.route('/send-verification')
  .get(UserController.sendVerification);

router.route('/notifications')
  .get(NotificationController.getAll);
router.route('/notifications/firebase')
  .post(NotificationController.registerfirebaseToken)
  .get(NotificationController.getfirebaseTokens);
router.route('/notifications/seen')
  .patch(NotificationController.updateSeen);
router.route('/notifications/state')
  .patch(NotificationController.updateState);

router.route('/:userId')
  .get(UserController.getUserById);

router.route('/:userId/report')
  .patch(UserController.report);

module.exports = router;


