const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
const { CourseController, SectionController } = require('../../../controllers/v3');
const { userAccessV3 } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);

router.route('/')
  .post(CourseController.add)
  .get(CourseController.getAll);

router.route('/:courseName/sections')
  .post(SectionController.add)
  .get(SectionController.getAllByCourse);

module.exports = router;
