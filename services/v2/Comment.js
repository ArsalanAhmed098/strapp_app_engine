const mongoose = require('mongoose');
const { PostModel, StudentModel } = require('../../models/v2');
const User = require('./User');

const add = async (user, postId, commentData) => {
  commentData._id = mongoose.Types.ObjectId();
  commentData.userId = user._id;

  const post = await PostModel.findOne({
    _id: postId,
    isDeleted: false
  });
  if (!post)
    throwError('POST_NOT_FOUND');

  let parentComment = null;
  if (commentData.parentId) {
    parentComment = post.comments.find(({ _id, isDeleted }) => _id.toString() === commentData.parentId.toString() && !isDeleted);
    if (!parentComment)
      throwError('COMMENT_NOT_FOUND');
    parentComment.children.push(commentData._id);
  }

  post.comments.push(commentData);
  await post.save();

  let userId = null;
  const notificationPayload = {
    sender: user,
    notification: {
      body: commentData.description
    },
    metadata: {
      postId: post._id,
      commentId: commentData._id,
    }
  };
  if (parentComment) {
    userId = parentComment.userId;
    notificationPayload.notification.title = 'There is a new reply';
  } else {
    userId = post.userId;
    notificationPayload.notification.title = 'There is a new comment';
  }

  if (userId.toString() !== user._id.toString()) {
    const user = await StudentModel.findById(userId);
    User.pushNotification(user, notificationPayload);
  }

  commentData.currentUserLiked = 0;
  commentData.likes = 0;
  commentData.commentId = commentData._id;
  delete commentData._id;

  return commentData;
};

const getAll = async (user, postId) => {
  let post = await PostModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(postId),
        isDeleted: false
      }
    },
    {
      $lookup: {
        from: 'students',
        localField: 'comments.userId',
        foreignField: '_id',
        as: 'commentedUsers'
      }
    },
    {
      $project: {
        // postId: '$_id',
        // _id: false,
        // user: true,
        // likes: true,
        // dislikes: true,
        // isRestricted: true,
        // isAnonymous: true,
        // content: true,
        // topics: true,
        comments: project(),
        commentedUsers: true,
        // commentCount: countProject(),
        // createdAt: true,
      }
    }
  ]);
  if (!post.length)
    throwError('POST_NOT_FOUND');

  post = post[0];

  return post.comments.reduce(normalize(user, post), []);
};

const remove = async (user, postId, commentId) => {
  const post = await PostModel.findByIdAndUpdate(
    postId,
    {
      $set: { 'comments.$[elem].isDeleted': true }
    },
    {
      arrayFilters: [{ 'elem._id': commentId, 'elem.userId': user._id }],
      new: true
    }
  );
  if (!post)
    throwError('POST_NOT_FOUND');

  const comment = post.comments.find(comment => commentId.toString() === comment._id.toString());
  if (!comment || !comment.isDeleted)
    throwError('COMMENT_NOT_FOUND');
};

const report = async (user, postId, commentId, reasons) => {
  const post = await PostModel.findById(postId);
  if (!post)
    throwError('POST_NOT_FOUND');

  let commentFound = false;
  post.comments.forEach(comment => {
    if (comment._id.toString() === commentId.toString()) {
      commentFound = true;
      reasons.forEach(reason => {
        if (!comment.reports[reason])
          reason = 'other';
        if (!comment.reports[reason].find(userId => userId.toString() === user._id.toString()))
          comment.reports[reason].push(user._id);
      });
    }
  });

  if (!commentFound)
    throwError('COMMENT_NOT_FOUND');

  await post.save();
};

const likeOperation = likeType => async (user, postId, commentId) => {
  const post = await PostModel.findByIdAndUpdate(
    postId,
    (() => {
      switch (likeType) {
        case 'like':
          return {
            $pull: { 'comments.$[elem].dislikes': user._id },
            $addToSet: { 'comments.$[elem].likes': user._id }
          };
        case 'unlike':
          return {
            $pull: { 'comments.$[elem].likes': user._id },
          };
        case 'dislike':
          return {
            $pull: { 'comments.$[elem].likes': user._id },
            $addToSet: { 'comments.$[elem].dislikes': user._id }
          };
        case 'undislike':
          return {
            $pull: { 'comments.$[elem].dislikes': user._id },
          };
      }
    })(),
    {
      arrayFilters: [{ 'elem._id': commentId }],
      new: true
    }
  );
  if (!post)
    throwError('POST_NOT_FOUND');

  const comment = post.comments.find(comment => commentId.toString() === comment._id.toString())._doc;
  if (!comment)
    throwError('COMMENT_NOT_FOUND');

  comment.commentId = comment._id;
  comment.currentUserLiked =
    (comment.likes.find(like => like.toString() === user._id.toString()) && 1) ||
    (comment.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
    0;
  comment.likes = comment.likes.length - comment.dislikes.length;
  delete comment.dislikes;
  delete comment._id;
  delete comment.isAnonymous;
  delete comment.userId;
  delete comment.description;
  delete comment.reports;
  delete comment.children;
  delete comment.isDeleted;
  delete comment.updatedAt;
  delete comment.createdAt;

  return comment;
};

const like = likeOperation('like');

const unlike = likeOperation('unlike');

const dislike = likeOperation('dislike');

const undislike = likeOperation('undislike');

// ========== Utilities =============
const normalize = (user, post) => (acc, comment) => {
  comment.commentId = comment._id;
  delete comment._id;

  comment.currentUserLiked =
    (comment.likes.find(like => like.toString() === user._id.toString()) && 1) ||
    (comment.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
    0;
  comment.likes = comment.likes.length - comment.dislikes.length;
  delete comment.dislikes;

  if (
    comment.isRestricted &&
    comment.userId.toString() !== user._id.toString() &&
    user.verification.status !== 'VERIFIED_STUDENT'
  ) {
    comment = user.verification.status === 'UNVERIFIED_ID' ?
      {
        isRestricted: true,
        commentId: comment.commentId
      } :
      null;
  } else {
    const commentedUser = post.commentedUsers.find(user => user._id.toString() === comment.userId.toString());
    if ((comment.userId.toString() !== user._id.toString()) && comment.isAnonymous) {
      comment.user = null;
    } else {
      comment.user = {
        userId: commentedUser._id,
        verification: commentedUser.verification,
        firstName: commentedUser.firstName,
        lastName: commentedUser.lastName,
        photo: commentedUser.photo,
        major: commentedUser.major,
        lastName: commentedUser.lastName,
        email: commentedUser.email,
        username: commentedUser.username,
      };
    }
    comment.userVerification = commentedUser.verification;
  }
  if (comment && comment.userId) {
    delete comment.userId;
    delete comment.reports;
  }
  if(comment) acc.push(comment);
  return acc;
};

const project = () => ({
  $filter: {
    input: '$comments',
    as: 'comment',
    cond: {
      $ne: ['$$comment.isDeleted', true]
    }
  }
});

const countProject = () => ({
  $cond: {
    if: { $isArray: '$comments' }, then: {
      $size: {
        $filter: {
          input: '$comments',
          as: 'comment',
          cond: {
            $ne: ['$$comment.isDeleted', true]
          }
        }
      }
    }, else: 0
  }
});

module.exports = {
  add,
  getAll,
  remove,
  report,
  like,
  unlike,
  dislike,
  undislike,
  normalize,
  project,
  countProject,
};
