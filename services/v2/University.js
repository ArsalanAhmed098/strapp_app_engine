const { UniversityModel } = require('../../models/v2');

const getByDomain = domain =>
  UniversityModel.findOne({ domains: { $elemMatch: { $eq: domain } } });

const getAll = domain =>
  UniversityModel.getAll();

module.exports = {
  getByDomain,
  getAll
};
