const firebaseAdmin = require('firebase-admin');

const serviceAccount = require('../strapp-test-firebase-adminsdk-4191s-5d72ee1f78.json');

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: 'https://strapp-test.firebaseio.com'
});

module.exports = firebaseAdmin;
