const mongoose = require('mongoose');
const { SectionModel, UserSeenGroupModel, CampusGroupsModel } = require('../../models/v2');
const User = require('./User');
const ObjectId = mongoose.Types.ObjectId;

const add = async(user, courseName, sectionName, sectionNumber, sectionCategory) => {
    courseName = courseName.toUpperCase();
    sectionName = sectionName.toLowerCase();
    const course = await SectionModel.find({
        'course.name': courseName,
        'university.name': user.university.name,
    });
    if (!course.length)
        throwError('COURSE_NOT_FOUND');

    let section = course.find(({ section }) => section.name === sectionName);
    if (section) {
        // throwError('SECTION_EXISTS');
        return join(user, section._id);
    }

    section = await save({
        course: {
            name: courseName
        },
        section: {
            name: sectionName,
            number: sectionNumber,
            category: sectionCategory
        },
        memberCount: 1,
        university: {
            name: user.university.name,
            domains: [user.university.domain]
        }
    });

    let membership = await getMembership(user._id, section._id);

    if (!membership)
        membership = await addMembership(user._id, section._id);

    section._doc.groupId = section._id;
    section._doc.title = section.course.name;
    delete section._doc._id;

    return section;
};

const getAllByCourse = async(user, courseName) => {
    courseName = courseName.toUpperCase();
    const sections = await SectionModel.aggregate([{
            $match: {
                'course.name': courseName,
                'university.name': user.university.name,
                memberCount: { $gt: 0 },
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                let: { gId: '$_id' },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$groupId', '$$gId'] },
                                    { $eq: ['$userId', user._id] }
                                ]
                            }
                        }
                    },
                    // {
                    //   $project : { "createdAt": 1, "_id": 0}
                    // }
                ],
                as: 'membership'
            }
        },
        {
            $unwind: {
                path: '$membership',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id' },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$groupId', '$$gId'] },
                                    { $ne: ['$isDeleted', true] },
                                ]
                            }
                        }
                    },
                    {
                        $project: { "createdAt": 1, "_id": 0 }
                    }
                ],
                as: 'posts'
            }
        },
        {
            $project: project()
        }
    ]);
    // , { allowDiskUse: true }
    return sections;
};

const getAllSectionsByGroup = async(user, fullUrl, groupId) => {

    groupId = groupId;

    console.log('one', groupId);

    const campusGroupSections = await SectionModel.aggregate([

        {
            $match: {
                'campusgroupId': ObjectId(groupId),
                'university.name': user.university.name,
                'grouptype': 'campusgroup',
                'isDeleted': { $ne: true },
                memberCount: { $gt: 0 },
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                let: { gId: '$_id' },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$groupId', '$$gId'] },
                                { $eq: ['$userId', ObjectId(user._id)] }
                            ]
                        }
                    }
                }],
                as: 'membership'
            }
        },
        {
            $unwind: {
                path: '$membership',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id' },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$groupId', '$$gId'] },
                                { $ne: ['$isDeleted', true] },
                            ]
                        }
                    }
                }],
                as: 'posts'
            }
        },
        {
            $project: project1(fullUrl)
        },
    ]).allowDiskUse(true);
    // , { allowDiskUse: true }
    return campusGroupSections;
};


const getAll = async(user) => {
    const sections = await UserSeenGroupModel.aggregate([{
            $match: {
                userId: user._id,
            },
        },
        {
            $lookup: {
                from: "sections",
                let: { gId: "$groupId" },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $ne: ["$isDeleted", true] },
                                { $eq: ["$_id", "$$gId"] },
                                { $eq: ["$university.name", user.university.name] },
                                { $ne: ["$grouptype", "campusgroup"] },

                            ],
                        },
                    },
                }, ],
                as: "section",
            },
        },
        { $unwind: "$section" },
        {
            $lookup: {
                from: "posts",
                let: { gId: "$groupId" },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ["$groupId", "$$gId"] },
                                    { $ne: ["$isDeleted", true] },
                                ],
                            },
                        },
                    },
                    {
                        $project: { "createdAt": 1, "_id": 0 }
                    }
                ],
                as: "posts",
            },
        },
        {
            $project: {
                section: {
                    groupId: "$groupId",
                    section: true,
                    course: true,
                    title: "$section.course.name",
                    grouptype: "$section.grouptype",
                    university: true,
                    memberCount: true,
                    topics: true,
                    createdAt: true,
                    newPostCount: {
                        $size: {
                            $filter: {
                                input: "$posts",
                                as: "post",
                                cond: {
                                    $and: [{ $gt: ["$$post.createdAt", "$lastSeen"] }, ],
                                },
                            },
                        },
                    },
                },
            },
        },
        {
            $replaceRoot: { newRoot: "$section" },
        },
    ]).allowDiskUse(true);
    return sections;
};


const getAllCampusSections = async(user, fullUrl) => {

    console.log('zero', fullUrl);

    const campussections = await UserSeenGroupModel.aggregate([{
            $match: {
                userId: user._id,
            },
        },
        {
            $lookup: {
                from: "sections",
                let: { gId: "$groupId" },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $ne: ["$isDeleted", true] },
                                { $eq: ["$_id", "$$gId"] },
                                { $eq: ["$university.name", user.university.name] },
                                { $eq: ["$grouptype", "campusgroup"] },

                            ],
                        },
                    },
                }, ],
                as: "section",
            },
        },
        { $unwind: "$section" },
        {
            $lookup: {
                from: "posts",
                let: { gId: "$groupId" },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ["$groupId", "$$gId"] },
                                { $ne: ["$isDeleted", true] },
                            ],
                        },
                    },
                }, ],
                as: "posts",
            },
        },
        {
            $project: {
                section: {
                    groupId: "$groupId",
                    section: true,
                    course: true,
                    title: "$section.course.name",
                    grouptype: "$section.grouptype",
                    //  photo: { $concat: [ fullUrl,"$section.photo" ] },
                    photo: "$section.photo",
                    campusgroupId: true,
                    university: true,
                    memberCount: true,
                    topics: true,
                    createdAt: true,
                    newPostCount: {
                        $size: {
                            $filter: {
                                input: "$posts",
                                as: "post",
                                cond: {
                                    $and: [{ $gt: ["$$post.createdAt", "$lastSeen"] }, ],
                                },
                            },
                        },
                    },
                },
            },
        },
        {
            $replaceRoot: { newRoot: "$section" },
        },
    ]);
    return campussections;
};


//// old work category wise work///

// const getAllCampusSections = async (user,fullUrl) => {

//   console.log('zero',fullUrl);

//   const campussections = await UserSeenGroupModel.aggregate([
//     {
//       $match: {
//         userId: user._id,
//       },
//     },
//     {
//       $lookup: {
//         from: "sections",
//         let: { gId: "$groupId" },
//         pipeline: [
//           {
//             $match: {
//               $expr: {
//                 $and: [
//                   { $ne: ["$isDeleted", true] },
//                   { $eq: ["$_id", "$$gId"] },
//                   { $eq: ["$university.name", user.university.name] },
//                   { $eq: ["$grouptype", "campusgroup"] },

//                 ],
//               },
//             },
//           },
//         ],
//         as: "section",
//       },
//     },
//     { $unwind: "$section" },

//     // {
//     //   $lookup:
//     //     {
//     //       from: "campusgroups",
//     //       localField: "section.campusgroupId",
//     //       foreignField: "_id",
//     //       as: "campuscategory"
//     //     },
//     // },
//     // { $unwind: "$campuscategory" },
//     {
//       $lookup:
//          {
//            from: "campusgroups",
//            let: { section_campusgroupId: "$section.campusgroupId"},
//            pipeline: [
//               { $match:
//                  { $expr:
//                     { $and:
//                        [
//                          { $eq: [ "$_id",  "$$section_campusgroupId" ] },
//                          { $ne: ['$isDeleted', true] },
//                          { $eq: ["$grouptype", "campusgroup"] },
//                        ]
//                     }
//                  }
//               },
//            ],
//            as: "campuscategory"
//          }
//     },
//     { $unwind: "$campuscategory" },
//     // {
//     //   $lookup: {
//     //     from: "posts",
//     //     let: { gId: "$groupId" },
//     //     pipeline: [
//     //       {
//     //         $match: {
//     //           $expr: {
//     //             $and: [
//     //               { $eq: ["$groupId", "$$gId"] },
//     //               { $ne: ["$isDeleted", true] },
//     //             ],
//     //           },
//     //         },
//     //       },
//     //     ],
//     //     as: "posts",
//     //   },
//     // },
//     {
//       $project: {
//         campuscategory: {
//           campusCategoryId: "$campuscategory._id",
//           title: "$campuscategory.group.name",
//           grouptype: "$campuscategory.grouptype",
//           photo: { $concat: [ fullUrl,"$campuscategory.photo" ] },
//           section: {"name": "general"},
//           // newPostCount: {
//           //   $size: {
//           //     $filter: {
//           //       input: "$posts",
//           //       as: "post",
//           //       cond: {
//           //         $and: [{ $gt: ["$$post.createdAt", "$lastSeen"] },
//           //       ],
//           //       },
//           //     },
//           //   },
//           // },
//         },
//       },
//     },
//     {
//       $replaceRoot: { newRoot: "$campuscategory" },
//     },
//   ]);

//   var obj = [];

//   campussections.forEach(async(element) => {
//     console.log('ye rha',obj.find(o => o.campusCategoryId.toString() == element.campusCategoryId.toString()));  

//     const check = obj.find(o => o.campusCategoryId.toString() == element.campusCategoryId.toString())

//     if(check === undefined){

//     obj.push(element);
//     }

//     });

//     console.log('final',obj);

//   return obj;
// };




// const getAllCampusExplore = async (user,fullUrl) => {

//   console.log(fullUrl);

//   const campusExploreListing = await UserSeenGroupModel.aggregate([
//     {
//       $match: {
//         userId: user._id,
//       },
//     },
//     {
//       $lookup: {
//         from: "sections",
//         // let: { gId: "$groupId" },
//         pipeline: [
//           {
//             $match: {
//               $expr: {
//                 $and: [
//                   { $ne: ["$isDeleted", true] },
//                   // { $eq: ["$_id", "$$gId"] },
//                   { $eq: ["$university.name", user.university.name] },
//                   { $eq: ["$grouptype", "campusgroup"] },

//                 ],
//               },
//             },
//           },
//         ],
//         as: "section",
//       },
//     },
//     { $unwind: "$section" },
//     // {
//     //   $lookup: {
//     //     from: "posts",
//     //     let: { gId: "$groupId" },
//     //     pipeline: [
//     //       {
//     //         $match: {
//     //           $expr: {
//     //             $and: [
//     //               { $eq: ["$groupId", "$$gId"] },
//     //               { $ne: ["$isDeleted", true] },
//     //             ],
//     //           },
//     //         },
//     //       },
//     //     ],
//     //     as: "posts",
//     //   },
//     // },
//     {
//       $project: {
//         // section: {
//           // groupId: "$groupId",
//           // section: true,
//           // course: true,
//           _id:false,
//           title: "$section.course.name",
//           grouptype: "$section.grouptype",
//            photo: { $concat: [ fullUrl,"$section.photo" ] },
//           // photo: "$section.photo",
//           university: true,
//           // memberCount: true,
//           topics: true,
//           // createdAt: true,
//           // newPostCount: {
//           //   $size: {
//           //     $filter: {
//           //       input: "$posts",
//           //       as: "post",
//           //       cond: {
//           //         $and: [{ $gt: ["$$post.createdAt", "$lastSeen"] },
//           //       ],
//           //       },
//           //     },
//           //   },
//           // },
//         },
//       },
//     // },
//     // {
//     //   $replaceRoot: { newRoot: "$section" },
//     // },
//   ]).allowDiskUse(true);
//   return campusExploreListing;
// };


const getalljoinedgroups = async(user) => {

    // let user_id=[];
    let section_id = [];
    let id = user._id;
    let section = await UserSeenGroupModel.distinct('groupId,userId').find({
        'userId': mongoose.Types.ObjectId(id),
    }, { groupId: 1, contribs: 1 });

    if (!section)
        throwError('SECTIONS_NOT_FOUND');

    section.forEach(async(element) => {
        // user_id.push(element.userId);
        section_id.push(element.groupId);

        // console.log('user',element.userId,'sectionId',element.groupId);
    });


    return { 'group_id': section_id }

}

const get = async(user, sectionId) => {
    const section = await SectionModel.aggregate([{
            $match: {
                _id: mongoose.Types.ObjectId(sectionId),
            }
        },
        {
            $lookup: {
                from: 'userseengroups',
                let: { gId: '$_id' },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$groupId', '$$gId'] },
                                { $eq: ['$userId', user._id] }
                            ]
                        }
                    }
                }],
                as: 'membership'
            }
        },
        {
            $unwind: {
                path: '$membership',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'posts',
                let: { gId: '$_id' },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$groupId', '$$gId'] },
                                { $ne: ['$isDeleted', true] }
                            ]
                        }
                    }
                }],
                as: 'posts'
            }
        },
        {
            $project: project()
        }
    ]);
    // , { allowDiskUse: true }
    if (!section.length)
        throwError('SECTION_NOT_FOUND');
    return section[0];
};

const join = async(user, sectionId) => {
    let section = await SectionModel.findOne({
        _id: mongoose.Types.ObjectId(sectionId),
        'university.name': user.university.name
    });
    if (!section)
        throwError('SECTION_NOT_FOUND');

    let membership = await getMembership(user._id, sectionId);
    if (!membership) {
        await addMembership(user._id, sectionId, null);
        section.memberCount++;
        section = await section.save();
    }

    section._doc.groupId = section._id;
    section._doc.title = section.course.name;
    delete section._doc._id;

    return section;
};

const leave = async(user, sectionId) => {
    const section = await UserSeenGroupModel.deleteOne({
        userId: user._id,
        groupId: sectionId
    });
    if (!section.deletedCount)
        throwError('SECTION_NOT_FOUND');

    await SectionModel.findByIdAndUpdate(sectionId, {
        $inc: { memberCount: -1 }
    });
};

const joinCampusGroup = async(user, sectionId, campusgroupId) => {
    let section = await SectionModel.findOne({
        _id: mongoose.Types.ObjectId(sectionId),
        'university.name': user.university.name
    });
    if (!section)
        throwError('SECTIONS_NOT_FOUND');

    let membership = await getMembership(user._id, sectionId);
    if (!membership) {
        await addMembership(user._id, sectionId, campusgroupId);
        section.memberCount++;
        section = await section.save();
    }

    section._doc.groupId = section._id;
    section._doc.title = section.course.name;
    delete section._doc._id;

    return section;
};

const leaveCampusGroup = async(user, sectionId, campusgroupId) => {
    const section = await UserSeenGroupModel.deleteOne({
        userId: user._id,
        groupId: sectionId,
        // campusgroupId: campusgroupId
    });
    if (!section.deletedCount)
        throwError('SECTION_NOT_FOUND');

    await SectionModel.findByIdAndUpdate(sectionId, {
        $inc: { memberCount: -1 }
    });
};

const joinCampusGroupOnBoarding = async(user, campusGroupId) => {

    try {

        const section = await SectionModel.find({
            campusgroupId: mongoose.Types.ObjectId(campusGroupId),
            'university.name': user.university.name
        }, { _id: 1, contribs: 1, memberCount: 1, contribs: 1, campusgroupId: 1, contribs: 1 });

        if (!section)
            throwError('SECTIONS_NOT_FOUND');

        // let membership = await getMembership(user._id, sectionId);
        // if (!membership) {
        section.forEach(async(element) => {
            // console.log('ye rha',element._id);
            await addMembership(user._id, element._id, element.campusgroupId);
            element.memberCount++;
            element = await element.save();
        })
        return true;
    } catch (e) {
        return false;
    }

    // return true;
    // return section ;
    // }
    // section._doc.groupId = section._id;
    // section._doc.title = section.course.name;
    // delete section._doc._id;

};

const leaveCampusGroupOnBoarding = async(user, campusGroupId) => {

    try {
        const section = await SectionModel.find({
            campusgroupId: mongoose.Types.ObjectId(campusGroupId),
            'university.name': user.university.name
        }, { _id: 1, contribs: 1, memberCount: 1, contribs: 1, campusgroupId: 1, contribs: 1 });

        if (!section)
            throwError('SECTIONS_NOT_FOUND');

        section.forEach(async(element) => {
            // console.log('ye rha',element._id);
            var combination_delete = await UserSeenGroupModel.deleteOne({
                userId: user._id,
                groupId: element._id,
                campusgroupId: element.campusgroupId
            });

            if (combination_delete.deletedCount != 0) {

                console.log('============================', combination_delete);
                await SectionModel.findByIdAndUpdate(element._id, {
                    $inc: { memberCount: -1 }
                });
            }

        })

        return true;
    } catch (e) {
        return false;
    }

};

const joinDefaultCampusGroupOnBoarding = async(user, userId, email) => {

    try {
        const domain = email.split('@')[1];
        var campusGroup_Id = await CampusGroupsModel.findOne({
            'isDeleted': { $ne: true },
            'university.domains': domain,
            isDefault: true
        }, { _id: 1, contribs: 1 });

        // console.log('+++++++++++++++++++++1111',campusGroup_Id._id);
        // return campusGroupId;
        if (!campusGroup_Id)
            throwError('campusGroupId_NOT_FOUND');

        const section = await SectionModel.find({
            campusgroupId: mongoose.Types.ObjectId(campusGroup_Id._id),
            'university.name': user.university.name,
            _id: mongoose.Types.ObjectId('6026f2feeccae8001c4617a6')

        }, { _id: 1, contribs: 1, memberCount: 1, contribs: 1, campusgroupId: 1, contribs: 1 });

        if (!section)
            throwError('SECTIONS_NOT_FOUND');

        section.forEach(async(element) => {
            console.log('ye rha', element._id);
            await addMembership(userId, element._id, element.campusgroupId);
            element.memberCount++;
            element = await element.save();
        })
        return true;
    } catch (e) {
        return false;
    }
};

const updateSeen = async(user, sectionId) => {
    const section = await UserSeenGroupModel.findOneAndUpdate({
        userId: user._id,
        groupId: sectionId
    }, {
        lastSeen: Date.now()
    });
    if (!section)
        throwError('SECTION_NOT_FOUND');
};

const getAllMembers = async sectionId => {
    const users = await UserSeenGroupModel.aggregate([{
            $match: {
                groupId: ObjectId(sectionId),
            }
        },
        {
            $lookup: {
                from: 'students',
                let: { uId: '$userId' },
                pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$_id', '$$uId'] },
                                    { $ne: ['$isDeleted', true] }
                                ]
                            }
                        }
                    },
                    {
                        $project: User.project()
                    }
                ],
                as: 'user'
            }
        },
        { $unwind: '$user' },
        {
            $group: {
                _id: '$groupId',
                users: { $addToSet: '$user' }
            }
        }
    ]);
    // , { allowDiskUse: true }

    return users.length ? users[0].users : [];
};

// ========== Utilities =============
const save = SectionModel.create.bind(SectionModel);

const saveMembership = UserSeenGroupModel.create.bind(UserSeenGroupModel);

const addMembership = (userId, groupId, campusgroupId) => saveMembership({ userId, groupId, campusgroupId });

const getMembership = (userId, groupId) =>
    UserSeenGroupModel.findOne({ userId, groupId });

const project = () => (

    {
        _id: false,
        groupId: '$_id',
        title: '$course.name',
        // grouptype: '$grouptype',
        // photo: '$photo',
        // photo1: { $concat: [ fullUrl,'$photo' ] },
        // url: fullUrl,
        section: true,
        course: true,
        university: true,
        memberCount: true,
        topics: true,
        createdAt: true,
        postCount: {
            $cond: ['$membership',
                {
                    $size: {
                        $filter: {
                            input: '$posts',
                            as: 'post',
                            cond: {
                                $and: [
                                    { $gt: ['$$post.createdAt', '$membership.lastSeen'] }
                                ]
                            }
                        }
                    }
                },
                { $size: '$posts' }
            ]
        },
        newPostCount: {
            $cond: ['$membership',
                {
                    $size: {
                        $filter: {
                            input: '$posts',
                            as: 'post',
                            cond: {
                                $and: [
                                    { $gt: ['$$post.createdAt', '$membership.lastSeen'] }
                                ]
                            }
                        }
                    }
                },
                { $size: '$posts' }
            ]
        },
        currentUserJoined: {
            $cond: ['$membership', true, false]
        }
    });


const project1 = (fullUrl) => (

    console.log('two', fullUrl),

    {

        _id: false,
        // groupId: '$_id',
        // title: '$course.name',
        sectionId: '$_id',
        // grouptype: '$grouptype',
        description: '$group.description',
        grouptype: 'campussection',
        // photo: { $concat: [ fullUrl,'$photo' ] },
        photo: '$photo',
        isGeneral: true,
        section: true,
        course: true,
        university: true,
        memberCount: true,
        topics: true,
        createdAt: true,
        postCount: {
            $cond: ['$membership',
                {
                    $size: {
                        $filter: {
                            input: '$posts',
                            as: 'post',
                            cond: {
                                $and: [
                                    { $gt: ['$$post.createdAt', '$membership.lastSeen'] }
                                ]
                            }
                        }
                    }
                },
                { $size: '$posts' }
            ]
        },
        newPostCount: {
            $cond: ['$membership',
                {
                    $size: {
                        $filter: {
                            input: '$posts',
                            as: 'post',
                            cond: {
                                $and: [
                                    { $gt: ['$$post.createdAt', '$membership.lastSeen'] }
                                ]
                            }
                        }
                    }
                },
                { $size: '$posts' }
            ]
        },
        currentUserJoined: {
            $cond: ['$membership', true, false]
        }
    });


module.exports = {
    add,
    getAllByCourse,
    getAll,
    getAllCampusSections,
    get,
    join,
    joinCampusGroup,
    joinCampusGroupOnBoarding,
    joinDefaultCampusGroupOnBoarding,
    leaveCampusGroupOnBoarding,
    leave,
    leaveCampusGroup,
    updateSeen,
    getAllMembers,
    save,
    saveMembership,
    addMembership,
    getMembership,
    // getAllSectionsByExplore,
    // getAllCampusExplore
    getAllSectionsByGroup,
    getalljoinedgroups
};