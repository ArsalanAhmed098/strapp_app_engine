const asyncHandler            = require('../../util/async-handler');
const AuthController          = require('./AuthController');
const UserController          = require('./UserController');
const NotificationController  = require('./NotificationController');
const CourseController        = require('./CourseController');
const SectionController       = require('./SectionController');
const PostController          = require('./PostController');
const CommentController       = require('./CommentController');
const FeedController          = require('./FeedController');
const AdminController         = require('./AdminController');

module.exports = asyncHandler({
  AuthController,
  UserController,
  NotificationController,
  CourseController,
  SectionController,
  CommentController,
  PostController,
  FeedController,
  AdminController,
});
