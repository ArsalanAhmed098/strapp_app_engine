const firebaseAdmin = require('../conf/firebase');

const notificationHandler = registrationToken =>  (data, options = { tieToLive: 60 * 60 * 24 })  =>
  // console.log('datahai',data);
   firebaseAdmin.messaging().sendToDevice(registrationToken, data, options);


module.exports = notificationHandler;
