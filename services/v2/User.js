const fs = require('fs').promises;
const path = require('path');
const Notification = require('./Notification');
const uuid4 = require('uuid/v4');
const {
  sendMail,
  genExpire,
  notificationHandler,
  Password,
  verifyByUniversity,
} = require('../../util');
const { StudentModel } = require('../../models/v2');

const get = (user, currAuthToken) => {
  user.password = undefined;
  user.confirmation = undefined;
  user.notification = undefined;
  user.block = undefined;
  user.isDeleted = undefined;
  user.university = undefined;
  user.tokens = undefined;
  user._doc.token = currAuthToken;
  return user;
};

const getUserById = async userId => {
  const user = await StudentModel.findById(userId);
  if (!user)
    throwError('STUDENT_NOT_FOUND');
  user.token = undefined;
  user.createdAt = undefined;
  user.updatedAt = undefined;
  user._doc.userId = user._id;
  delete user._doc._id;
  return get(user);
};

const updatePhoto = async (user, file) => {
  if (!file)
    throwError('PHOTO_REQUIRED');
  try {
    const photoPath = path.join(require.main.filename, '../../', user.photo);
    await fs.unlink(photoPath);
  } catch (err) { console.log('unlink err:', err) }

  user.photo = `/${file.destination}/${file.filename}`;
  await user.save();
  return user.photo;
};

const updatePassword = async (user, oldPassword, newPassword, confirmNewPassword) => {
  if (newPassword !== confirmNewPassword)
    throwError('NEW_PASSWORDS_NOT_MATCH');

  const comparedResult = await Password.compare(oldPassword, user.password);
  if (!comparedResult)
    throwError('OLD_PASSWORD_INCORRECT');

  user.password = await Password.hash(newPassword);
  await user.save();
};

const updateInfo = async (user, firstName, lastName, major) => {
  firstName && (user.firstName = firstName);
  lastName && (user.lastName = lastName);
  major && (user.major = major);

  const newInfo = await user.save();
  const returnedData = {};

  firstName && (returnedData.firstName = newInfo.firstName);
  lastName && (returnedData.lastName = newInfo.lastName);
  major && (returnedData.major = newInfo.major);

  return returnedData;
};

const sendVerification = async (user, hostURL) => {
  if (user.verification.isVerified)
    throwError('USER_CONFIRMED');
  const confirmCode = user.confirmation.code || uuid4();
  const link = `${hostURL}/api/v2/user/verify/${confirmCode}`;
  user.confirmation = {
    code: confirmCode,
    expire: genExpire(5)
  };
  user = await user.save();
  const verifyMail = await sendMail({
    firstName: user.firstName,
    email: user.email,
    link
  });
  console.log('verifyMail state:', verifyMail);
  return verifyMail;
};

const verifyUser = async verifyCode => {
  let user = await StudentModel.findOne({ 'confirmation.code': verifyCode });

  try {
    if (!user)
      throwError('STUDENT_NOT_FOUND');

    if (!user.verification.isVerified) {
      if (user.confirmation.code !== verifyCode || user.confirmation.expire < Date.now())
        throwError('CODE_INCORRECT');

      user.verification.status = await verifyByUniversity(user);

      user.verification.isVerified = true;
      user.confirmation = undefined;
      if (user.block.status === 'UNVERIFIED') {
        user.block.status = 'NOT_BLOCKED';
        user.block.deniedRoutes = [];
      }
    }
  } catch (err) {
    console.log('student verification error:', err);
  } finally {
    return user && get(await user.save());
  }
};

const report = async (user, reportUserId, reasons) => {
  if (user._id.toString() === reportUserId)
    throwError('STUDENT_NOT_FOUND');

  const student = await StudentModel.findById(reportUserId);
  if (!student)
    throwError('STUDENT_NOT_FOUND');

  reasons.forEach(reason => {
    if (!student.reports[reason])
      reason = 'other';
    if (!student.reports[reason].find(userId => userId.toString() === user._id.toString()))
      student.reports[reason].push(user._id);
  });

  await student.save();
};

const registerfirebaseToken = async (user, firebaseToken) => {
  for (const token of user.tokens)
    if (token.authToken === user.currAuthToken)
      token.firebaseToken = firebaseToken;
  await user.save();
};

const getfirebaseTokens = async user => {
  return user.tokens.reduce((acc, { firebaseToken }) => {
    if (firebaseToken) acc.push(firebaseToken);
    return acc;
  }, []);
};

const updateNotificationSeen = async user => {
  user.notification.lastSeen = Date.now();
  await user.save();
};

const updateNotificationState = async (user, state) => {
  user.notification.state = state;
  await user.save();
};

const pushNotification = async (user, payload, options) => {
  const { sender, metadata } = payload;
  payload.data = {
    body: sender.photo || ''
  };
  delete payload.sender;
  delete payload.metadata;
  const notification = {
    receiver: user._id,
    sender: sender._id,
    metadata,
    payload: JSON.stringify(payload),
    firebaseResponses: [],
  };
  try {
    const firebaseTokens = user.tokens.reduce((acc, { firebaseToken }) => {
      firebaseToken && acc.push(firebaseToken);
      return acc;
    }, []);
    if (user.notification && user.notification.state && firebaseTokens.length) {
      for (const token of firebaseTokens) {
        const firebaseState = { token };
        const response = await notificationHandler(token)(payload, options);
        if (response.successCount) {
          firebaseState.status = true;
          firebaseState.messageId = response.results[0].messageId;
        } else {
          firebaseState.status = false;
          firebaseState.error = response.results[0].error;
        }
        firebaseState.multicastId = response.multicastId;
        notification.firebaseResponses.push(firebaseState);
      }
    }
  } catch (err) {
    notification.firebaseResponses.push({
      status: false,
      error: err,
    });
  }
  await Notification.save(notification);

  console.log('Push notification to:', user.email);
  console.log('Push notification status:', notification.status);

  return notification.status;
};

const hasAccess = (user, { path, method }) =>
  !(user.block.deniedRoutes && user.block.deniedRoutes.find(route => {
    if (route.method !== '*' && route.method !== method) return false;
    const path1 = route.path.split('/');
    const path2 = path.split('/');
    if (path1.length > path2.length) return false;
    for (let i = 0; i < path1.length; i++)
      if (path1[i] !== '*' && path1[i] !== path2[i]) return false;
    return true;
  })) || throwError(user.block.status === 'UNVERIFIED' ? 'UNVERIFIED' : 'FORBIDDEN');

const permission = async (user, status) => userBlock[status] && userBlock[status](user);

const userBlock = {
  ['NOT_BLOCKED'](user) {
    user.block.status = 'NOT_BLOCKED';
    user.block.deniedRoutes = [];
    return user.save();
  },
  ['WRITE_BLOCKED'](user) {
    user.block.status = 'WRITE_BLOCKED';
    user.block.deniedRoutes = [
      {
        path: 'sections/*/posts',
        method: 'POST'
      },
      {
        path: 'posts/*/comments',
        method: 'POST'
      }
    ];
    return user.save();
  },
  ['TOTAL_BLOCKED'](user) {
    user.block.status = 'TOTAL_BLOCKED';
    user.block.deniedRoutes = [
      {
        path: '*',
        method: '*'
      }
    ];
    return user.save();
  },
  ['UNVERIFIED'](user) {
    user.block.status = 'UNVERIFIED';
    user.block.deniedRoutes = [
      {
        path: 'sections/*/posts',
        method: 'POST'
      },
      {
        path: 'posts/*/comments',
        method: 'POST'
      }
    ];
    return user.save();
  },
};

const project = () => ({
  userId: '$_id',
  _id: false,
  firstName: true,
  lastName: true,
  photo: true,
  major: true,
  email: true,
  username: true,
  verification: true,
});

module.exports = {
  get,
  getUserById,

  updatePhoto,
  updatePassword,
  updateInfo,

  sendVerification,
  verifyUser,

  report,

  registerfirebaseToken,
  getfirebaseTokens,
  updateNotificationSeen,
  updateNotificationState,
  pushNotification,

  hasAccess,
  permission,
  project,
};
