require('dotenv').config();
require('../../conf/db');
// require('./import-universities');
const mongoose = require('mongoose');

const userData = require('./users');
const genPosts = require('./posts');
const { UserModel, GroupModel, UserSeenGroupModel, PostModel } = require('../../models/v1');

const insertGroups = async () => {
  const groupNames = [
    'EECS101',
    'MATH101',
    'ANTHRO101',
  ];
  const users = await UserModel.find();
  for (let index = 0; index < users.length; index++) {
    const user = users[index];
    if (index % 6) {
      const randGroups = groupNames.slice(0, 1 + Math.floor(Math.random() * groupNames.length));
      for (let gName of randGroups) {
        let groupData = {
          title: gName
        };
        let group = await GroupModel.findOneAndUpdate({
          title: groupData.title,
          'university.name': user.university.name
        }, {
          $addToSet: { 'university.domains': user.university.domain }
        }, { new: true });
        if (!group) {
          groupData.university = {
            name: user.university.name,
            domains: [user.university.domain]
          };
          group = await GroupModel.create(groupData);
        }

        let joinGroup = await UserSeenGroupModel.findOne({
          userId: user._id,
          groupId: group._id,
        });
        if (!joinGroup) {
          joinGroup = await UserSeenGroupModel.create({
            userId: user._id,
            groupId: group._id,
          });
          group.memberCount++;
          await group.save();
        }
      }
    }
  }
};

const insertPosts = async () => {
  const groups = await UserSeenGroupModel.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'userId',
        foreignField: '_id',
        as: 'users'
      }
    },
    { $unwind: '$users'},
    {
      $group: {
        _id: '$groupId',
        users: { $addToSet: '$users'}
      }
    },
  ]);

  for (let group of groups) {
    for(let user of group.users) {
      const posts = genPosts(user._id, group, 5);
      await PostModel.insertMany(posts);
    }
  }

};

(async () => {
  try {
    await mongoose.connection.collections.notifications.drop();
    console.log('====================> notifications collection is droped! <====================');
  } catch { }

  try {
    await mongoose.connection.collections.users.drop();
    console.log('====================> users collection is droped! <====================');
  } catch { }

  await UserModel.insertMany(userData);
  console.log('====================> insert new users! <====================');

  try {
    await mongoose.connection.collections.groups.drop();
    console.log('====================> groups collection is droped! <====================');
  } catch { }

  try {
    await mongoose.connection.collections.userseengroups.drop();
    console.log('====================> userseengroups collection is droped! <====================');
  } catch { }

  await insertGroups();
  console.log('====================> insert new groups! <====================');

  try {
    await mongoose.connection.collections.posts.drop();
    console.log('====================> posts collection is droped! <====================');
  } catch { }

  await insertPosts();
  console.log('====================> insert new posts! <====================');

  process.exit(0);
})();

