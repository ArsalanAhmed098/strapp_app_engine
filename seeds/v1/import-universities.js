const https = require('https');
require('dotenv').config();
require('../../conf/db');
const mongoose = require('mongoose');
const { UniversityModel } = require('../../models/v1');

const universityDomainsListURL = 'https://raw.githubusercontent.com/Hipo/university-domains-list/master/world_universities_and_domains.json';

const importUniversities = async data => {
  try {
    await mongoose.connection.collections.universities.drop();
    console.log('====================> universities collection is droped! <====================');
  } catch {}

  await UniversityModel.insertMany(data);
  console.log('====================> insert universities info! <====================');

};

https.get(universityDomainsListURL, res => {
  let body = '';
  res.on('data', function (chunk) {
    body += chunk;
  });
  res.on('end', async () => {
    try {
      const universityDomainsList = JSON.parse(body).map(({ name, domains }) => ({ name, domains }));
      console.log('====================> Fetch university domains list successfully <====================');

      await importUniversities(universityDomainsList);

      process.exit(0);

    } catch(err) {
      console.log(`University domains list error:`, err);
      process.exit(1);
    }
  });
}).on('error', err => {
  console.log(`Got an error to fetch university domains list:`, err);
  process.exit(1);
});
