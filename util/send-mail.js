const mg = require('../conf/mail');

const sendMail = async user => {
  const mailOptions = {
    from: '"Strapp" <no-reply@strapp.ai>',
    to: user.email,
    subject: 'Account verification',
    "o:tracking": 'False',
    html: `
      <center>
        <H2>Hi!</H2>
        <strong>
          Thanks so much for joining Strapp!
        </strong>
        <p>
          Just click <a href="${user.link}">here</a> to verify your account!
        </p>
      </center>
    `
  };
  try {
    const response = await mg.messages().send(mailOptions);
    console.log('========== email response:', response);
    return response;
  } catch(err) {
    console.log('send email err:', err);
  }
};

module.exports = sendMail;

