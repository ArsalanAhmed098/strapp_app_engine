const { User } = require('../../../services/v3');
const update = require('./update');
const confirmation = require('./confirmation');

const get = async (req, res) => {
  req.user._doc.notificationState =
    req.user.notification.state === undefined ? true : req.user.notification.state;
  res.success(User.get(req.user));
};

const checkVerification = async (req, res) => {
  res.success(req.user.verification.isVerified);
};

const getUserById = async (req, res) => {
  const user = await User.getUserById(req.params.userId);
  res.success(user);
};

const sendVerification = async (req, res) => {
  const hostURL = req.protocol + '://' + req.get('host');
  console.log('hostURL:', hostURL);
  await User.sendVerification(req.user, hostURL);
  res.success();
};

const verifyUserByEmail = async (req, res) => {
  let { email, status } = req.query;
  const validStatus = ['VERIFIED_STUDENT', 'UNIVERSITY_FACULTY', 'UNVERIFIED_ID'];
  if(validStatus.includes(status.toUpperCase()))
    status = status.toUpperCase();
  else
    status = 'VERIFIED_STUDENT';
  const user = await User.verifyUserByEmail(email, status);
  res.success(user);
};

const verifyUser = async (req, res) => {
  const user = await User.verifyUser(req.params.verifyCode);
  console.log('verfiy user:', user);
  res.redirect(`/api/v3/user/deeplink?url=strapp://open/${user ? user.verification.isVerified : false}`);
};

const report = async (req, res) => {
  await User.report(req.user, req.params.userId, req.body.reasons);
  res.success();
};

const block = async (req, res) => {
  await User.block(req.user, req.params.userId, req.body.isAnonymousBlock);
  res.success();
};

const unblock = async (req, res) => {
  await User.unblock(req.user, req.params.userId);
  res.success();
};

module.exports = {
  get,
  checkVerification,
  getUserById,
  sendVerification,
  verifyUserByEmail,
  verifyUser,
  report,
  block,
  unblock,
  update,
  confirmation,
};
