const { User } = require('../../../services/v2');

const confirm = async (req, res) => {
  const user = await User.confirm(req.user, req.body.code);
  res.success(user);
};

const resend = async (req, res) => {
  await User.resendConfirmCode(req.user);
  res.success();
};

module.exports = {
  confirm,
  resend
};
