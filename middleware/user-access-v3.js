const { User } = require('../services/v3');

const userAccess = (req, res, next) => {
  const path = req.originalUrl.replace('/api/v3/', '').split('?')[0];
  const method = req.method;
  User.hasAccess(req.user, { path, method });
  next();
};

module.exports = userAccess;
