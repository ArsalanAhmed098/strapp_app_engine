const mongoose = require('mongoose');
const { PostModel, SectionModel, UserSeenGroupModel } = require('../../models/v2');
const { getMembership } = require('./Section');
const User = require('./User');
const Comment = require('./Comment');

const add = async (user, postData) => {
  const { groupId } = postData;
  user._id, groupId
  const sectionEntity = await UserSeenGroupModel.aggregate([
    {
      $match: {
        groupId: mongoose.Types.ObjectId(groupId),
        userId: user._id
      }
    },
    {
      $lookup: {
        from: 'sections',
        localField: 'groupId',
        foreignField: '_id',
        as: 'section'
      }
    },
    {
      $unwind: '$section'
    }
  ]);

  if (!sectionEntity.length)
    throwError('SECTION_NOT_FOUND');

  const { course, section } = sectionEntity[0].section;

  const post = await save({ userId: user._id, ...postData });

  const members = await UserSeenGroupModel.aggregate([
    {
      $match: {
        groupId: mongoose.Types.ObjectId(groupId),
        userId: { $ne: user._id }
      }
    },
    {
      $lookup: {
        from: 'students',
        let: {
          uId: '$userId',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: (() => {
                  const query = [
                    { $eq: ['$_id', '$$uId'] }
                  ];
                  if (post.isRestricted) {
                    query.push({
                      $eq: ['$verification.status', 'VERIFIED_STUDENT']
                    })
                  }
                  return query;
                })()
              }
            }
          },
          {
            $lookup: {
              from: 'blockstudents',
              let: {
                postType: post.isAnonymous,
                sId: user._id
              },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $or: [
                        {
                          $and: [
                            {
                              $or: [
                                {
                                  $and: [
                                    { $eq: ['$blockerUser', '$$uId'] },
                                    { $eq: ['$blockedUser', '$$sId'] },
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ['$blockerUser', '$$sId'] },
                                    { $eq: ['$blockedUser', '$$uId'] },
                                  ]
                                }
                              ]
                            },
                            { $eq: ['$isAnonymousPost', false] },
                            { $eq: ['$$postType', false] },
                          ]
                        },
                        {
                          $and: [
                            { $eq: ['$blockerUser', '$$uId'] },
                            { $eq: ['$blockedUser', '$$sId'] },
                            { $eq: ['$isAnonymousPost', true] },
                            { $eq: ['$$postType', true] },
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              as: 'blockedUser'
            }
          },
          {
            $unwind: {
              path: '$blockedUser',
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $match: {
              'blockedUser': { '$exists': false },
            }
          },
        ],
        as: 'user'
      }
    },
    {
      $unwind: '$user'
    },
    {
      $replaceRoot: {
        newRoot: '$user'
      }
    },
  ]);

    const notificationPayload = {
    sender: post.isAnonymous ? null : user,
    notification: {
      title: `${course.name} • ${section.name[0].toUpperCase() + section.name.substr(1)}`,
      body: post.content.title
    },
    metadata: {
      groupId,
      postId: post._id,
    }
  };

  User.bulkPushNotification(members, notificationPayload);
  return normalize(user, post);
};

const get = async (user, postId) => {
  const post = await PostModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(postId),
        isDeleted: false
      }
    },
    {
      $lookup: {
        from: 'sections',
        localField: 'groupId',
        foreignField: '_id',
        as: 'group'
      }
    },
    { $unwind: '$group' },
    {
      $project: project()
    }
  ]);
  if (!post.length)
    throwError('POST_NOT_FOUND');

  return normalize(user, post[0]);
};

const getAll = async (user, sectionId, query) => {
  const parseQuery = query => {
    const normalizeQuery = Object.entries(query).map(([key, value]) => `${key}=${value}`);
    const parsedQuery = normalizeQuery.reduce((acc, curr) => {
      if (curr.startsWith('read')) {
        const { limit = -1, skip = 0 } = Object.fromEntries(curr
          .split('read')[1]
          .replace(/(\]|\[)/g, '')
          .split(',')
          .reduce((acc, curr) => {
            const [key, value] = curr.split('=');
            acc.push([key, +value]);
            return acc;
          }, []));
        acc.read = { limit, skip };
      } else if (curr.startsWith('unread')) {
        const { limit = -1, skip = 0 } = Object.fromEntries(curr
          .split('unread')[1]
          .replace(/(\]|\[)/g, '')
          .split(',')
          .reduce((acc, curr) => {
            const [key, value] = curr.split('=');
            acc.push([key, +value]);
            return acc;
          }, []));
        acc.unread = { limit, skip };
      } else if (curr.startsWith('topic')) {
        acc.topic = curr.split('=')[1];
      }
      return acc;
    }, {
      read: {
        limit: -1,
        skip: 0
      },
      unread: {
        limit: -1,
        skip: 0
      },
      topic: '',
    });
    return parsedQuery;
  };

  const genPostPipeline = filterQuery => postType => {
    const pipeline = [ 
      {
        $sort: { createdAt: postType === 'read' ? -1 : 1 }
      },
      {
        $lookup: {
          from: 'blockstudents',
          let: {
            postUserId: '$userId',
            postType: '$isAnonymous',
            postCreatedAt: '$createdAt',
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $or: [
                    {
                      $and: [
                        { $eq: ['$blockerUser', user._id] },
                        { $eq: ['$blockedUser', '$$postUserId'] },
                        { $eq: ['$isAnonymousPost', '$$postType'] },
                        { $lt: ['$blockDate', '$$postCreatedAt'] },
                      ]
                    },
                    {
                      $and: [
                        { $eq: ['$blockedUser', user._id] },
                        { $eq: ['$blockerUser', '$$postUserId'] },
                        { $eq: ['$$postType', false] },
                        { $eq: ['$isAnonymousPost', false] },
                        { $lt: ['$blockDate', '$$postCreatedAt'] },
                      ]
                    }
                  ]
                }
              }
            }
          ],
          as: 'blockedPost'
        }
      },
      {
        $unwind: {
          path: '$blockedPost',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $match: {
          'blockedPost': { '$exists': false },
          $expr: {
            $and: (() => {
              const query = [
                { $eq: ['$groupId', '$$gId'] },
                { $ne: ['$isDeleted', true] },
                postType === 'read' ? { $lte: ['$createdAt', '$$lastSeen'] } : { $gt: ['$createdAt', '$$lastSeen'] }
              ];
              if (user.verification.status === 'UNIVERSITY_FACULTY') {
                query.push({
                  $or: [
                    {
                      $eq: ['$isRestricted', false]
                    },
                    {
                      $and: [
                        { $eq: ['$isRestricted', true] },
                        { $eq: ['$userId', user._id] },
                      ]
                    },
                  ]
                })
              }
              return query;
            })()
          }
        }
      },
    ];
    filterQuery[postType].skip > 0 && pipeline.push({ $skip: filterQuery[postType].skip });
    filterQuery[postType].limit > 0 && pipeline.push({ $limit: filterQuery[postType].limit });
    pipeline.push(
      {
        $lookup: {
          from: 'students',
          let: {
            uId: '$userId',
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ['$_id', '$$uId']
                }
              }
            },
            {
              $project: User.project()
            }
          ],
          as: 'user'
        }
      },
      { $unwind: '$user' },
      {
        $lookup: {
          from: 'students',
          localField: "comments.userId",
          foreignField: "_id",
          as: 'commentedUsers'
        }
      },
      {
        $lookup: {
          from: 'blockstudents',
          localField: 'comments.userId',
          foreignField: 'blockedUser',
          as: 'blockedCommentedUsers'
        }
      },
      {
        $lookup: {
          from: 'blockstudents',
          localField: 'comments.userId',
          foreignField: 'blockerUser',
          as: 'blockerCommentedUsers'
        }
      },
      {
        $project: {
          postId: '$_id',
          _id: false,
          userId: true,
          user: true,
          likes: true,
          dislikes: true,
          isRestricted: true,
          isAnonymous: true,
          content: true,
          topics: true,
          comments: Comment.project(),
          commentedUsers: true,
          commentCount: Comment.countProject(),
          blockedCommentedUsers: true,
          blockerCommentedUsers: true,
          createdAt: true,
        }
      }
    );

    return filterQuery[postType].limit ?
      pipeline : [{
        $match: {
          $expr: false
        }
      }];
  };

  const postPipeline = genPostPipeline(parseQuery(query));
  let section = await SectionModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(sectionId),
        isBlocked: false,
        isDeleted: false
      }
    },
    {
      $lookup: {
        from: 'userseengroups',
        let: { gId: '$_id' },
        pipeline: [{
          $match: {
            $expr: {
              $and: [
                { $eq: ['$groupId', '$$gId'] },
                { $eq: ['$userId', user._id] }
              ]
            }
          }
        }],
        as: 'userseengroups'
      }
    },
    { $unwind: '$userseengroups' },
    {
      $lookup: {
        from: 'posts',
        let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
        pipeline: postPipeline('read'),
        as: 'reads'
      }
    },
    {
      $lookup: {
        from: 'posts',
        let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
        pipeline: postPipeline('unread'),
        as: 'unreads'
      }
    },
    {
      $lookup: {
        from: 'posts',
        let: { gId: '$_id', lastSeen: '$userseengroups.lastSeen' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$groupId', '$$gId'] },
                  { $ne: ['$isDeleted', true] }
                ]
              }
            }
          },
          {
            $group: {
              _id: { $cond: { if: { $gt: ['$createdAt', '$$lastSeen'] }, then: 'unread', else: 'read' } },
              postCount: { $sum: 1 }
            }
          }
        ],
        as: 'totalPosts'
      }
    },
    {
      $project: {
        _id: false,
        'groupId': '$_id',
        title: '$course.name',
        memberCount: true,
        university: '$university.name',
        topics: true,
        course: true,
        section: true,
        'lastSeen': '$userseengroups.lastSeen',
        readPosts: {
          posts: '$reads',
          count: {
            '$filter': {
              input: '$totalPosts',
              as: 'elem',
              cond: { $eq: ['$$elem._id', 'read'] }
            }
          }
        },
        unreadPosts: {
          posts: '$unreads',
          count: {
            '$filter': {
              input: '$totalPosts',
              as: 'elem',
              cond: { $eq: ['$$elem._id', 'unread'] }
            }
          }
        },
      }
    }
  ]);



  if (!section.length)
    throwError('SECTION_NOT_FOUND');

  section = section[0];
  const modifePostFields = (acc, post) => {
    if (
      post.isRestricted &&
      post.user.userId.toString() !== user._id.toString() &&
      user.verification.status !== 'VERIFIED_STUDENT'
    ) {
      post = user.verification.status === 'UNVERIFIED_ID' ?
        {
          isRestricted: true,
          postId: post.postId
        } :
        null;
    } else {
      post.userVerification = post.user.verification;
      if (post.user.userId.toString() !== user._id.toString() && post.isAnonymous) {
        post.user = null;
      }
    }
    if (post && post.likes) {
      post.currentUserLiked =
        (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
        (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
        0;
      post.likes = post.likes.length - post.dislikes.length;
      delete post.dislikes;
    }
    if (post && post.comments) {
      post.comments = post.comments
        .filter(comment => !comment.parentId)
        .reduce(Comment.normalize(user, post), [])
        .sort((ca, cb) => cb.createdAt - ca.createdAt)
        .slice(0, 2);
      delete post.commentedUsers;
      delete post.blockedCommentedUsers;
      delete post.blockerCommentedUsers;
    }
    if (post) acc.push(post);
    return acc;
  };
  section.readPosts.count = section.readPosts.count.length && section.readPosts.count[0].postCount;
  section.unreadPosts.count = section.unreadPosts.count.length && section.unreadPosts.count[0].postCount;
  section.readPosts.posts = section.readPosts.count ? section.readPosts.posts.reduce(modifePostFields, []) : [];
  section.unreadPosts.posts = section.unreadPosts.count ? section.unreadPosts.posts.reduce(modifePostFields, []) : [];

  return section;
};

const getAllByTopic = async (user, sectionId, query) => {
  const limit = +query.limit > 0 ? +query.limit : 20;
  const skip = +query.skip > 0 ? +query.skip : 0;
  const topics = query.topics.split(',').map(topic => topic.trim());
  const matchQuery = (() => ([
    {
      $lookup: {
        from: 'blockstudents',
        let: {
          postUserId: '$userId',
          postType: '$isAnonymous',
          postCreatedAt: '$createdAt',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $or: [
                  {
                    $and: [
                      { $eq: ['$blockerUser', user._id] },
                      { $eq: ['$blockedUser', '$$postUserId'] },
                      { $eq: ['$isAnonymousPost', '$$postType'] },
                      { $lt: ['$blockDate', '$$postCreatedAt'] },
                    ]
                  },
                  {
                    $and: [
                      { $eq: ['$blockedUser', user._id] },
                      { $eq: ['$blockerUser', '$$postUserId'] },
                      { $eq: ['$$postType', false] },
                      { $eq: ['$isAnonymousPost', false] },
                      { $lt: ['$blockDate', '$$postCreatedAt'] },
                    ]
                  }
                ]
              }
            }
          }
        ],
        as: 'blockedPost'
      }
    },
    {
      $unwind: {
        path: '$blockedPost',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $match: {
        'blockedPost': { '$exists': false },
        $and: (() => {
          const query = [
            { isDeleted: { $ne: true } },
            { groupId: mongoose.Types.ObjectId(sectionId) },
            { topics: { $exists: true } },
            { topics: { $all: topics } },
          ];
          if (user.verification.status === 'UNIVERSITY_FACULTY') {
            query.push({
              $or: [
                {
                  isRestricted: false
                },
                {
                  $and: [
                    { isRestricted: true },
                    { userId: user._id }
                  ]
                },
              ]
            })
          }
          return query;
        })()
      }
    },
  ]))
  const count = (await PostModel.aggregate(matchQuery())).length;

  let posts = await PostModel.aggregate([
    ...matchQuery(),
    {
      $sort: { _id: -1 }
    },
    {
      $skip: skip
    },
    {
      $limit: limit
    },
    {
      $lookup: {
        from: 'students',
        let: {
          uId: '$userId',
        },
        pipeline: [{
          $match: {
            $expr: {
              $eq: ['$_id', '$$uId']
            }
          }
        },
        {
          $project: User.project(),
        }
        ],
        as: 'user'
      }
    },
    { $unwind: '$user' },
    {
      $lookup: {
        from: 'students',
        localField: "comments.userId",
        foreignField: "_id",
        as: 'commentedUsers'
      }
    },
    {
      $lookup: {
        from: 'blockstudents',
        localField: 'comments.userId',
        foreignField: 'blockedUser',
        as: 'blockedCommentedUsers'
      }
    },
    {
      $lookup: {
        from: 'blockstudents',
        localField: 'comments.userId',
        foreignField: 'blockerUser',
        as: 'blockerCommentedUsers'
      }
    },
    {
      $project: {
        postId: '$_id',
        _id: false,
        userId: true,
        user: true,
        likes: true,
        dislikes: true,
        isRestricted: true,
        isAnonymous: true,
        content: true,
        topics: true,
        comments: Comment.project(),
        commentedUsers: true,
        commentCount: Comment.countProject(),
        blockedCommentedUsers: true,
        blockerCommentedUsers: true,
        createdAt: true,
      }
    },
  ]);
  posts = posts.reduce((acc, post) => {
    if (
      post.isRestricted &&
      post.user.userId.toString() !== user._id.toString() &&
      user.verification.status !== 'VERIFIED_STUDENT'
    ) {
      post = user.verification.status === 'UNVERIFIED_ID' ?
        {
          isRestricted: true,
          postId: post.postId
        } :
        null;
    } else {
      post.userVerification = post.user.verification;
      if (post.user.userId.toString() !== user._id.toString() && post.isAnonymous) {
        post.user = null;
      }
    }
    if (post && post.likes) {
      post.currentUserLiked =
        (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
        (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
        0;
      post.likes = post.likes.length - post.dislikes.length;
      delete post.dislikes;
    }
    if (post && post.comments) {
      post.comments = post.comments
        .filter(comment => !comment.parentId)
        .reduce(Comment.normalize(user, post), [])
        .sort((ca, cb) => cb.createdAt - ca.createdAt)
        .slice(0, 2);
      delete post.commentedUsers;
      delete post.blockedCommentedUsers;
      delete post.blockerCommentedUsers;
    }
    if (post) acc.push(post);
    return acc;
  }, []);

  return {
    readPosts: {
      posts,
      count
    }
  };
};

const update = async (user, postId, postData) => {
  const post = await PostModel.findOneAndUpdate({
    userId: user._id,
    _id: postId,
  }, postData, { new: true });
  if (!post)
    throwError('POST_NOT_FOUND');

  post._doc.postId = post._doc._id;
  delete post._doc._id;
  post._doc.currentUserLiked =
    (post._doc.likes.find(like => like.toString() === user._id.toString()) && 1) ||
    (post._doc.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
    0;
  post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
  delete post._doc.dislikes;
  delete post._doc.comments;

  return post;
};

const remove = async (user, postId) => {
  const post = await PostModel.findOneAndUpdate({
    userId: user._id,
    _id: postId,
  }, {
    isDeleted: true
  });
  if (!post)
    throwError('POST_NOT_FOUND');
};

const report = async (user, postId, reasons) => {
  const post = await PostModel.findById(postId);
  if (!post)
    throwError('POST_NOT_FOUND');

  reasons.forEach(reason => {
    if (!post.reports[reason])
      reason = 'other';
    if (!post.reports[reason].find(userId => userId.toString() === user._id.toString()))
      post.reports[reason].push(user._id);
  });

  await post.save();
};

const likeOperation = likeType => async (user, postId) => {
  const post = await PostModel.findByIdAndUpdate(postId, (() => {
    switch (likeType) {
      case 'like':
        return {
          $pull: { dislikes: user._id },
          $addToSet: { likes: user._id }
        };
      case 'unlike':
        return {
          $pull: { likes: user._id },
        };
      case 'dislike':
        return {
          $pull: { likes: user._id },
          $addToSet: { dislikes: user._id },
        };
      case 'undislike':
        return {
          $pull: { dislikes: user._id },
        };
    }
  })(),
    { new: true }).select('likes dislikes');
  if (!post)
    throwError('POST_NOT_FOUND');

  post._doc.postId = post._doc._id;
  delete post._doc._id;
  post._doc.currentUserLiked =
    (post._doc.likes.find(like => like.toString() === user._id.toString()) && 1) ||
    (post._doc.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
    0;
  post._doc.likes = post._doc.likes.length - post._doc.dislikes.length;
  delete post._doc.dislikes;

  return post;
};

const like = likeOperation('like');

const unlike = likeOperation('unlike');

const dislike = likeOperation('dislike');

const undislike = likeOperation('undislike');

// ========== Utilities =============
const save = PostModel.create.bind(PostModel);

const normalize = (user, post) => {
  if (post.isRestricted && post.userId.toString() !== user._id.toString() && !user.verification.isVerified) {
    post = {
      isRestricted: post.isRestricted,
      postId: post.postId
    }
  } else {
    post.currentUserLiked =
      (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
      (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
      0;
    if (post._doc) {
      post._doc.likes = post.likes.length - post.dislikes.length;
      delete post._doc.dislikes;
    } else {
      post.likes = post.likes.length - post.dislikes.length;
      delete post.dislikes;
    }
    if (post._id) {
      post._doc.postId = post._doc._id;
      delete post._doc._id;
    }
    // if (post.groupId) {
    //   post._doc.sectionId = post._doc.groupId;
    //   delete post._doc.groupId;
    // }
    if (post.reports)
      delete post._doc.reports;
  }
  return post;
};

const project = () => ({
  _id: false,
  postId: '$_id',
  likes: true,
  dislikes: true,
  isRestricted: true,
  isAnonymous: true,
  userId: true,
  topics: true,
  groupId: '$group._id',
  section: '$group.section',
  course: {
    name: '$group.course.name'
  },
  content: true,
  commentCount: Comment.countProject(),
  createdAt: true,
});

module.exports = {
  add,
  get,
  getAll,
  getAllByTopic,
  update,
  remove,
  report,
  like,
  unlike,
  dislike,
  undislike,
  save,
};
