const mongoose = require('mongoose');
const moment = require('moment');
const { UserModel, GroupModel, UserSeenGroupModel, PostModel } = require('../../../models/v1');
const { pipeline } = require('nodemailer/lib/xoauth2');

const loginPage = async (req, res) => {
  res.render('pages/login');
};

const homePage = async (req, res) => {
  res.redirect('/admins/students');
};

const studentsPage = async (req, res) => {
  const schoolQuery = (req.query.school || '').trim();
  const classQuery = (req.query.class || '').trim().toUpperCase();
  console.log('schoolQuery:', schoolQuery);
  console.log('classQuery:', classQuery);

  const schoolReg = new RegExp(schoolQuery, 'i');

  const pipeline = [
    {
      $match: {
        'university.name': schoolReg
      }
    },
    {
      $match: {
        $expr: {
          $ne: ['$isDeleted', true],
        }
      }
    },
  ];

  if (classQuery) {
    pipeline.push(
      {
        $lookup: {
          from: 'userseengroups',
          let: { uId: '$_id' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ['$userId', '$$uId']
                }
              }
            },
            {
              $lookup: {
                from: 'groups',
                let: { gId: '$groupId' },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          { $ne: ['$isDeleted', true] },
                          { $eq: ['$_id', '$$gId'] },
                          { $eq: ['$title', classQuery] }
                        ]
                      }
                    }
                  }
                ],
                as: 'group'
              }
            },
            { $unwind: '$group' }
          ],
          as: 'groupSeens'
        }
      },
      {
        $match: {
          groupSeens: { $not: { $size: 0 } }
        }
      },
    );
  }

  pipeline.push({
    $project: {
      firstName: true,
      lastName: true,
      photo: true,
      email: true,
      username: true,
      major: true,
      isVerified: true,
      'school': '$university.name',
      'blockStatus': '$block.status',
    }
  });

  let students = await UserModel.aggregate(pipeline);

  res.render('pages/students', {
    user: req.user,
    page: {
      title: 'Students',
      subtitle: 'Students Management'
    },
    sidebar: { active: 'students' },
    breadcrumb: {
      list: [
        { title: 'Home', link: '/admins' },
        { title: 'Students' },
      ]
    },
    list: students,
    filter: {
      schoolQuery,
      classQuery
    }
  });
};

const classesPage = async (req, res) => {
  const schoolQuery = (req.query.school || '').trim();
  const classQuery = (req.query.class || '').trim().toUpperCase();
  console.log('schoolQuery:', schoolQuery);
  console.log('classQuery:', classQuery);

  const schoolReg = new RegExp(schoolQuery, 'i');

  const classes = await GroupModel.aggregate([
    {
      $match: (() => {
        const matchQuery = {
          isDeleted: { $ne: true },
          'university.name': schoolReg,
        };
        if (classQuery)
          matchQuery.title = classQuery;
        return matchQuery;
      })()
    },
    {
      $lookup: {
        from: 'userseengroups',
        localField: '_id',
        foreignField: 'groupId',
        as: 'users'
      }
    },
    {
      $project: {
        title: true,
        school: '$university.name',
        memberCount: { $size: '$users' },
      }
    }
  ]);

  res.render('pages/classes', {
    user: req.user,
    page: {
      title: 'Classes',
      subtitle: 'Classes Management'
    },
    sidebar: { active: 'classes' },
    breadcrumb: {
      list: [
        { title: 'Home', link: '/admins' },
        { title: 'Classes' },
      ]
    },
    list: classes,
    filter: {
      schoolQuery,
      classQuery
    }
  });
};

const postsPage = async (req, res) => {
  const schoolQuery = (req.query.school || '').trim();
  const classQuery = (req.query.class || '').trim().toUpperCase();
  const postType = (req.query.type || 'all').trim().toLowerCase();
  console.log('schoolQuery:', schoolQuery);
  console.log('classQuery:', classQuery);
  console.log('postType:', postType);

  const schoolReg = new RegExp(schoolQuery, 'i');

  const pipeline = [];
  switch (postType) {
    case 'reported':
      pipeline.push(
        {
          $match: {
            $or: [
              { 'reports.abuse': { $not: { $size: 0 } } },
              { 'reports.recism': { $not: { $size: 0 } } },
              { 'reports.cheating': { $not: { $size: 0 } } },
              { 'reports.bad language': { $not: { $size: 0 } } },
              { 'reports.spam': { $not: { $size: 0 } } },
              { 'reports.fruad': { $not: { $size: 0 } } },
              { 'reports.illegal activity': { $not: { $size: 0 } } },
              { 'reports.other': { $not: { $size: 0 } } },
            ]
          }
        }
      );
      break;
    case 'removed':
      pipeline.push(
        {
          $match: {
            isDeleted: { $eq: true }
          }
        }
      );
      break;
  }
  pipeline.push(
    { $sort: { createdAt: -1 } },
    {
      $lookup: {
        from: 'groups',
        let: { gId: '$groupId' },
        pipeline: [
          {
            $match: {
              'university.name': schoolReg,
            }
          },
          {
            $match: {
              $expr: (() => {
                let matchQuery = {
                  $eq: ['$_id', '$$gId']
                };
                if (classQuery) {
                  matchQuery = {
                    $and: [
                      matchQuery,
                      { $eq: ['$title', classQuery] }
                    ]
                  }
                }
                return matchQuery;
              })()
            }
          }
        ],
        as: 'group'
      }
    },
    { $unwind: '$group' },
    {
      $project: {
        content: true,
        group: true,
        isRestricted: true,
        isAnonymous: true,
        isDeleted: true,
        reports: {
          'abuse': { $size: '$reports.abuse' },
          'recism': { $size: '$reports.recism' },
          'cheating': { $size: '$reports.cheating' },
          'bad language': { $size: '$reports.bad language' },
          'spam': { $size: '$reports.spam' },
          'fruad': { $size: '$reports.fruad' },
          'illegal activity': { $size: '$reports.illegal activity' },
          'other': { $size: '$reports.other' },
        },
        reportsCount: {
          $sum: [
            { $size: '$reports.abuse' },
            { $size: '$reports.recism' },
            { $size: '$reports.cheating' },
            { $size: '$reports.bad language' },
            { $size: '$reports.spam' },
            { $size: '$reports.fruad' },
            { $size: '$reports.illegal activity' },
            { $size: '$reports.other' },
          ]
        },
        commentCount: { $size: '$comments' },
      }
    }
  );

  let posts = await PostModel.aggregate(pipeline);

  res.render('pages/posts', {
    user: req.user,
    page: {
      title: 'Posts',
      subtitle: 'Post list'
    },
    sidebar: { active: 'posts' },
    breadcrumb: {
      list: [
        { title: 'Home', link: '/admins' },
        { title: 'Posts', link: '/admins/Posts' },
      ]
    },
    list: posts,
    filter: {
      schoolQuery,
      classQuery,
      postType
    }
  });

};

const commentsPage = async (req, res) => {
  const commentType = (req.query.type || 'all').trim().toLowerCase();
  console.log('commentType:', commentType);

  const { postId } = req.params;

  const result = await PostModel.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(postId)
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'comments.userId',
        foreignField: '_id',
        as: 'users'
      }
    },
    {
      $project: {
        _id: false,
        comments: true,
        users: true,
      }
    },
  ]);

  const comments = result[0].comments
    .filter(comment => {
      switch(commentType) {
        case 'reported':
          return Object.entries(comment.reports).reduce((acc, [, value]) => acc + value.length, 0);
        case 'removed':
          return comment.isDeleted;
        default:
          return true;
      }
    })
    .map(comment => {
      comment.student = result[0].users.find(user => user._id.toString() === comment.userId.toString());
      comment.createdAt = moment(comment.createdAt).fromNow();
      let reportsCount = 0;
      comment.reports = Object.entries(comment.reports).reduce((acc, [key, value]) => {
        acc[key] = value.length;
        reportsCount += acc[key];
        return acc;
      }, {});
      comment.reportsCount = reportsCount;
      comment.postId = postId;
      return comment;
    });

  res.render('pages/comments', {
    user: req.user,
    page: {
      title: 'Comments',
      subtitle: 'Comment list'
    },
    sidebar: { active: 'posts' },
    breadcrumb: {
      list: [
        { title: 'Home', link: '/admins' },
        { title: 'Posts', link: '/admins/posts' },
        { title: 'Comments', link: '/admins/posts/comments' },
      ]
    },
    list: comments,
    filter: {
      commentType
    }
  });

};

module.exports = {
  loginPage,
  homePage,
  studentsPage,
  classesPage,
  postsPage,
  commentsPage,
};
