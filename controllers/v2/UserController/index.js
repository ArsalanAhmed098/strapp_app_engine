const { User } = require('../../../services/v2');
const update = require('./update');
const confirmation = require('./confirmation');

const get = async (req, res) => {
  req.user._doc.notificationState =
    req.user.notification.state === undefined ? true : req.user.notification.state;
  res.success(User.get(req.user));
};

const getUserById = async (req, res) => {
  const user = await User.getUserById(req.params.userId);
  res.success(user);
};

const sendVerification = async (req, res) => {
  const hostURL = req.protocol + '://' + req.get('host');
  console.log('hostURL:', hostURL);
  await User.sendVerification(req.user, hostURL);
  res.success();
};

const verifyUser = async (req, res) => {
  const user = await User.verifyUser(req.params.verifyCode);
  console.log('verfiy user:', user);
  res.redirect(`/api/v2/user/deeplink?url=strapp://open/${user ? user.verification.isVerified : false}`);
};

const report = async (req, res) => {
  await User.report(req.user, req.params.userId, req.body.reasons);
  res.success();
};

module.exports = {
  get,
  getUserById,
  sendVerification,
  verifyUser,
  report,
  update,
  confirmation,
};
