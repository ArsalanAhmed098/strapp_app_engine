const UserSeenGroupModel = require('./user-seen-group');
const AdminModel = require('./admin');
const NotificationModel = require('./notification');
const PostModel = require('./post');
const UniversityModel = require('./university');

module.exports = {
  UserSeenGroupModel,
  AdminModel,
  NotificationModel,
  PostModel,
  UniversityModel,
};
