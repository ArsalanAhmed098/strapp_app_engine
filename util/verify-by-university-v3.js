const request = require('./request');

const verifyByUniversity = async user => {
  const { person } = await getStudentInfo(user.email.split('@')[0]);
  // const { person } = await getStudentInfo('faiz');
  console.log('verifyByUniversity-v3:', person);
  if (person.errors) {
    throw new Error('UNVERIFIED_ID');
  }
  let status = 'UNIVERSITY_FACULTY';
  let affiliation = '';
  const { displayName, uniqname } = person;
  const firstName = displayName.split(' ')[0];
  const lastName = displayName.replace(firstName, '').trim();
  const username = uniqname;
  if (person.affiliation) {
    affiliation = Array.isArray(person.affiliation) ?
      person.affiliation.join(' ').toLowerCase() :
      person.affiliation.toLowerCase();
  }
  if (affiliation.includes('undergraduate') || affiliation.includes('student')) {
    const title = person.title ? person.title.toLowerCase() : '';
    if (
      !title.includes('graduate student instructor') &&
      !title.includes('professor') &&
      !title.includes('lecturer') &&
      !title.includes('instructor')
    )
      status = 'VERIFIED_STUDENT';
  }
  return {
    status,
    firstName,
    lastName,
    username,
  };
};

const getStudentInfo = async uniqueName => {
  const clientId = process.env.UNIVERSITY_CLIENT_ID;
  const token = await getAccessToken();
  const Authorization = `Bearer ${token}`;

  const options = {
    hostname: 'apigw.it.umich.edu',
    port: 443,
    path: `/um/MCommunity/People/${uniqueName}`,
    method: 'GET',
    headers: {
      'X-IBM-Client-Id': clientId,
      Authorization
    }
  };

  return request(options).then(JSON.parse);
};

const getAccessToken = async () => {
  if (!globalThis.UNIVERSITY_ACCESS_TOKEN || globalThis.UNIVERSITY_ACCESS_TOKEN.expiresIn <= Date.now())
    await setAccessToken();
  return globalThis.UNIVERSITY_ACCESS_TOKEN.token;
};

const setAccessToken = async () => {
  const clientId = process.env.UNIVERSITY_CLIENT_ID;
  const clientSecret = process.env.UNIVERSITY_CLIENT_SECRET;
  const token = Buffer.from(`${clientId}:${clientSecret}`).toString('base64');
  const Authorization = `Basic ${token}`;

  const options = {
    hostname: 'apigw.it.umich.edu',
    port: 443,
    path: '/um/inst/oauth2/token?grant_type=client_credentials&scope=mcommunity',
    method: 'POST',
    headers: { Authorization }
  };

  const response = await request(options).then(JSON.parse);
  globalThis.UNIVERSITY_ACCESS_TOKEN = {
    token: response.access_token,
    expiresIn: new Date(Date.now() + (55 * 60 * 1000))
  };
};

module.exports = verifyByUniversity;
