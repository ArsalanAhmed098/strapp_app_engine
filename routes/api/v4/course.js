const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
// const { CourseController, SectionController } = require('../../../controllers/v3');
const { CourseController, SectionController } = require('../../../controllers/v4');
const { userAccessV3 } = require('../../../middleware');

router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);

router.route('/')
  .post(CourseController.add)
  .get(CourseController.getAll);

router.route('/:courseName/sections')
  .post(SectionController.add)
  .get(SectionController.getAllByCourse);


  router.route('/:exploreName/exploresections')
  // .post(SectionController.add)
  .get(SectionController.getAllSectionsByExplore);

  router.route('/:groupId/groupsections')
  // .post(SectionController.add)
  .get(SectionController.getAllSectionsByGroup);
  

module.exports = router;
