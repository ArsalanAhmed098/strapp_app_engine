const { UserSeenGroupModel, BlockStudentModel } = require('../../models/v2');
const User = require('./User');
const Comment = require('./Comment');
const mongoose = require('mongoose');

const getAll = async (user, query) => {
  const limit = +query.limit > 0 ? +query.limit : 20;
  const skip = +query.skip > 0 ? +query.skip : 0;

  let feed = await UserSeenGroupModel.aggregate([
    {
      $match: {
        userId: user._id,
      }
    },
    {
      $lookup: {
        from: 'sections',
        let: {
          gId: '$groupId'
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$_id', '$$gId'] },
                  { $ne: ['$isDeleted', true] }
                ]
              },
            }
          },
        ],
        as: 'group'
      }
    },
    { $unwind: '$group' },
    {
      $group:
      {
        _id: '$userId',
        // groups: { $addToSet: { section: { sectionId: '$groupId', title: '$group.section.name' }, course: { name: '$group.course.name' } } }
        groups: { $addToSet: { groupId: '$groupId', title: '$group.course.name' } },
        sections: { $addToSet: { groupId: '$groupId', name: '$group.section.name', number: '$group.section.number', category: '$group.section.category' } },
        courses: { $addToSet: { groupId: '$groupId', name: '$group.course.name' } },
      }
    },
    {
      $lookup: {
        from: 'posts',
        let: {
          gs: '$groups',
          ss: '$sections',
          cs: '$courses',
          gIds: '$groups.groupId',
        },
        pipeline: [
          {
            $lookup: {
              from: 'blockstudents',
              let: {
                postUserId: '$userId',
                postType: '$isAnonymous',
                postCreatedAt: '$createdAt',
              },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $or: [
                        {
                          $and: [
                            { $eq: ['$blockerUser', user._id] },
                            { $eq: ['$blockedUser', '$$postUserId'] },
                            { $eq: ['$isAnonymousPost', '$$postType'] },
                            { $lt: ['$blockDate', '$$postCreatedAt'] },
                          ]
                        },
                        {
                          $and: [
                            { $eq: ['$blockedUser', user._id] },
                            { $eq: ['$blockerUser', '$$postUserId'] },
                            { $eq: ['$$postType', false] },
                            { $eq: ['$isAnonymousPost', false] },
                            { $lt: ['$blockDate', '$$postCreatedAt'] },
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              as: 'blockedPost'
            }
          },
          {
            $unwind: {
              path: '$blockedPost',
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $match: {
              'blockedPost': { '$exists': false },
              $expr: {
                $and: (() => {
                  const query = [
                    { $in: ['$groupId', '$$gIds'] },
                    { $ne: ['$isDeleted', true] },
                  ];
                  if (user.verification.status === 'UNIVERSITY_FACULTY') {
                    query.push({
                      $or: [
                        {
                          $eq: ['$isRestricted', false]
                        },
                        {
                          $and: [
                            { $eq: ['$isRestricted', true] },
                            { $eq: ['$userId', user._id] },
                          ]
                        },
                      ]
                    })
                  }
                  return query;
                })()
              }
            }
          },
          {
            $sort: { _id: -1 }
          },
          {
            $skip: skip
          },
          {
            $limit: limit
          },
          {
            $lookup: {
              from: 'students',
              let: {
                uId: '$userId',
              },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $eq: ['$_id', '$$uId']
                    }
                  }
                },
                {
                  $project: User.project()
                }
              ],
              as: 'user'
            }
          },
          {
            $lookup: {
              from: 'students',
              localField: 'comments.userId',
              foreignField: '_id',
              as: 'commentedUsers'
            }
          },
          {
            $lookup: {
              from: 'blockstudents',
              localField: 'comments.userId',
              foreignField: 'blockedUser',
              as: 'blockedCommentedUsers'
            }
          },
          {
            $lookup: {
              from: 'blockstudents',
              localField: 'comments.userId',
              foreignField: 'blockerUser',
              as: 'blockerCommentedUsers'
            }
          },
          {
            $project: {
              postId: '$_id',
              _id: false,
              likes: true,
              dislikes: true,
              isRestricted: true,
              isAnonymous: true,
              content: true,
              user: true,
              comments: Comment.project(),
              commentedUsers: true,
              commentCount: Comment.countProject(),
              blockedCommentedUsers: true,
              blockerCommentedUsers: true,
              createdAt: true,
              group: {
                $filter: {
                  input: '$$gs',
                  as: 'g',
                  cond: {
                    $eq: ['$$g.groupId', '$groupId']
                  }
                }
              },
              section: {
                $filter: {
                  input: '$$ss',
                  as: 's',
                  cond: {
                    $eq: ['$$s.groupId', '$groupId']
                  }
                }
              },
              course: {
                $filter: {
                  input: '$$cs',
                  as: 'c',
                  cond: {
                    $eq: ['$$c.groupId', '$groupId']
                  }
                }
              },
            }
          },
          { $unwind: '$user' },
          { $unwind: '$group' },
          { $unwind: '$section' },
          { $unwind: '$course' },
        ],
        as: 'posts'
      }
    },
    {
      $project: {
        _id: false,
        posts: true
      }
    }
  ]);

  return feed.length ? feed[0].posts.reduce((acc, post) => {
    if (
      post.isRestricted &&
      post.user.userId.toString() !== user._id.toString() &&
      user.verification.status !== 'VERIFIED_STUDENT'
    ) {
      post = user.verification.status === 'UNVERIFIED_ID' ?
        {
          isRestricted: true,
          postId: post.postId
        } :
        null;
    } else {
      post.userVerification = post.user.verification;
      if (post.user.userId.toString() !== user._id.toString() && post.isAnonymous) {
        post.userId = post.user.userId;
        post.user = null;
      }
    }
    if (post && post.likes) {
      post.currentUserLiked =
        (post.likes.find(like => like.toString() === user._id.toString()) && 1) ||
        (post.dislikes.find(dislike => dislike.toString() === user._id.toString()) && -1) ||
        0;
      post.likes = post.likes.length - post.dislikes.length;
      delete post.dislikes;
    }
    if (post && post.comments) {
      post.comments = post.comments
        .filter(comment => !comment.parentId)
        .reduce(Comment.normalize(user, post), [])
        .sort((ca, cb) => cb.createdAt - ca.createdAt)
        .slice(0, 2);
      delete post.commentedUsers;
      delete post.blockedCommentedUsers;
      delete post.blockerCommentedUsers;
    }
    if (post) acc.push(post);
    return acc;
  }, []) : [];
};

module.exports = {
  getAll,
};
