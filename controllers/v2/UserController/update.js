const { User } = require('../../../services/v2');

const photo = async (req, res) => {
  const { user, file } = req;
  const photoURL = await User.updatePhoto(user, file);
  res.success(photoURL);
};

const password = async (req, res) => {
  const { oldPassword, newPassword, confirmNewPassword } = req.body;
  await User.updatePassword(req.user, oldPassword, newPassword, confirmNewPassword);
  res.success();
};

const info = async (req, res) => {
  const { firstName, lastName, major } = req.body;
  const newInfo = await User.updateInfo(req.user, firstName, lastName, major);
  res.success(newInfo);
};

module.exports = {
  photo,
  password,
  info
};
