const router = require('express').Router();
const passport = require('../../../conf/v1/auth');
const { FeedController } = require('../../../controllers/v1');
const { userAccess } = require('../../../util');

router.use(passport.authenticate('user-bearer', { session: false }), userAccess);

router.route('/')
  .get(FeedController.getFeed);

module.exports = router;
