const Cookies = require('cookies');
const { Admin } = require('../../../services/v2');

const register = async (req, res) => {
  const admin = await Admin.register(req.body);
  res.success(admin);
};

const login = async (req, res) => {
  const admin = await Admin.login(req.user);
  const cookies = new Cookies(req, res);
  cookies.set('token', admin.token);
  res.success(admin);
};

const logout = async (req, res) => {
  await Admin.logout(req.user);
  const cookies = new Cookies(req, res);
  cookies.set('token', undefined);
  res.success();
};

const authLocal = async (req, res, next) => {
  try {
    const { token } = req.cookies;
    console.log('authLocal token:', token);
    if (token) {
      const admin = await Admin.getByToken(token);
      if (admin) {
        req.user = admin;
        throw new Error('Admin has already authenticated');
      }
    }
    next();
  } catch (err) {
    res.redirect('/admins');
  }
};

const authCookie = async (req, res, next) => {
  try {
    const { token } = req.cookies;
    console.log('authCookie token:', token);
    if (token) {
      const admin = await Admin.getByToken(token);
      if (admin) {
        req.user = admin;
        return next();
      }
    }
    throw new Error('Admin must be authenticated');
  } catch (err) {
    res.redirect('/admins/login');
  }
};

module.exports = {
  register,
  login,
  logout,
  authLocal,
  authCookie,
};
