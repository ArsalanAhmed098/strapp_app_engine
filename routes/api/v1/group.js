const router = require('express').Router();
const passport = require('../../../conf/v1/auth');
const { GroupController } = require('../../../controllers/v1');
const { userAccess } = require('../../../util');

router.use(passport.authenticate('user-bearer', { session: false }), userAccess);

router.route('/')
  .get(GroupController.getGroups)
  .post(GroupController.autoJoinGroup);

router.route('/:groupId')
  .get(GroupController.getGroup)
  .patch(GroupController.updateLastSeenGroup)
  .delete(GroupController.leaveGroup);

router.route('/:groupId/topic')
  .get(GroupController.getGroupTopic);

router.route('/:groupId/users')
  .get(GroupController.getGroupUsers);

module.exports = router;
