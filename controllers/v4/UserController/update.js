const { User } = require('../../../services/v4');

const photo = async (req, res) => {
  // console.log('idhar hai ',req.user,req.body.photo);
  // const { user, photo } = req;
  const photoURL = await User.updatePhoto(req.user, req.body.photo);
  res.success(photoURL);
};

const password = async (req, res) => {
  const { oldPassword, newPassword, confirmNewPassword } = req.body;
  await User.updatePassword(req.user, oldPassword, newPassword, confirmNewPassword);
  res.success();
};

const addStudentData = async (req, res) => {
  const {gradelevel, major } = req.body;
  const newdata = await User.updateStudentInfo(req.user, gradelevel, major);
  res.success(newdata);
};

const updateStudentVerification = async (req, res) => {
  const newdata = await User.updateStudentVerification(req.params.userId);
  res.success(newdata);
};

const updateStudentVerificationEmail = async (req, res) => {
  const {email } = req.body;
  const newdata = await User.updateStudentVerificationEmail(email);
  res.success(newdata);
};

const deleteStudent = async (req, res) => {
  const newdata = await User.deleteStudent(req.params.userId);
  res.success(newdata);
};

// const info = async (req, res) => {
//   const { firstName, lastName, major } = req.body;
//   const newInfo = await User.updateInfo(req.user, firstName, lastName, major);
//   res.success(newInfo);
// };

const info = async (req, res) => {
  const {gradelevel, major } = req.body;
  const newInfo = await User.updateInfo(req.user,gradelevel, major);
  res.success(newInfo);
};

// const profile = async (req, res) => {
//   const { user, file } = req;
//   const { major, gradelevel } = req.body;
//   const photoURL = await User.updateProfile(user, file, major, gradelevel);
//   res.success(photoURL);
// };

const profile = async (req, res) => {
  // const { user } = req;
  // const { major, gradelevel } = req.body;
  const photoURL = await User.updateProfile(req.user, req.body.major, req.body.gradelevel, req.body.photo);
  res.success(photoURL);
};

module.exports = {
  photo,
  password,
  info,
  profile,
  addStudentData,
  updateStudentVerification,
  updateStudentVerificationEmail,
  deleteStudent,
};
