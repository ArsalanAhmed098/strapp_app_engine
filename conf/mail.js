const mailgun = require('mailgun-js');
const DOMAIN = process.env.MAILGUN_DOMAIL;
const api_key = process.env.MAILGUN_API_KEY;
const mg = mailgun({ apiKey: api_key, domain: DOMAIN });
console.log('DOMAIN:', DOMAIN);
console.log('api_key:', api_key);
module.exports = mg;
