const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const uuid4 = require('uuid/v4');
const Cookies = require('cookies');
const { AdminModel } = require('../../../models/v1');

const register = async (req, res) => {
  let { password, ...otherFields } = req.body;
  password = await bcrypt.hash(req.body.password, 8)
  try {
    const admin = await AdminModel.create({ ...otherFields, password });
    admin.password = undefined;
    return res.status(200).json({
      success: true,
      data: {
        admin: admin,
      },
      message: 'admin created successfully',
    });
  } catch (e) {
    return res.status(400).json({
      success: false,
      data: null,
      message: `Problem occurred`,
      errorCode: `Problem occurred`
    });
  }
};

const login = async (req, res) => {
  console.log(req.body);
  req.user.token = uuid4();
  await req.user.save();
  req.user.password = undefined;
  const cookies = new Cookies(req, res);
  cookies.set('token', req.user.token);
  res.status(200).json({
    data: {
      user: req.user,
    },
    success: true,
    message: 'admin logged in successfully',
    errorCode: null
  });
};

const logout = async (req, res) => {
  req.user.token = '';
  await req.user.save();
  const cookies = new Cookies(req, res);
  cookies.set('token', undefined);
  res.status(200).json({
    success: true,
    message: 'admin logged out successfully',
  });
};

const authLocal = async (req, res, next) => {
  try {
    const { token } = req.cookies;
    console.log('authLocal token:', token);
    if (token) {
      const admin = await AdminModel.findOne({ token });
      if (admin) {
        req.user = admin;
        throw new Error('Admin has already authenticated');
      }
    }
    next();
  } catch (err) {
    res.redirect('/admins');
  }
};

const authCookie = async (req, res, next) => {
  try {
    const { token } = req.cookies;
    console.log('authCookie token:', token);
    if (token) {
      const admin = await AdminModel.findOne({ token });
      if (admin) {
        req.user = admin;
        return next();
      }
    }
    throw new Error('Admin must be authenticated');
  } catch (err) {
    res.redirect('/admins/login');
  }
};

module.exports = {
  register,
  login,
  logout,
  authLocal,
  authCookie,
};
