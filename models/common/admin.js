const mongoose = require('mongoose')

const AdminSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
    minLength: 8
  },
  token: {
    type: String,
  },
}, {
  timestamps: true
});

module.exports = mongoose.model('Admin', AdminSchema)
