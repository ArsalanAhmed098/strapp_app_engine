const fs = require('fs');
const path = require('path');
const moment = require('moment');
const multer = require('multer');
const uuid4 = require('uuid/v4');

const storage = (pathName, fileName) => multer.diskStorage({
  destination(req, file, cb) {
    const upldFolder = `uploads/${pathName || moment().format('YMMDD')}`;
    fs.mkdir(upldFolder, { recursive: true }, (err) => {
      if (err && err.code !== 'EEXIST') {
        cb(err, null);
      } else {
        cb(null, upldFolder);
      }
    });
  },
  filename(req, file, cb) {
    cb(null, file.fieldname + '-' + (fileName || uuid4()) + file.originalname.substr(file.originalname.lastIndexOf('.')));
  }
});

const upload = (pathName, fileName) => multer({
  storage: storage(pathName, fileName),
  fileFilter(req, file, cb) {
    const ext = path.extname(file.originalname).toLowerCase();
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg') {
      return cb(new Error('Only images are allowed'));
    }
    cb(null, true);
  }
});

module.exports = upload;
