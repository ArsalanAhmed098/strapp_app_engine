const mongoose = require('mongoose');
const { notificationHandler } = require('../../util');
const bcrypt = require('bcrypt');
const uuid4 = require('uuid/v4');
const { duplicateUsernameOrEmail } = require('../../controllers/generic');
const { sendMail, genConfirmCode, genExpire } = require('../../util');

const UserSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
    minLength: 8
  },
  username: {
    type: String,
    unique: true,
    required: true,
    minLength: 4,
    maxLength: 20,
  },
  photo: {
    type: String,
  },
  major: {
    type: String,
  },
  university: {
    name: {
      type: String,
    },
    domain: {
      type: String
    }
  },
  token: {
    type: String,
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  block: {
    status: {
      type: String,
      enum: ['NOT_BLOCKED', 'TOTAL_BLOCKED', 'WRITE_BLOCKED'],
      default: 'NOT_BLOCKED'
    },
    deniedRoutes: [{
      path: String,
      method: {
        type: String,
        enum: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', '*']
      }
    }]
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  confirmation: {
    code: {
      type: String
    },
    expire: {
      type: Date,
    }
  },
  notification: {
    state: {
      type: Boolean,
      default: true
    },
    firebaseTokens: [String],
    lastSeen: Date
    }
}, {
  timestamps: true
});

UserSchema.statics.register = async function (userData) {
  try {
    // Check the duplication of username/email
    const duplicateError = await duplicateUsernameOrEmail(this, userData);
    if (duplicateError) {
      return {
        error: {
          message: `${duplicateError.email ? duplicateError.email : duplicateError.username}`,
          errorCode: `${duplicateError.email ? `EMAIL_ALREADY_EXISTS` : `USERNAME_ALREADY_EXISTS`}`
        }
      };
    }

    // Check the email based on university data
    const domain = userData.email.split('@')[1];
    const university = await mongoose.model('University').findByDomain(domain);
    if (!university) {
      return {
        error: {
          message: `Email is incorrect`,
          errorCode: `EMAIL_IS_INCORRECT`
        }
      };
    }

    userData.university = {
      name: university.name,
      domain
    };
    userData.password = await bcrypt.hash(userData.password, 8);
    userData.token = uuid4();

    const confirmCode = genConfirmCode();
    userData.confirmation = {
      code: confirmCode,
      expire: genExpire(5)
    };

    const user = await this.create(userData);
    user.password = undefined;
    user.confirmation = undefined;
    user.firebaseTokens = undefined;
    user.deniedRoutes = undefined;

    const verifyMail = await sendMail({
      name: userData.firstName,
      email: userData.email,
      confirmCode
    });
    console.log('verifyMail state:', verifyMail);

    return {
      data: user,
      message: 'User created successfully'
    };

  } catch (err) {
    console.log('registration err:', err);
    return {
      error: {
        message: `Problem occurred`,
        errorCode: `Problem occurred`
      }
    };
  }
};

UserSchema.methods.registerfirebaseTokens = async function (token) {
  this.notification.firebaseTokens || (this.notification.firebaseTokens = []);
  if (!this.notification.firebaseTokens.find(existingToken => existingToken === token)) {
    this.notification.firebaseTokens.push(token);
    await this.save();
  }
};

UserSchema.methods.changeNotificationState = function (state) {
  this.notification.state = state;
  return this.save();
};

UserSchema.methods.pushNotification = async function (payload, options) {
  const { sender } = payload;
  payload.data = {
    body: sender.photo || ''
  };
  delete payload.sender;
  const notification = {
    receiver: this._id,
    sender: sender._id,
    payload: JSON.stringify(payload),
    firebaseResponses: [],
  };
  try {
    if (this.notification && this.notification.state && this.notification.firebaseTokens.length) {
      for (let token of user.notification.firebaseTokens) {
        const firebaseState = { token };
        const response = await notificationHandler(token)(payload, options);
        if (response.successCount) {
          firebaseState.status = true;
          firebaseState.messageId = response.results[0].messageId;
        } else {
          firebaseState.status = false;
          firebaseState.error = response.results[0].error;
        }
        firebaseState.multicastId = response.multicastId;
        notification.firebaseResponses.push(firebaseState);
      }
    }
  } catch (err) {
    notification.firebaseResponses.push({
      status: false,
      error: err,
    });
  }
  await mongoose.model('Notification').create(notification);

  console.log('Push notification to:', user.notification.token);
  console.log('Push notification status:', notification.status);

  return notification.status;
};

UserSchema.methods.hasAccess = function ({ path, method }) {
  const result = !(this.block.deniedRoutes && this.block.deniedRoutes.find(route => {
    if (route.method !== '*' && route.method !== method) return false;
    const path1 = route.path.split('/');
    const path2 = path.split('/');
    if (path1.length > path2.length)
      return false;
    for (let i = 0; i < path1.length; i++)
      if (path1[i] !== '*' && path1[i] !== path2[i])
        return false;
    return true;
  }));
  return result;
};

UserSchema.methods.permission = function (status) {
  if (userBlock[status]) {
    return userBlock[status](this);
  }
};

const userBlock = {
  ['NOT_BLOCKED'](user) {
    user.block.status = 'NOT_BLOCKED';
    user.block.deniedRoutes = [];
    return user.save();
  },
  ['WRITE_BLOCKED'](user) {
    user.block.status = 'WRITE_BLOCKED';
    user.block.deniedRoutes = [
      {
        path: 'posts',
        method: 'POST'
      },
      {
        path: 'comments',
        method: 'POST'
      }
    ];
    return user.save();
  },
  ['TOTAL_BLOCKED'](user) {
    user.block.status = 'TOTAL_BLOCKED';
    user.block.deniedRoutes = [
      {
        path: '*',
        method: '*'
      }
    ];
    return user.save();
  },
};

module.exports = mongoose.model('User', UserSchema);
