const bcrypt = require('bcrypt');
const { UserModel, NotificationModel } = require('../../models/v1');
const { isUniqueness } = require('../generic');
const { genExpire, genConfirmCode, sendMail } = require('../../util');
const fs = require('fs').promises;
const path = require('path');

const register = async (req, res) => {

  const response = await UserModel.register(req.body);
  if (response.error) {
    return res.status(400).json({
      success: false,
      data: null,
      message: response.error.message,
      errorCode: response.error.errorCode
    });
  } else {
    return res.status(200).json({
      success: true,
      data: {
        user: response.data,
      },
      message: response.message,
    });
  }

};

const tokenValidation = async (req, res) => {
  const user = await UserModel.findOne({ token: req.body.token });
  if (!user) {
    return res.status(200).json({
      success: false,
      data: null,
      message: `token incorrect`,
      errorCode: `TOKEN_INCORRECT`
    });
  }
  return res.status(200).json({
    success: true,
    data: user.token,
    message: `token correct`,
  });
};

const checkUniqueness = async (req, res) => {
  const fieldName = req.body.username ? `username` : `email`;
  const result = await isUniqueness(UserModel, req);
  if (typeof result === 'object') {
    res.status(200).json({
      data: null,
      message: `${fieldName} already exists`,
      success: false,
      errorCode: `${fieldName.toUpperCase()}_ALREADY_EXISTS`
    });
  } else if (result === true) {
    res.status(200).json({
      data: null,
      message: `email is invalid`,
      success: false,
      errorCode: `EMAIL_IS_INVALID`
    });
  } else {
    res.status(200).json({
      data: null,
      message: `${fieldName} isn\'t taken, good to go`,
      success: true,
      errorCode: null,
    });
  }
};

const uploadUserPic = async (req, res) => {
  if (!req.file) {
    return res.status(400).json({
      success: false,
      data: null,
      message: `Photo is required`,
      errorCode: `PHOTO_IS_REQUIRED`
    });
  }
  try {
    const photoPath = path.join(require.main.filename, '../../' , req.user.photo);
    await fs.unlink(photoPath);
  } catch (err) { console.log('unlink err:', err) }
  const user = await UserModel.findByIdAndUpdate(req.user._id, {
    photo: `/${req.file.destination}/${req.file.filename}`
  }, { new: true });
  return res.status(200).json({
    success: true,
    data: user.photo,
    message: `Profile image uploaded successfully`,
  });
};

const changePassword = async (req, res) => {
  const { oldPassword, newPassword, confirmNewPassword } = req.body;
  if (newPassword !== confirmNewPassword) {
    return res.status(400).json({
      success: false,
      data: null,
      message: `New passwords do not match`,
      errorCode: `NEW_PASSWORDS_DONT_MATCH`
    });
  }
  const comparedResult = await bcrypt.compare(oldPassword, req.user.password);
  if (!comparedResult) {
    return res.status(400).json({
      success: false,
      data: null,
      message: `Old password is incorrect`,
      errorCode: `OLD_PASSWORD_IS_INCORRECT`
    });
  }
  const password = await bcrypt.hash(newPassword, 8);
  const user = await UserModel.findByIdAndUpdate(req.user._id, { password });
  return res.status(200).json({
    success: true,
    data: null,
    message: `Password changed successfully`,
  });
};

const changeInfo = async (req, res) => {
  const info = {};
  req.body.firstName && (info.firstName = req.body.firstName);
  req.body.lastName && (info.lastName = req.body.lastName);
  req.body.major && (info.major = req.body.major);
  const user = await UserModel.findByIdAndUpdate(req.user._id, info, { new: true }).select('firstName lastName major');
  return res.status(200).json({
    success: true,
    data: user,
    message: `Info changed successfully`,
  });
};

const confirmUser = async (req, res) => {
  if (req.user.confirmation.code === req.body.code && req.user.confirmation.expire > Date.now()) {
    req.user.isVerified = true;
    req.user.confirmation = undefined;
    const user = await req.user.save();
    return res.status(200).json({
      success: true,
      data: user,
      message: `User verified successfully`,
    });
  } else {
    return res.status(400).json({
      success: false,
      data: null,
      message: `Code is incorrect or it's expired!`,
      errorCode: `CODE_IS_INCORRECT`
    });
  }
};

const resendConfirmUser = async (req, res) => {
  const confirmCode = req.user.confirmation.code || genConfirmCode();
  const verifyMail = await sendMail({
    name: req.user.firstName,
    email: req.user.email,
    confirmCode
  });
  console.log('verifyMail:', verifyMail);
  req.user.confirmation = {
    code: confirmCode,
    expire: genExpire(5)
  };
  const user = await req.user.save();
  return res.status(200).json({
    success: true,
    data: null,
    message: `Confirmation code has been sent to your email!`,
  });
}

const registerfirebaseTokens = async (req, res) => {
  await req.user.registerfirebaseTokens(req.body.token);

  return res.status(200).json({
    success: true,
    data: null,
    message: `Firebase token registered successfully`,
  });
};

const getNotifications = async (req, res) => {
  const result = await NotificationModel.aggregate([
    {
      $match: {
        receiver: req.user._id
      }
    },
    {
      $lookup: {
        from: 'users',
        let: { uId: '$sender'},
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$uId']
              }
            }
          },
          {
            $project: {
              _id: false,
              userId: '$_id',
              isVerified: true,
              firstName: true,
              lastName: true,
              photo: true,
              major: true,
              username: true,
              email: true,
              university: true,
              createdAt: true,
            }
          }
        ],
        as: 'userSender'
      }
    },
    { $unwind: '$userSender'},
    {
      $project: {
        _id: false,
        notificationId: '$_id',
        payload: true,
        sender: '$userSender',
        createdAt: true
      }
    },
    {
      $group: {
        _id: { $cond: { if: { $gt: ['$createdAt', req.user.notification.lastSeen] }, then: 'unseen', else: 'seen' } },
        notificationCount: { $sum: 1 },
        notifications: { $addToSet: '$$ROOT' }
      }
    },
  ]);
  const notifications = result.reduce((acc, curr) => {
    curr.notifications = curr.notifications.map(noti => {
      noti.payload = JSON.parse(noti.payload);
      noti.content = {
        title: noti.payload.notification.title,
        description: noti.payload.notification.body,
      };
      delete noti.payload;
      return noti;
    });
    acc[curr._id] = {
      count: curr.notificationCount,
      notifications: curr.notifications
    };
    return acc;
  }, {});
  return res.status(200).json({
    success: true,
    data: notifications,
    message: `Notifications fetched successfully`,
  });

};

const changeNotificationState = async (req, res) => {
  await req.user.changeNotificationState(req.body.pushState);

  return res.status(200).json({
    success: true,
    data: null,
    message: `Notification state changed successfully`,
  });
};

const updateNotificationSeen = async (req, res) => {
  req.user.notification.lastSeen = Date.now();
  await req.user.save();
  return res.status(200).json({
    success: true,
    data: null,
    message: `lastSeen updated successfully`,
  });
};

module.exports = {
  register,
  tokenValidation,
  checkUniqueness,
  uploadUserPic,
  changePassword,
  changeInfo,
  confirmUser,
  resendConfirmUser,
  registerfirebaseTokens,
  getNotifications,
  changeNotificationState,
  updateNotificationSeen,
};
