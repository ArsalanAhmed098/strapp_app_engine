const { Comment } = require('../../services/v4');

const add = async (req, res) => {
  req.body.photos = [];
  console.log('comment photos:', req.files);
  if(req.files) {
    for(let file of req.files) {
      req.body.photos.push(`/${file.destination}/${file.filename}`);
    }
  }
  const comment = await Comment.add(req.user, req.params.postId, req.body);
  res.success(comment);
};

const getAll = async (req, res) => {
  const comments = await Comment.getAll(req.user, req.params.postId);
  res.success(comments);
};

const remove = async (req, res) => {
  await Comment.remove(req.user, req.params.postId, req.params.commentId);
  res.success();
};

const report = async (req, res) => {
  await Comment.report(req.user, req.params.postId, req.params.commentId, req.body.reasons);
  res.success();
};

const like = async (req, res) => {
  const comment = await Comment.like(req.user, req.params.postId, req.params.commentId);
  res.success(comment);
};

const unlike = async (req, res) => {
  const comment = await Comment.unlike(req.user, req.params.postId, req.params.commentId);
  res.success(comment);
};

const dislike = async (req, res) => {
  const comment = await Comment.dislike(req.user, req.params.postId, req.params.commentId);
  res.success(comment);
};

const undislike = async (req, res) => {
  const comment = await Comment.undislike(req.user, req.params.postId, req.params.commentId);
  res.success(comment);
};

module.exports = {
  add,
  getAll,
  remove,
  report,
  like,
  unlike,
  dislike,
  undislike,
};
