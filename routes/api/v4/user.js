const router = require('express').Router();
const passport = require('../../../conf/v2/auth');
// const { UserController, NotificationController } = require('../../../controllers/v3');
const { UserController, NotificationController } = require('../../../controllers/v4');
const { userAccessV3, upload } = require('../../../middleware');
const deeplink = require('node-deeplink');

router.route('/verify')
  .get(UserController.verifyUserByEmail);

router.route('/verify/:verifyCode')
  .get(UserController.verifyUser);

router.route('/deeplink')
  .get(
    deeplink({
      fallback: process.env.DEEPLINK_CALLBACK,
      android_package_name: process.env.DEEPLINK_ANDROID,
      ios_store_link: process.env.DEEPLINK_IOS,
    })
  );

  router.route('/forgot/password')
  .post(UserController.forgotPassword);

router.use(passport.authenticate('student-bearer', { session: false }), userAccessV3);

router.route('/')
  .get(UserController.get);

router.route('/check-verification')
  .get(UserController.checkVerification);


router.route('/photo')
  .put(
    // upload('profiles').single('photo'),
    UserController.update.photo
  );
router.route('/password')
  .put(UserController.update.password);

router.route('/add/userdata')
  .put(UserController.update.addStudentData);

  router.route('/:userId/custom/verifystudent')
  .put(UserController.update.updateStudentVerification);

  router.route('/:userId/custom/deletestudent')
  .put(UserController.update.deleteStudent);

  router.route('/custom/verifystudentbyemail')
  .put(UserController.update.updateStudentVerificationEmail);

  router.route('/info')
  .put(UserController.update.info);

// router.route('/profile')
//   .put(
//     upload('profiles').single('photo'),
//     UserController.update.profile
//   );

router.route('/profile')
  .put(
    // upload('profiles').single('photo'),
    UserController.update.profile
  );

router.route('/send-verification')
  .get(UserController.sendVerification);

router.route('/notifications')
  .get(NotificationController.getAll);
  
router.route('/notifications/firebase')
  .post(NotificationController.registerfirebaseToken)
  .get(NotificationController.getfirebaseTokens);
router.route('/notifications/seen')
  .patch(NotificationController.updateSeen);
router.route('/notifications/state')
  .patch(NotificationController.updateState);

router.route('/:userId')
  .get(UserController.getUserById);

router.route('/:userId/report')
  .patch(UserController.report);

router.route('/:userId/block')
  .patch(UserController.block);
router.route('/:userId/unblock')
  .patch(UserController.unblock);

router.route('/userlogs/add')
  .post(UserController.userLogsAdd);

  router.route('/userlogs/update')
  .post(UserController.userLogsUpdate);

  router.route('/chat/notification')
  .post(UserController.chattNotify);

module.exports = router;


