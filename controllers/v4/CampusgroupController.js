const { Campusgroups } = require('../../services/v4');

// const add = async (req, res) => {
//   const { courseName } = req.params;
//   const {
//     name: sectionName,
//     number: sectionNumber,
//     category: sectionCategory
//   } = req.body;
//   const section = await Section.add(req.user, courseName, sectionName, sectionNumber, sectionCategory);
//   res.success(section);
// };

// const getAllByCourse = async (req, res) => {
//   const { courseName } = req.params;
//   const sections = await Section.getAllByCourse(req.user, courseName);
//   res.success(sections);
// };

// const getAll = async (req, res) => {
//   const sections = await Section.getAll(req.user);
//   res.success(sections);
// };

// const getAllCampusSections = async (req, res) => {
//   var fullUrl = req.protocol + '://' + req.get('host') + '/';
//   const campussections = await Section.getAllCampusSections(req.user,fullUrl);
//   res.success(campussections);
// };

// const getAllSectionsByExplore = async (req, res) => {

//   var fullUrl = req.protocol + '://' + req.get('host') + '/';

//   console.log('hello',fullUrl);
//   const { exploreName } = req.params;
//   const campusExploreSections = await Section.getAllSectionsByExplore(req.user,fullUrl,exploreName);
//   res.success(campusExploreSections);
// };

const getAllCampusGrouplist = async (req, res) => {
  var fullUrl = req.protocol + '://' + req.get('host') + '/';
  const campusGrouplisting = await Campusgroups.getAllCampusGroups(req.user,fullUrl);
  res.success(campusGrouplisting);
};



// const get = async (req, res) => {
//   const section = await Section.get(req.user, req.params.sectionId);
//   res.success(section);
// };

// const join = async (req, res) => {
//   const section = await Section.join(req.user, req.params.sectionId);
//   res.success(section);
// };

// const leave = async (req, res) => {
//   await Section.leave(req.user, req.params.sectionId);
//   res.success();
// };

// const updateSeen = async (req, res) => {
//   await Section.updateSeen(req.user, req.params.sectionId);
//   res.success();
// };

// const getAllMembers = async (req, res) => {
//   const users = await Section.getAllMembers(req.params.sectionId);
//   res.success(users);
// };

module.exports = {
//   add,
//   getAllByCourse,
//   getAll,
//   get,
//   join,
//   leave,
//   updateSeen,
//   getAllMembers,
//   getAllCampusSections,
//   getAllSectionsByExplore,
getAllCampusGrouplist
};
