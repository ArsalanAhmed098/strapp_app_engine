const mongoose = require('mongoose');

const campusgroupsSchema = new mongoose.Schema({
  group: {
    name: {
      type: String,
      required: true,
      // uppercase: true
    },
    description: {
      type: String,
      required: true,
    },
  },
  photo: {
    type: String,
    default: 'empty',
  },
  university: {
    name: {
      type: String,
      required: true,
    },
    domains: [{
      type: String
    }]
  },
  isDefault: {
    type: Boolean,
    default: false
  },
  isBlocked: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  grouptype: {
    type: String,
    default: 'campusgroup'
  }
}, 
{
  timestamps: true
});

module.exports = mongoose.model('campusgroups', campusgroupsSchema);
