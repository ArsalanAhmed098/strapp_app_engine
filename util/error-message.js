const errorCodeList = {
  'TOKEN_INVALID': {
    message: `token is invalid`,
    statusCode: 400,
  },
  'EMAIL_INVALID': {
    message: `email is invalid`,
    statusCode: 400,
  },
  'EMAIL_EXISTS': {
    message: `email already exists`,
    statusCode: 400,
  },
  'USERNAME_EXISTS': {
    message: `username already exists`,
    statusCode: 400,
  },
  'CODE_INCORRECT': {
    message: `Code is incorrect or it is expired`,
    statusCode: 400,
  },
  'USER_CONFIRMED': {
    message: `user has already confirmed`,
    statusCode: 400,
  },
  'PHOTO_REQUIRED': {
    message: `Photo is required`,
    statusCode: 400,
  },
  'NEW_PASSWORDS_NOT_MATCH': {
    message: `New passwords do not match`,
    statusCode: 400,
  },
  'OLD_PASSWORD_INCORRECT': {
    message: `Old password is incorrect`,
    statusCode: 400,
  },
  'PASSWORDS_NOT_MATCH': {
    message: `passwords do not match`,
    statusCode: 400,
  },
  'SECTION_EXISTS': {
    message: `section already exists`,
    statusCode: 400,
  },
  'BAD_REQUEST': {
    message: `Bad request`,
    statusCode: 400,
  },
  'UNAUTHORIZED': {
    message: `Unauthorized`,
    statusCode: 401,
  },
  'FORBIDDEN': {
    message: `You do not have access to this route`,
    statusCode: 403,
  },
  'UNVERIFIED': {
    message: `You must be verified to access to this route`,
    statusCode: 403,
  },
  'STUDENT_NOT_FOUND': {
    message: `Student not found`,
    statusCode: 404,
  },
  'COURSE_NOT_FOUND': {
    message: `Course not found`,
    statusCode: 404,
  },
  'SECTION_NOT_FOUND': {
    message: `Section not found`,
    statusCode: 404,
  },
  'POST_NOT_FOUND': {
    message: `post not found`,
    statusCode: 404,
  },
  'COMMENT_NOT_FOUND': {
    message: `comment not found`,
    statusCode: 404,
  },
  'NOT_FOUND': {
    message: `Not found`,
    statusCode: 404,
  },
  'INTERNAL_SERVER_ERROR': {
    message: `Internal Server Error`,
    statusCode: 500,
  },
};

const statusCodeList = {
  400: 'BAD_REQUEST',
  401: 'UNAUTHORIZED',
  403: 'FORBIDDEN',
  404: 'NOT_FOUND',
  500: 'INTERNAL_SERVER_ERROR',
};

const getByErrorCode = errorCode => errorCodeList[errorCode] || errorCodeList['INTERNAL_SERVER_ERROR'];

const getByStatusCode = statusCode => statusCodeList[statusCode] || statusCodeList[500];

module.exports = {
  getByErrorCode,
  getByStatusCode,
};
